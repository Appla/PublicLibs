<?php
namespace Cache;

class Redis
{
    /**
     * default configs
     *
     * @var array
     */
    private static $config = [
        'host' => '127.0.0.1',
        // 'host' => '192.168.1.77',
        'port' => 6379,
        'timeout' => 5,
    ];

    /**
     * singleton instance
     *
     * @var array
     */
    private static $instance = null;

    /**
     * facade door
     *
     * @param string $method
     * @param mixed $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        return call_user_func_array([static::getInstance(), $method], $args);
    }

    /**
     * connect a redis instance
     *
     * @param  array  $config
     * @return null|\Redis
     */
    public static function connect(array $config = [])
    {
        static::$instance = new \Redis();
        static::$config = array_merge(static::$config, $config);
        // if (self::$instance->connect(...self::$config['host'], self::$config['port'], self::$config['timeout'])) {
        if (static::$instance->connect(...array_values(static::$config))) {
            return static::$instance;
        }
        die('error');
    }

    /**
     *
     * @return \Redis
     */
    public static function getInstance()
    {
        return static::$instance ?: static::connect();
    }
}
