<?php
namespace HttpRequester;

use HttpRequester\Curl;

class CurlFacade
{
    /**
     * self instance
     *
     * @var \HttpRequester\Curl
     */
    protected static $instance;

    /**
     * __callStatic
     *
     * @param  string $method
     * @param  array $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        if(is_callable([static::getInstance(), $method])){
            return call_user_func_array([static::getInstance(), $method], $args);
        }
    }

    /**
     * setInstance
     *
     * @param \HttpRequester\Curl $obj
     * @return \HttpRequester\Curl
     */
    public static function setInstance(Curl $obj)
    {
        return static::$instance = $obj;
    }

    /**
     * getInstance
     *
     * @return \HttpRequester\Curl
     */
    public static function getInstance()
    {
        return static::$instance ?: static::$instance = new Curl();
    }
}
