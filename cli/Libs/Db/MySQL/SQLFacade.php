<?php

namespace Db\MySQL;

class SQLFacade
{
    /**
     * default configs
     *
     * @var array
     */
    private static $config = [
        'host' => '',
        'username' => 'root',
        'password' => 'root',
        'db'=> '',
        'port' => 3306,
        'prefix' => '',
        'charset' => 'utf8'
    ];

    /**
     * singleton instance
     *
     * @var array
     */
    private static $instance = null;

    /**
     * facade door
     *
     * @param string $method
     * @param mixed $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(static::getInstance(), $method), $args);
    }

    /**
     * __call
     *
     * @param  string $method
     * @param  array $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        return call_user_func_array(array(static::getInstance(), $method), $args);
    }

    /**
     * connect a redis instance
     *
     * @param  array  $config
     * @return null|\MysqliDb
     */
    public static function connect(array $config = [])
    {
        if(!class_exists('MysqliDb ')){
            require __DIR__. DIRECTORY_SEPARATOR .'MysqliDb'. DIRECTORY_SEPARATOR.'MysqliDb.php';
        }
        $config = array_merge(static::$config, $config);
        if (static::$instance = new \MysqliDb($config)) {
            return static::$instance;
        }
        die('error');
    }

    /**
     *
     * @return \MysqliDb
     */
    public static function getInstance(array $config = [])
    {
        return static::$instance ?: static::connect($config);
    }
}
