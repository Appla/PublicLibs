<?php
namespace CookieAuth;

use HttpRequester\CurlFacade;

abstract class BaseTester
{
    public static $host = '';
    protected $request_url = '';

    /**
     * __construct
     *
     * @param string $url
     */
    public function __construct($url = null, CurlFacade $requester = null)
    {
        $url and $this->request_url = $url;
    }

    /**
     * request via get
     *
     * @param  array $configs
     * @return mixed
     */
    public function get($url = '', array $configs = [])
    {
        $url === '' and $url = $this->request_url;
        $options = $this->getCurlConfig($configs);
        $options[CURLOPT_HTTPHEADER] = $this->getHeaders();
        return CurlFacade::get($url, $options);
    }

    /**
     * request via post
     *
     * @param  array $configs
     * @return mixed
     */
    public function post($url = '', $data = '' ,array $configs = [])
    {
        $url === '' and $url = $this->request_url;
        $options = $this->getCurlConfig($configs);
        $options[CURLOPT_HTTPHEADER] = $this->getHeaders();
        return CurlFacade::post($url, $data ,$options);
    }

    public function getCurlInfo()
    {
        return CurlFacade::getLastRequestInfo();
    }

    /**
     * getCurlConfig
     *
     * @param  array $config
     * @return array
     */
    public function getCurlConfig(array $config = [])
    {
        $data = [
            CURLOPT_ENCODING => 'gzip, deflate, sdch',
        ];
        return array_replace($data, $config);
    }

    /**
     * get http request header
     *
     * @param  array  $header
     * @return array
     */
    abstract public function getHeaders(array $header = []);
}
