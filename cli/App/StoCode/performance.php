<?php


$str1 = implode('', range(1, 5005));
$str2 = implode('', range(1001, 6000));

timer('stringCross');
$r1 = stringCross($str1, $str2);
timer('stringCross');

timer('arrayCross');
$r2 = implode('', arrayCross(str_split($str1), str_split($str2)));
timer('arrayCross');

timer('oldStringCross');
$r3 = oldStringCross($str1, $str2);
timer('oldStringCross');
timer(true);
var_dump($r1 === $r2, $r2 === $r3);
exit;
$arr = array_combine(range(1, 20, 2), range(1, 10));
$arr2 = array_combine(range(2, 20, 2), range(11, 20));
$allArr = $arr + $arr2;
ksort($allArr);
var_dump($allArr);

function arrayCross(array $arr1, array $arr2)
{
    $minLen = min(count($arr1), count($arr2));
    $stack = [];
    //array_shift cost more time
    // while ($minLen--) {
    //     $stack[] = array_shift($arr1);
    //     $stack[] = array_shift($arr2);
    // }
    for ($i = 0; $i < $minLen; $i++) {
        $stack[] = $arr1[$i];
        $stack[] = $arr2[$i];
    }
    return array_merge($stack, array_slice(isset($arr1[$minLen]) ? $arr1 : $arr2, $minLen));
}

exit;
$arr1 = range(1,10);
$arr2 = range(11,21);

$minLen = min(count($arr1), count($arr2));
$stack = [];
while($minLen--){
    $stack[] = array_shift($arr1);
    $stack[] = array_shift($arr2);
}
$stack = array_merge($stack, $arr1 + $arr2);
var_dump($stack);

exit;


// timer();
// $const = get_defined_constants(true);
// // var_dump($const['curl']);
// $curl_const = array_filter($const['curl'], function ($key) {
//     return strpos($key, 'CURLOPT_') === 0;
// }, ARRAY_FILTER_USE_KEY);

// var_dump(constant('CURLOPT_CAINFO'));

// var_dump($const);
// timer();
// timer(true);

const ROOT_PATH = __DIR__ . DIRECTORY_SEPARATOR;

// $first_codes = require ROOT_PATH . '1st_codes.php';
// $second_codes1 = require ROOT_PATH . 'indexed_address_2nd_code.php';
// // $second_codes2 = require ROOT_PATH . 'indexed_2nd_codes.php';
// $arr0 = [55,3,12,55,8];
// $arr1 = ['a'=>5, 'b'=> 4, 'c' => 1, 'd'=>8,'e' =>2];
// // $arr2 = [1,2,3,4,5,6,7];
// array_multisort($arr0, SORT_ASC, SORT_NUMERIC ,$arr1);
// var_dump($arr0, $arr1);


exit;

$tpl = [
    'string' => 'abc',
];
$second_codes2 = [];
$sortArr = [];
// buildTestSortData($tpl, $second_codes2);
$key = 'multi_center';
$maxLen = 300;
while($maxLen-- > 0) {
    $num = mt_rand(0, 99);
    $sortArr[] = $tpl[$key] = $num;
    $second_codes2[] = $tpl;
}
var_dump(array_slice($second_codes2, 0, 15));

timer('php_quick_sort');
$data = quickSort($second_codes2, 'multi_center');
// $data = quickSort($second_codes2, 'multi_center');
timer('php_quick_sort');

var_dump(array_slice($data, 0, 15));

timer('array_multisort');
// $sortArr = array_combine(array_keys($second_codes2), array_column($second_codes2, 'multi_center'));
array_multisort($sortArr, SORT_ASC, SORT_NUMERIC, $second_codes2);
// array_multisort($sortArr, SORT_ASC, SORT_NUMERIC, $second_codes2);
timer('array_multisort');
var_dump(array_slice($second_codes2, 0, 15));

timer(true);
exit;



timer('array_map');
arrayValueToString($second_codes1);
arrayValueToString($second_codes1);
arrayValueToString($second_codes1);
arrayValueToString($second_codes1);
arrayValueToString($second_codes1);
arrayValueToString($second_codes2);
timer('array_map');

timer('foreach');
arrVal2Str($second_codes1);
arrVal2Str($second_codes1);
arrVal2Str($second_codes1);
arrVal2Str($second_codes1);
arrVal2Str($second_codes1);
arrVal2Str($second_codes2);
timer('foreach');

timer('array_map_1');
arrayValueToString($second_codes1);
arrayValueToString($second_codes1);
arrayValueToString($second_codes1);
arrayValueToString($second_codes1);
arrayValueToString($second_codes1);
arrayValueToString($second_codes2);
timer('array_map_1');

timer('foreach_1');
arrVal2Str($second_codes1);
arrVal2Str($second_codes1);
arrVal2Str($second_codes1);
arrVal2Str($second_codes1);
arrVal2Str($second_codes1);
arrVal2Str($second_codes2);
timer('foreach_1');

timer(true);

function stringCross(string $str1, string $str2)
{
    $minLen = min(strlen($str1), strlen($str2));
    $str = '';
    for ($i = 0; $i < $minLen; $i++) {
        $str .= $str1[$i].$str2[$i];
    }
    return $str.substr(isset($str1[$minLen]) ? $str1 : $str2, $minLen);
}

function oldStringCross($str1, $str2)
{
    $binary = '';
    $uselong = 1;
    while (strlen($str2) + strlen($str1)) {
        if ($uselong) {
            $binary = $binary.substr($str1, 0, 1);
            $str1 = substr($str1, 1);
        } else {
            $binary = $binary.substr($str2, 0, 1);
            $str2 = substr($str2, 1);
        }
        $uselong = !$uselong;
    }
    return $binary;
}

function buildTestSortData(array $tpl, array &$data, $key = 'multi_center', $maxLen = 10000)
{
    for (; $maxLen > 0; $maxLen--) {
        $num = mt_rand(0, 99);
        $tpl[$key] = $num;
        $data[] = $tpl;
    }
}

function arrayValueToString(array $data)
{
    return array_map(function ($val) {
        return json_encode($val, JSON_UNESCAPED_UNICODE);
    }, $data);
}
function arrVal2Str(array $data)
{
    foreach ($data as &$datum) {
        $datum = json_encode($datum, JSON_UNESCAPED_UNICODE);
    }
    return $data;
}

function timer($key = 'default')
{
    static $log = [];
    if ($key === true) {
        foreach ($log as $key => $val) {
            printf('item:%s, start:%s, end:%s, cost:%s' . PHP_EOL, $key, $val['start'], $val['end'], $val['cost']);
        }
        printf('memory usage: %s,  memory peak usage:%s', memoryConvert(memory_get_usage(true)), memoryConvert(memory_get_peak_usage(true)));
    }
    if (!isset($log[$key]['start'])) {
        $log[$key]['start'] = microtime(true);
    } else {
        $log[$key]['end'] = microtime(true);
        $log[$key]['cost'] = $log[$key]['end'] - $log[$key]['start'];
    }
}

function memoryConvert($size)
{
    $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
    return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
}


// ?????
function quickSort($result, $field = 'distance', $order_by = 'asc')
{
    $stack = array();
    array_push($stack, array(0, count($result) - 1));
    while (list($beg, $end) = array_pop($stack)) {
        if ($end > $beg) {
            $piv = $result[$beg][$field];
            $k = $beg + 1;
            $r = $end;
            while ($k < $r) {
                if ($result[$k][$field] < $piv) {
                    $k++;
                } else {
                    $tmp = $result[$k];
                    $result[$k] = $result[$r];
                    $result[$r] = $tmp;
                    $r--;
                }
            }
            if ($result[$k][$field] >= $piv) {
                $k--;
            }
            $tmp = $result[$k];
            $result[$k] = $result[$beg];
            $result[$beg] = $tmp;
            array_push($stack, array($beg, $k));
            array_push($stack, array($r, $end));
        }
    }

    return $order_by == 'asc' ? $result : array_reverse($result);
}
