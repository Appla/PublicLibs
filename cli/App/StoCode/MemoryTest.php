<?php
$usage['start'] = microtime(true);
const ROOT_PATH = __DIR__.DIRECTORY_SEPARATOR;
// $addressBaseArray = require ROOT_PATH.'address_base_array.php';
// $codeBaseArray = require ROOT_PATH.'code_base_array.php';
$firstCodes = require ROOT_PATH.'1st_codes.php';

// $usage['usage'] = memory_get_usage();
// $usage['usage_peak'] = memory_get_peak_usage();

$usage['usage'] = memory_get_usage(true);
$usage['usage_peak'] = memory_get_peak_usage(true);

$usage['end'] = microtime(true);
$usage['cost'] = $usage['end'] - $usage['start'];
$usage['usage_readable'] = memoryConvert($usage['usage']);
$usage['usage_peak_readable'] = memoryConvert($usage['usage_peak']);
var_dump($usage);


function memoryConvert($size)
{
 $unit=array('b','kb','mb','gb','tb','pb');
 return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}
