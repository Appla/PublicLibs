<?php

require __DIR__.DIRECTORY_SEPARATOR.'StoCodeTableBuilder.php';

class StoRouteTableBuilder extends StoCodeTableBuilder
{

// CREATE TABLE `special_pack_rule` (
// `id`  int UNSIGNED NOT NULL AUTO_INCREMENT ,
// `from_province`  varchar(15) NOT NULL COMMENT '始发省份' ,
// `from_city`  varchar(15) NOT NULL DEFAULT '' COMMENT '始发城市' ,
// `from_district`  varchar(15) NOT NULL DEFAULT '' COMMENT '始发区县' ,
// `from_town`  varchar(15) NOT NULL DEFAULT '' COMMENT '始发乡镇/街道' ,
// `to_province`  varchar(15) NOT NULL COMMENT '目的省份' ,
// `to_city`  varchar(15) NOT NULL COMMENT '目的城市' ,
// `to_district`  varchar(15) NOT NULL DEFAULT '' COMMENT '目的区县' ,
// `to_town`  varchar(15) NOT NULL DEFAULT '' COMMENT '目的乡/镇/街道' ,
// `pack_name`  varchar(15) NOT NULL COMMENT '集包名' ,
// `first_code`  varchar(3) NOT NULL COMMENT '一段码' ,
// `trans_center_name`  varchar(15) NOT NULL COMMENT '转运中心' ,
// `trans_center_code`  varchar(10) NOT NULL COMMENT '转运中心编号' ,
// `is_valid`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否有效' ,
// `create_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
// `update_at`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
// PRIMARY KEY (`id`),
// INDEX `index` (`from_province`, `to_province`, `to_city`) USING BTREE
// ) COMMENT '特殊包裹'
// ;

    const CLEAR_FIELD = [
        8
    ];

    const USELESS_FIELD = 8;

    const SLICE_PARAMS = [0, 12];

    const COLUMNS = [
        'from_province', //始发省份
        'from_city',  //始发城市
        'from_district', //始发区县
        'from_town', //始发乡镇/街道
        'to_province', //目的省份
        'to_city', //目的城市
        'to_district', //目的区县
        'to_town', //目的乡/镇/街道
        'pack_name', //集包名
        'first_code', //一段码
        'trans_center_name', //转运中心
        'trans_center_code', //转运中心
    ];

    const FILE_STORE_NAME = 'special_route.php';

    private $tableName = 'special_pack_route';

    private $sqlPrefix = '';


    protected function build()
    {
        $this->columnLength = count(static::COLUMNS);
        $this->buildCodeBaseArray();
        $this->buildSqlField();
    }

    /**
     * buildSqlField
     * @return string
     */
    private function buildSqlField() : string
    {
        return $this->sqlPrefix = sprintf('INSERT INTO `%s` (`'.implode('`,`', static::COLUMNS).'`) VALUES ', $this->tableName);
    }

    /**
     * @param array $data
     * @return string
     */
    private function formatInfo(array $data) : string
    {
        unset($data[static::USELESS_FIELD]);
        return '("'.implode('","', array_slice($data, ...static::SLICE_PARAMS)).'"),';
    }

    /**
     * @return array
     */
    private function buildCodeBaseArray() : array
    {
        $data = [];
        foreach ($this->codeArray as $item) {
            if(count($item) <= $this->columnLength){
                $this->printErrorInfo($item);
                continue;
            }
           $data[] = $this->formatInfo($item);
        }
        $last = trim(array_pop($data), ',');
        $data[] = $last.';';
        return $this->codeBaseArray = $data;
    }

    /**
     *
     */
    protected function init()
    {
        $this->toArray();
    }

    /**
     * @return array
     */
    protected function toFile(): array
    {
        $result = [];
        if ($this->codeBaseArray && is_array($this->codeBaseArray)) {
            $result[] = file_put_contents(static::FILE_STORE_NAME, $this->sqlPrefix.implode("\n", $this->codeBaseArray));
        }
        return $result;
    }

}


$filePath = __DIR__.DIRECTORY_SEPARATOR.'route.csv';
(new StoRouteTableBuilder($filePath))->run();
