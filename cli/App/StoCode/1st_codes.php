<?php

return	array (
  100001 => 
  array (
    'trans_center_code' => '100001',
    'trans_center_name' => '北京转运中心',
    'province' => '北京',
    '1st_code_count' => 13,
    'codes' => 
    array (
      '北京市内包' => '100',
      '北京中转河北包' => '101',
      '北京中转内蒙古包' => '102',
      '北京中转其他省份包' => '103',
      '唐山包' => '104',
      '大连包' => '203',
      '保定包' => '133',
      '张家口包' => '105',
      '廊坊包' => '106',
      '秦皇岛包' => '107',
      '承德包' => '108',
      '北京朝阳包' => '109',
      '北京昌平包' => '110',
    ),
  ),
  300006 => 
  array (
    'trans_center_code' => '300006',
    'trans_center_name' => '天津转运中心',
    'province' => '天津',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '天津中转包' => '120',
    ),
  ),
  150019 => 
  array (
    'trans_center_code' => '150019',
    'trans_center_name' => '石家庄转运中心',
    'province' => '河北',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '石家庄中转包' => '130',
      '石家庄市内包' => '131',
      '保定包' => '133',
      '邯郸包' => '132',
    ),
  ),
  130014 => 
  array (
    'trans_center_code' => '130014',
    'trans_center_name' => '太原转运中心',
    'province' => '山西',
    '1st_code_count' => 6,
    'codes' => 
    array (
      '太原中转包' => '140',
      '太原市内包' => '141',
      '太原中转运城包' => '142',
      '大同包' => '143',
      '临汾包' => '144',
      '长治包' => '145',
    ),
  ),
  110080 => 
  array (
    'trans_center_code' => '110080',
    'trans_center_name' => '呼和浩特转运中心',
    'province' => '内蒙古',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '呼和浩特中转包' => '170',
      '呼和浩特市内包' => '171',
      '包头包' => '172',
    ),
  ),
  110020 => 
  array (
    'trans_center_code' => '110020',
    'trans_center_name' => '沈阳转运中心',
    'province' => '辽宁',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '沈阳中转包' => '200',
      '沈阳市内包' => '201',
      '大连包' => '203',
      '鞍山包' => '202',
    ),
  ),
  124001 => 
  array (
    'trans_center_code' => '124001',
    'trans_center_name' => '盘锦转运中心',
    'province' => '辽宁',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '盘锦中转包' => '210',
      '营口包' => '211',
    ),
  ),
  130002 => 
  array (
    'trans_center_code' => '130002',
    'trans_center_name' => '长春转运中心',
    'province' => '吉林',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '长春中转包' => '220',
      '长春市内包' => '221',
      '吉林包' => '222',
      '延吉包' => '223',
    ),
  ),
  150004 => 
  array (
    'trans_center_code' => '150004',
    'trans_center_name' => '哈尔滨转运中心',
    'province' => '黑龙江',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '哈尔滨中转包' => '240',
      '哈尔滨市内包' => '241',
      '大庆包' => '242',
    ),
  ),
  154003 => 
  array (
    'trans_center_code' => '154003',
    'trans_center_name' => '佳木斯转运中心',
    'province' => '黑龙江',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '佳木斯中转包' => '250',
    ),
  ),
  200019 => 
  array (
    'trans_center_code' => '200019',
    'trans_center_name' => '上海转运中心',
    'province' => '上海',
    '1st_code_count' => 8,
    'codes' => 
    array (
      '上海中转包' => '300',
      '嘉定包' => '301',
      '昆山包' => '403',
      '宝山包' => '302',
      '青浦包' => '303',
      '松江包' => '304',
      '太仓包' => '305',
      '金山包' => '306',
    ),
  ),
  200028 => 
  array (
    'trans_center_code' => '200028',
    'trans_center_name' => '浦东转运中心',
    'province' => '上海',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '浦东中转包' => '310',
      '奉贤包' => '311',
    ),
  ),
  310001 => 
  array (
    'trans_center_code' => '310001',
    'trans_center_name' => '杭州转运中心',
    'province' => '浙江',
    '1st_code_count' => 11,
    'codes' => 
    array (
      '杭州中转包' => '320',
      '杭州市内包' => '321',
      '绍兴包' => '322',
      '萧山包' => '323',
      '杭州下沙包' => '324',
      '湖州包' => '325',
      '余姚包' => '343',
      '慈溪包' => '342',
      '诸暨包' => '326',
      '临平包' => '327',
      '上虞包' => '328',
    ),
  ),
  315002 => 
  array (
    'trans_center_code' => '315002',
    'trans_center_name' => '宁波转运中心',
    'province' => '浙江',
    '1st_code_count' => 8,
    'codes' => 
    array (
      '宁波中转包' => '336',
      '江东包' => '337',
      '舟山包' => '338',
      '镇海包' => '339',
      '宁波下应包' => '340',
      '慈溪包' => '342',
      '余姚包' => '343',
      '望春包' => '341',
    ),
  ),
  333333 => 
  array (
    'trans_center_code' => '333333',
    'trans_center_name' => '临海转运中心',
    'province' => '浙江',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '临海中转包' => '346',
      '温岭包' => '347',
      '椒江包' => '348',
    ),
  ),
  325111 => 
  array (
    'trans_center_code' => '325111',
    'trans_center_name' => '温州转运中心',
    'province' => '浙江',
    '1st_code_count' => 6,
    'codes' => 
    array (
      '温州中转包' => '350',
      '温州转福鼎包' => '351',
      '温州市内包' => '352',
      '苍南包' => '353',
      '乐清包' => '354',
      '瑞安包' => '355',
    ),
  ),
  322001 => 
  array (
    'trans_center_code' => '322001',
    'trans_center_name' => '义乌转运中心',
    'province' => '浙江',
    '1st_code_count' => 5,
    'codes' => 
    array (
      '义乌中转包' => '360',
      '义乌包' => '361',
      '金华包' => '362',
      '东阳包' => '363',
      '永康包' => '364',
    ),
  ),
  314501 => 
  array (
    'trans_center_code' => '314501',
    'trans_center_name' => '嘉兴转运中心',
    'province' => '浙江',
    '1st_code_count' => 5,
    'codes' => 
    array (
      '嘉兴中转包' => '370',
      '嘉兴包' => '371',
      '海宁包' => '372',
      '桐乡包' => '373',
      '嘉善包' => '374',
    ),
  ),
  324001 => 
  array (
    'trans_center_code' => '324001',
    'trans_center_name' => '衢州转运中心',
    'province' => '浙江',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '衢州中转包' => '380',
    ),
  ),
  214001 => 
  array (
    'trans_center_code' => '214001',
    'trans_center_name' => '无锡转运中心',
    'province' => '江苏',
    '1st_code_count' => 8,
    'codes' => 
    array (
      '江阴中转包' => '400',
      '无锡市内包' => '401',
      '江阴包' => '402',
      '昆山包' => '403',
      '镇江包' => '404',
      '宜兴包' => '405',
      '丹阳包' => '406',
      '张家港包' => '407',
    ),
  ),
  213002 => 
  array (
    'trans_center_code' => '213002',
    'trans_center_name' => '常州转运中心',
    'province' => '江苏',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '常州中转包' => '410',
      '常州包' => '411',
    ),
  ),
  225410 => 
  array (
    'trans_center_code' => '225410',
    'trans_center_name' => '泰州转运中心',
    'province' => '江苏',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '泰州中转包' => '416',
      '扬州包' => '417',
      '泰州包' => '418',
    ),
  ),
  226001 => 
  array (
    'trans_center_code' => '226001',
    'trans_center_name' => '南通转运中心',
    'province' => '江苏',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '南通中转包' => '420',
      '南通包' => '421',
      '通州包' => '422',
      '海门包' => '423',
    ),
  ),
  210002 => 
  array (
    'trans_center_code' => '210002',
    'trans_center_name' => '南京转运中心',
    'province' => '江苏',
    '1st_code_count' => 5,
    'codes' => 
    array (
      '南京中转包' => '426',
      '南京包' => '427',
      '南京江宁包' => '428',
      '南京浦口包' => '429',
      '马鞍山包' => '477',
    ),
  ),
  223300 => 
  array (
    'trans_center_code' => '223300',
    'trans_center_name' => '淮安转运中心',
    'province' => '江苏',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '淮安中转包' => '436',
      '淮安包' => '437',
      '连云港包' => '438',
      '日照包' => '514',
    ),
  ),
  221001 => 
  array (
    'trans_center_code' => '221001',
    'trans_center_name' => '徐州转运中心',
    'province' => '江苏',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '徐州中转包' => '446',
      '徐州转山东包' => '447',
      '徐州转商丘包' => '448',
      '徐州市内包' => '449',
    ),
  ),
  215001 => 
  array (
    'trans_center_code' => '215001',
    'trans_center_name' => '苏州转运中心',
    'province' => '江苏',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '苏州中转包' => '456',
      '苏州包' => '457',
      '常熟包' => '458',
      '吴江包' => '459',
    ),
  ),
  224203 => 
  array (
    'trans_center_code' => '224203',
    'trans_center_name' => '盐城转运中心',
    'province' => '江苏',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '盐城中转包' => '466',
      '盐城市内包' => '467',
    ),
  ),
  230002 => 
  array (
    'trans_center_code' => '230002',
    'trans_center_name' => '合肥转运中心',
    'province' => '安徽',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '合肥中转包' => '470',
      '合肥市内包' => '471',
    ),
  ),
  241001 => 
  array (
    'trans_center_code' => '241001',
    'trans_center_name' => '芜湖转运中心',
    'province' => '安徽',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '芜湖中转包' => '476',
      '马鞍山包' => '477',
    ),
  ),
  246002 => 
  array (
    'trans_center_code' => '246002',
    'trans_center_name' => '安庆转运中心',
    'province' => '安徽',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '安庆中转包' => '480',
      '安庆包' => '481',
    ),
  ),
  233001 => 
  array (
    'trans_center_code' => '233001',
    'trans_center_name' => '蚌埠转运中心',
    'province' => '安徽',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '蚌埠中转包' => '486',
      '蚌埠包' => '487',
      '淮南包' => '488',
    ),
  ),
  236002 => 
  array (
    'trans_center_code' => '236002',
    'trans_center_name' => '阜阳转运中心',
    'province' => '安徽',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '阜阳中转包' => '490',
    ),
  ),
  250001 => 
  array (
    'trans_center_code' => '250001',
    'trans_center_name' => '济南转运中心',
    'province' => '山东',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '济南中转包' => '500',
      '济南市内包' => '501',
    ),
  ),
  273101 => 
  array (
    'trans_center_code' => '273101',
    'trans_center_name' => '曲阜转运中心',
    'province' => '山东',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '曲阜中转包' => '507',
      '济宁包' => '508',
    ),
  ),
  261001 => 
  array (
    'trans_center_code' => '261001',
    'trans_center_name' => '潍坊转运中心',
    'province' => '山东',
    '1st_code_count' => 7,
    'codes' => 
    array (
      '潍坊中转包' => '512',
      '潍坊市内包' => '513',
      '日照包' => '514',
      '东营包' => '515',
      '淄博包' => '516',
      '烟台包' => '517',
      '威海包' => '518',
    ),
  ),
  276001 => 
  array (
    'trans_center_code' => '276001',
    'trans_center_name' => '临沂转运中心',
    'province' => '山东',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '临沂中转包' => '520',
      '临沂市内包' => '521',
    ),
  ),
  266011 => 
  array (
    'trans_center_code' => '266011',
    'trans_center_name' => '青岛转运中心',
    'province' => '山东',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '青岛中转包' => '523',
      '青岛市内包' => '524',
    ),
  ),
  265204 => 
  array (
    'trans_center_code' => '265204',
    'trans_center_name' => '莱阳转运中心',
    'province' => '山东',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '莱阳中转包' => '528',
    ),
  ),
  350001 => 
  array (
    'trans_center_code' => '350001',
    'trans_center_name' => '福州转运中心',
    'province' => '福建',
    '1st_code_count' => 5,
    'codes' => 
    array (
      '福州中转包' => '530',
      '福州市内包' => '531',
      '福清包' => '532',
      '长乐包' => '533',
      '闽侯包' => '534',
    ),
  ),
  362021 => 
  array (
    'trans_center_code' => '362021',
    'trans_center_name' => '泉州转运中心',
    'province' => '福建',
    '1st_code_count' => 6,
    'codes' => 
    array (
      '泉州中转包' => '538',
      '龙岩包' => '539',
      '泉州市内包' => '540',
      '莆田包' => '541',
      '石狮包' => '542',
      '晋江包' => '543',
    ),
  ),
  363001 => 
  array (
    'trans_center_code' => '363001',
    'trans_center_name' => '漳州转运中心',
    'province' => '福建',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '漳州中转包' => '548',
    ),
  ),
  361001 => 
  array (
    'trans_center_code' => '361001',
    'trans_center_name' => '厦门转运中心',
    'province' => '福建',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '厦门中转包' => '552',
    ),
  ),
  365501 => 
  array (
    'trans_center_code' => '365501',
    'trans_center_name' => '沙县转运中心',
    'province' => '福建',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '沙县中转包' => '556',
    ),
  ),
  352118 => 
  array (
    'trans_center_code' => '352118',
    'trans_center_name' => '宁德转运中心',
    'province' => '福建',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '宁德中转包' => '560',
    ),
  ),
  330001 => 
  array (
    'trans_center_code' => '330001',
    'trans_center_name' => '南昌转运中心',
    'province' => '江西',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '南昌中转包' => '570',
      '南昌市内包' => '571',
      '九江地区包' => '572',
    ),
  ),
  341001 => 
  array (
    'trans_center_code' => '341001',
    'trans_center_name' => '赣州转运中心',
    'province' => '江西',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '赣州中转包' => '576',
      '赣州市内包' => '577',
    ),
  ),
  330080 => 
  array (
    'trans_center_code' => '330080',
    'trans_center_name' => '鹰潭转运中心',
    'province' => '江西',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '鹰潭中转包' => '580',
    ),
  ),
  336501 => 
  array (
    'trans_center_code' => '336501',
    'trans_center_name' => '新余转运中心',
    'province' => '江西',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '新余中转包' => '585',
    ),
  ),
  510002 => 
  array (
    'trans_center_code' => '510002',
    'trans_center_name' => '广州转运中心',
    'province' => '广东',
    '1st_code_count' => 5,
    'codes' => 
    array (
      '广州中转包' => '600',
      '广州市内包' => '601',
      '中山包' => '602',
      '珠海包' => '603',
      '韶关包' => '604',
    ),
  ),
  510171 => 
  array (
    'trans_center_code' => '510171',
    'trans_center_name' => '共和转运中心',
    'province' => '广东',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '共和中转包' => '610',
      '湛江中转包' => '611',
      '茂名包' => '612',
      '阳江包' => '613',
    ),
  ),
  529601 => 
  array (
    'trans_center_code' => '529601',
    'trans_center_name' => '揭阳转运中心',
    'province' => '广东',
    '1st_code_count' => 6,
    'codes' => 
    array (
      '揭阳中转包' => '620',
      '潮阳包' => '621',
      '潮州包' => '622',
      '揭阳包' => '623',
      '汕头包' => '624',
      '普宁包' => '625',
    ),
  ),
  514002 => 
  array (
    'trans_center_code' => '514002',
    'trans_center_name' => '梅州转运中心',
    'province' => '广东',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '梅州中转包' => '630',
    ),
  ),
  518003 => 
  array (
    'trans_center_code' => '518003',
    'trans_center_name' => '深圳转运中心',
    'province' => '广东',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '深圳中转包' => '634',
      '深圳市内包' => '635',
      '深圳宝安包' => '636',
      '深圳龙岗包' => '637',
    ),
  ),
  516010 => 
  array (
    'trans_center_code' => '516010',
    'trans_center_name' => '惠州转运中心',
    'province' => '广东',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '惠州中转包' => '641',
      '惠州包' => '642',
    ),
  ),
  528001 => 
  array (
    'trans_center_code' => '528001',
    'trans_center_name' => '佛山转运中心',
    'province' => '广东',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '佛山中转包' => '644',
      '佛山市内包' => '645',
    ),
  ),
  511784 => 
  array (
    'trans_center_code' => '511784',
    'trans_center_name' => '东莞转运中心',
    'province' => '广东',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '东莞市内包' => '650',
      '汕尾包' => '651',
      '河源中转包' => '652',
    ),
  ),
  530002 => 
  array (
    'trans_center_code' => '530002',
    'trans_center_name' => '南宁转运中心',
    'province' => '广西',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '南宁中转包' => '660',
      '南宁市内包' => '661',
      '南宁中转玉林包' => '662',
    ),
  ),
  540013 => 
  array (
    'trans_center_code' => '540013',
    'trans_center_name' => '柳州转运中心',
    'province' => '广西',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '柳州中转包' => '666',
      '柳州市内包' => '667',
    ),
  ),
  541002 => 
  array (
    'trans_center_code' => '541002',
    'trans_center_name' => '桂林转运中心',
    'province' => '广西',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '桂林中转包' => '670',
      '桂林市内包' => '671',
    ),
  ),
  570003 => 
  array (
    'trans_center_code' => '570003',
    'trans_center_name' => '海口转运中心',
    'province' => '海南',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '海口中转包' => '680',
      '海口包' => '681',
      '三亚中转包' => '682',
    ),
  ),
  410001 => 
  array (
    'trans_center_code' => '410001',
    'trans_center_name' => '长沙转运中心',
    'province' => '湖南',
    '1st_code_count' => 6,
    'codes' => 
    array (
      '长沙中转包' => '700',
      '长沙市内包' => '701',
      '岳阳包' => '702',
      '湘潭包' => '703',
      '株洲包' => '704',
      '长沙岳麓包' => '705',
    ),
  ),
  421201 => 
  array (
    'trans_center_code' => '421201',
    'trans_center_name' => '衡阳转运中心',
    'province' => '湖南',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '衡阳中转包' => '710',
      '郴州包' => '711',
      '永州包' => '712',
      '衡阳包' => '713',
    ),
  ),
  415002 => 
  array (
    'trans_center_code' => '415002',
    'trans_center_name' => '常德转运中心',
    'province' => '湖南',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '常德中转包' => '720',
      '常德市内包' => '721',
    ),
  ),
  430001 => 
  array (
    'trans_center_code' => '430001',
    'trans_center_name' => '武汉转运中心',
    'province' => '湖北',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '武汉中转包' => '730',
      '武汉市内包' => '731',
      '武昌包' => '732',
    ),
  ),
  434001 => 
  array (
    'trans_center_code' => '434001',
    'trans_center_name' => '荆州转运中心',
    'province' => '湖北',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '荆州中转包' => '736',
      '宜昌包' => '737',
    ),
  ),
  441001 => 
  array (
    'trans_center_code' => '441001',
    'trans_center_name' => '襄阳转运中心',
    'province' => '湖北',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '襄阳中转包' => '740',
      '襄樊包' => '741',
    ),
  ),
  436000 => 
  array (
    'trans_center_code' => '436000',
    'trans_center_name' => '武昌转运中心',
    'province' => '湖北',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '鄂州中转包' => '746',
    ),
  ),
  450003 => 
  array (
    'trans_center_code' => '450003',
    'trans_center_name' => '郑州转运中心',
    'province' => '河南',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '郑州中转包' => '760',
      '郑州市内包' => '761',
    ),
  ),
  471001 => 
  array (
    'trans_center_code' => '471001',
    'trans_center_name' => '洛阳转运中心',
    'province' => '河南',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '洛阳中转包' => '766',
      '洛阳市内包' => '767',
      '洛阳中转运城包' => '768',
    ),
  ),
  462001 => 
  array (
    'trans_center_code' => '462001',
    'trans_center_name' => '漯河转运中心',
    'province' => '河南',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '漯河中转包' => '770',
      '南阳包' => '771',
      '信阳包' => '772',
      '驻马店包' => '773',
    ),
  ),
  476001 => 
  array (
    'trans_center_code' => '476001',
    'trans_center_name' => '商丘转运中心',
    'province' => '河南',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '商丘中转包' => '780',
    ),
  ),
  610003 => 
  array (
    'trans_center_code' => '610003',
    'trans_center_name' => '成都转运中心',
    'province' => '四川',
    '1st_code_count' => 8,
    'codes' => 
    array (
      '成都中转包' => '800',
      '成都市内包' => '801',
      '雅乐地区件' => '802',
      '德阳包' => '803',
      '郫县包' => '804',
      '绵阳包' => '805',
      '成都转昌都包' => '806',
      '成都转拉萨包' => '807',
    ),
  ),
  637001 => 
  array (
    'trans_center_code' => '637001',
    'trans_center_name' => '南充转运中心',
    'province' => '四川',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '南充中转包' => '820',
      '南充市内包' => '821',
    ),
  ),
  643001 => 
  array (
    'trans_center_code' => '643001',
    'trans_center_name' => '自贡转运中心',
    'province' => '四川',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '自贡中转包' => '830',
    ),
  ),
  550003 => 
  array (
    'trans_center_code' => '550003',
    'trans_center_name' => '贵阳转运中心',
    'province' => '贵州',
    '1st_code_count' => 3,
    'codes' => 
    array (
      '贵阳中转包' => '840',
      '贵阳市内包' => '841',
      '遵义包' => '842',
    ),
  ),
  650222 => 
  array (
    'trans_center_code' => '650222',
    'trans_center_name' => '昆明转运中心',
    'province' => '云南',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '昆明中转包' => '850',
      '昆明市内包' => '851',
      '大理包' => '852',
      '红河地区包' => '853',
    ),
  ),
  630050 => 
  array (
    'trans_center_code' => '630050',
    'trans_center_name' => '重庆转运中心',
    'province' => '重庆',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '重庆中转包' => '860',
      '重庆市内包' => '861',
    ),
  ),
  404100 => 
  array (
    'trans_center_code' => '404100',
    'trans_center_name' => '万州转运中心',
    'province' => '重庆',
    '1st_code_count' => 1,
    'codes' => 
    array (
      '万州中转包' => '866',
    ),
  ),
  710001 => 
  array (
    'trans_center_code' => '710001',
    'trans_center_name' => '西安转运中心',
    'province' => '陕西',
    '1st_code_count' => 7,
    'codes' => 
    array (
      '西安中转包' => '900',
      '西安市内包' => '901',
      '咸阳包' => '902',
      '汉中包' => '903',
      '宝鸡包' => '904',
      '延安地区包' => '905',
      '西安中转运城包' => '906',
    ),
  ),
  730015 => 
  array (
    'trans_center_code' => '730015',
    'trans_center_name' => '兰州转运中心',
    'province' => '甘肃',
    '1st_code_count' => 4,
    'codes' => 
    array (
      '兰州中转包' => '920',
      '兰州市内包' => '921',
      '河西中转包' => '922',
      '天水包' => '923',
    ),
  ),
  740002 => 
  array (
    'trans_center_code' => '740002',
    'trans_center_name' => '银川转运中心',
    'province' => '宁夏',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '银川中转包' => '930',
      '银川市内包' => '931',
    ),
  ),
  810002 => 
  array (
    'trans_center_code' => '810002',
    'trans_center_name' => '西宁转运中心',
    'province' => '青海',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '西宁中转包' => '940',
      '西宁市内包' => '941',
    ),
  ),
  830001 => 
  array (
    'trans_center_code' => '830001',
    'trans_center_name' => '乌鲁木齐转运中心',
    'province' => '新疆',
    '1st_code_count' => 2,
    'codes' => 
    array (
      '乌鲁木齐中转包' => '950',
      '乌鲁木齐市内包' => '951',
    ),
  ),
);