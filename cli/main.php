<?php
define('ROOT_PATH', __DIR__ . DIRECTORY_SEPARATOR);
const LIB_PATH = ROOT_PATH . 'Libs' . DIRECTORY_SEPARATOR;
const APP_PATH = ROOT_PATH . 'App' . DIRECTORY_SEPARATOR;
const TESTING_PATH = ROOT_PATH . 'Testing' . DIRECTORY_SEPARATOR;
const VENDOR_PATH = ROOT_PATH . 'vendor' . DIRECTORY_SEPARATOR;
require VENDOR_PATH.'autoload.php';

class main
{

    /**
     * init handler
     *
     * @return void
     */
    public static function run()
    {
        spl_autoload_register([__CLASS__, 'autoloader']);
    }

    /**
     * autoloader
     *
     * @param  string $class
     * @return void
     */
    public static function autoloader($classname)
    {
        $paths = [
            APP_PATH,
            TESTING_PATH,
            LIB_PATH,
            ROOT_PATH,
        ];
        foreach ($paths as $path) {
            $filename = $path . $classname . ".php";
            if (file_exists($filename)) {
                include $filename;
                break;
            }
        }
    }
}
