var page = require('webpage').create(),
    system = require('system'),
    response = {},
    now = Date.now(),
    extend = function(target) {
        var sources = [].slice.call(arguments, 1);
        sources.forEach(function (source) {
            for (var prop in source) {
                target[prop] = source[prop];
            }
        });
        return target;
    };
page.onConsoleMessage = function(msg, lineNum, sourceId) {
    console.log(msg);
};

response.headers = JSON.parse('{"DNT":1,"Visitor":"Appla"}' || '{}');

page.customHeaders = {
    "DNT": "1",
    "test": 'Appla',
    "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1",
};

page.customHeaders = extend({}, page.customHeaders, response.headers);

var url = 'https://passport.jd.com/new/login.aspx';
// var url = 'http://bhgit.com/appla/index.php?debug=1';
page.open(url);
// page.open('https://passport.jd.com/new/login.aspx', function (status) {
//     var cookies = page.cookies;
//     response.content = {
//         cookies : {}
//     };
//     for(var i in cookies) {
//         response.content.cookies[cookies[i].name] = cookies[i].value;
//     }
//     response.status = status;
//     setTimeout(function(){
//         response.content.inputs = page.evaluate(function() {
//             var tmp = {};
//             tmp['uuid'] = document.getElementById("uuid").value;
//             tmp['eid'] = document.getElementById("eid").value;
//             tmp['sessionId'] = document.getElementById("sessionId").value;
//             return tmp;
//         });
//         response.time = Date.now() - now;
//         system.stdout.write(JSON.stringify(window.response));
//         phantom.exit();
//     },600);
// });

page.onLoadFinished = function (status) {
    // console.log(page.content);
    // phantom.exit();
    var cookies = page.cookies;
    response.content = {
        cookies : {}
    };
    for(var i in cookies) {
        response.content.cookies[cookies[i].name] = cookies[i].value;
    }
    response.status = status;
    setTimeout(function(){
        response.content.inputs = page.evaluate(function() {
            var tmp = {},
                uuid = document.getElementById("uuid"),
                eid = document.getElementById("eid"),
                sessionId = document.getElementById("sessionId"),
                token = document.getElementById("token"),
                pubKey = document.getElementById("pubKey"),
                last = document.getElementById("formlogin").lastChild;

            tmp[uuid.name] = uuid.value;
            tmp[eid.name] = eid.value;
            tmp[sessionId.name] = sessionId.value;
            tmp[token.name] = token.value;
            tmp[pubKey.name] = pubKey.value;
            tmp[last.name] = last.value;
            return tmp;
        });
        response.time = Date.now() - now;
        system.stdout.write(JSON.stringify(window.response));
        phantom.exit();
    },600);
}
