// phantom.onError = function(msg, trace) {
//     var msgStack = ['PHANTOM ERROR: ' + msg];
//     if (trace && trace.length) {
//         msgStack.push('TRACE:');
//         trace.forEach(function(t) {
//             msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? '(in function' + t.function + ')' : ''));
//         });
//     }
//     console.error(msgStack.join('\n'));
//     phantom.exit(1);
// };
var webPage = require('webpage'),
    system = require('system');
// fs = require('fs');
var page = webPage.create(),
    args = system.args,
    url;

if (args.length === 1) {
    url = 'http://phantomjs.org';
}else if(args.length === 2) {
    url = args[1];
}

if(!url){
    console.log('url is error' + url);
    phantom.exit();
}
function click(el){
    var ev = document.createEvent("MouseEvent");
    ev.initMouseEvent(
        "click",
        true /* bubble */, true /* cancelable */,
        window, null,
        0, 0, 0, 0, /* coordinates */
        false, false, false, false, /* modifier keys */
        0 /*left*/, null
    );
    el.dispatchEvent(ev);
}
page.onConsoleMessage = function(msg, lineNum, sourceId) {
    //console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
    console.log(msg);
};
page.open(url, function (status) {
    setTimeout(function(){
    page.evaluate(function() {
    // #uuid
    // #eid
    // #sessionId
    // #token
    // #loginType
    // #pubKey
        var inputs = {};
        inputs['uuid'] = document.getElementById("uuid").value;
        inputs['eid'] = document.getElementById("eid").value;
        inputs['sessionId'] = document.getElementById("sessionId").value;
        console.log(JSON.stringify(inputs));
    });
        // var content = page.content;
        // console.log('Content: ' + content);
        phantom.exit();
    },600);
});
