<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'main.php';
main::run();
/**
 *  QueryList使用示例
 *
 * 入门教程:http://doc.querylist.cc/site/index/doc/4
 *
 * QueryList::Query(采集的目标页面,采集规则[,区域选择器][，输出编码][，输入编码][，是否移除头部])
* //采集规则
* $rules = array(
*   '规则名' => array('jQuery选择器','要采集的属性'[,"标签过滤列表"][,"回调函数"]),
*   '规则名2' => array('jQuery选择器','要采集的属性'[,"标签过滤列表"][,"回调函数"]),
*    ..........
*    [,"callback"=>"全局回调函数"]
* );
 */

use Querylist\QueryList;

#formlogin

#uuid
#eid
#sessionId
#token
#loginType
#pubKey
#formlogin > input[type="hidden"]:nth-child(7)

#loginname  登录名
#nloginpwd  pass
#autoLogin 自动登录???
#authcode 验证码?

$url = 'https://passport.jd.com/new/login.aspx';
$rules = [
    'uuid' => ['#uuid','value'],
    'eid' => ['#eid','value'],
    'sessionId' => ['#sessionId','value'],
    'token' => ['#token','value'],
    'loginType' => ['#loginType','value'],
    'pubKey' => ['#pubKey','value'],
    'formlogin' => ['#formlogin > input[type="hidden"]:nth-child(7)','value'],
    'loginname' => ['#loginname','value'],
    'nloginpwd' => ['#nloginpwd','value'],
    'autoLogin' => ['#autoLogin','value'],
    'authcode' => ['#authcode','value'],
];
$range = '#formlogin';
$data = QueryList::Query($url, $rules, $range)->getData();

print_r($data);
exit;
/*************************************************/

//使用插件
$urls = QueryList::run('Request',array(
        'target' => 'http://cms.querylist.cc/news/list_2.html',
        'referrer'=>'http://cms.querylist.cc',
        'method' => 'GET',
        'params' => ['var1' => 'testvalue', 'var2' => 'somevalue'],
        'user_agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0',
        'cookiePath' => './cookie.txt',
        'timeout' =>'30'
    ))->setQuery(array('link' => array('h2>a','href','',function($content){
    //利用回调函数补全相对链接
    $baseUrl = 'http://cms.querylist.cc';
    return $baseUrl.$content;
})),'.cate_list li')->getData(function($item){
    return $item['link'];
});

print_r($urls);
