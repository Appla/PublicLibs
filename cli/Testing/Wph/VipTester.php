<?php
namespace Wph;

use CookieAuth\BaseTester;
use HttpRequester\CurlFacade;

class VipTester extends BaseTester
{
    public static $host = 'http://m.vip.com';
    protected $request_url = 'http://m.vip.com/user-order-list-unreceive.html';

    public function getCurlConfig(array $config = [])
    {
        return [];
    }

    public function getHeaders(array $header = [])
    {
        $data = [
            // 'Connection: keep-alive',
            'Pragma: no-cache',
            'Cache-Control: no-cache',
            // 'Upgrade-Insecure-Requests: 1',
            'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
            // 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            // 'DNT: 1',
            // 'Referer: http://m.vip.com/user.html?islogin=1',
            // 'Accept-Encoding: gzip, deflate, sdch',
            'Accept-Language: zh-CN,zh;q=0.8,en;q=0.6',
            // 'Cookie: vip_first_visitor=1; vip_province=103101; vip_province_name=%E4%B8%8A%E6%B5%B7; vip_city_name=%E4%B8%8A%E6%B5%B7%E5%B8%82; vip_city_code=103101101; vip_wh=VIP_SH; vip_ipver=31; tmp_mars_cid=1475042706000_6b85cb7259ce4616056bf959618d3396; user_class=a; VipUINFO=luc%3Aa%7Csuc%3Aa%7Cbct%3Ac_new%7Ckct%3Ac_new%7Chct%3Ac_new%7Cbdts%3A0%7Cbcts%3A0%7Ckfts%3A0%7Cc10%3A0%7Crcabt%3A0%7Cp2%3A0%7Cp3%3A0%7Cp4%3A0%7Cp5%3A0; PHPSESSID=80e4a7de5db72939757a5b9eb6f478fe; _adwc=41626277; _adwp=41626277.6899959269.1475042711.1475042711.1475042711.1; _smt_uid=57eb5d97.4e3b7254; _mj_si=si3666102272; _adwr=123520138%23http%253A%252F%252Fwww.vip.com%252F; _jzqco=%7C%7C%7C%7C%7C1.235274042.1475042711361.1475042712813.1475043529587.1475042712813.1475043529587.0.0.0.3.3; WAP_ID=5f7dcf1d3a9167efcf7bb3edb295a990495cdc89; time_offset=1; WAP[hd]=1; WAP[p_area]=%25E4%25B8%258A%25E6%25B5%25B7; WEIXIN_PROVINCE=%E4%B8%8A%E6%B5%B7; m_vip_province=103101; WEIXIN_PROVINCEID=103101; WAP[oxo_area]=103101%2C103101101%2C; m_vipUINFO=newCustomerGuide%3AB%7Cop_rName%3AWAP; WAP[%2Findex.html]=1; WAP[p_wh]=VIP_SH; warehouse=VIP_SH; WEIXIN_WAREHOUSE=VIP_SH; m_vipruid=205907112; _mj_c=v3.3,last,1475043632444,1475042711937,315360000000|v3.3,cm,1475042712044,1475042711937,315360000000; saturn=v032aaeca22c68d6f29fa6e69749d57b2; triton=Wm9VNFI3C2tXPAE3AWIAZwQ3VzIGNg%3D%3D%3A978D108F7C848A8F143A6DC4F18403B81966C9BE; rhea=zzzOeiFthTdqqpCgzAbR1SLP20cNSethpx4sxM91eY0KWNcdrfhmWd-DoroqjP7qrw5TvjznwemzeHbJGMfKFkRZsO9ocga8VKGOzLc1wBkaBPRRsALSZs_Jl6Cf6GYy8jJiW7XNxe-CtY8CnKehmw; m_vipruid=205907112; m_vipuuid=205907112; m_viplid=1; WAP[back_act]=user-index.html; entryPage=1475048022178; WAP_u_new=newone; wap_consumer=B; mars_pid=54; mars_cid=1475042706000_6b85cb7259ce4616056bf959618d3396; mars_sid=1f4b3ce0ddfa210b0cb5477df927dde5; visit_id=978DB54D49C4C557F475628A8A3BC9A9',
            'Cookie: vip_first_visitor=1; vip_province=103101; vip_province_name=%E4%B8%8A%E6%B5%B7; vip_city_name=%E4%B8%8A%E6%B5%B7%E5%B8%82; vip_city_code=103101101; vip_wh=VIP_SH; vip_ipver=31; tmp_mars_cid=1475042706000_6b85cb7259ce4616056bf959618d3396; user_class=a; VipUINFO=luc%3Aa%7Csuc%3Aa%7Cbct%3Ac_new%7Ckct%3Ac_new%7Chct%3Ac_new%7Cbdts%3A0%7Cbcts%3A0%7Ckfts%3A0%7Cc10%3A0%7Crcabt%3A0%7Cp2%3A0%7Cp3%3A0%7Cp4%3A0%7Cp5%3A0; PHPSESSID=80e4a7de5db72939757a5b9eb6f478fe; _adwc=41626277; _adwp=41626277.6899959269.1475042711.1475042711.1475042711.1; _smt_uid=57eb5d97.4e3b7254; _adwr=123520138%23http%253A%252F%252Fwww.vip.com%252F; _jzqco=%7C%7C%7C%7C%7C1.235274042.1475042711361.1475042712813.1475043529587.1475042712813.1475043529587.0.0.0.3.3; WAP_ID=5f7dcf1d3a9167efcf7bb3edb295a990495cdc89; time_offset=1; WAP[hd]=1; WAP[p_area]=%25E4%25B8%258A%25E6%25B5%25B7; WEIXIN_PROVINCE=%E4%B8%8A%E6%B5%B7; m_vip_province=103101; WEIXIN_PROVINCEID=103101; WAP[oxo_area]=103101%2C103101101%2C; m_vipUINFO=newCustomerGuide%3AB%7Cop_rName%3AWAP; WAP[%2Findex.html]=1; WAP[p_wh]=VIP_SH; warehouse=VIP_SH; WEIXIN_WAREHOUSE=VIP_SH; m_channel_uuid=shop_wap_m; _mj_si=si3662034944; _mj_c=v3.3,last,1475049218021,1475042711937,315360000000|v3.3,cm,1475042712044,1475042711937,315360000000; saturn=va7944189be886fe448d773723d68cad7; triton=U2ZWN1U8BGNQNwU1BGVTNFpuVzoIPA%3D%3D%3A32D67FF9167B0698D26EEA5599E17D9B9C0F764F; rhea=IYcfY1euCj-ucqKM6EzSCLuSc9u70IrsnWvEWLWzg4eiDob1nDX7qGiLff2nphbEjGaPGSQUdhVBi5kA6qTR0Oi-s1nZkczSGh2v1jNLtSWSuANkgJaSo8883_df8H5_1LknR3G-6bYdO71Usknhig; m_vipruid=61630123; m_vipuuid=61630123; m_viplid=1; m_vipruid=61630123; WAP[user_back_act]=http%3A%2F%2Fm.vip.com%2Fuser-order-list.html; WAP[back_act]=user-index.html; entryPage=1475049423027; WAP_u_new=newone; wap_consumer=C2-3; mars_pid=95; mars_cid=1475042706000_6b85cb7259ce4616056bf959618d3396; mars_sid=1f4b3ce0ddfa210b0cb5477df927dde5; visit_id=978DB54D49C4C557F475628A8A3BC9A9',
        ];
        return array_merge($data, $header);
    }

}
