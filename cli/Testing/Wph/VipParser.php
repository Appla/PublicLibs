<?php
namespace Wph;

use QL\QueryList;

class VipParser
{
    public function parseOrderList($str)
    {
        $items_container = '';

        $rules = [
            'detail_url' => ['.userdiv > .orderlistdiv a.orderRlink', 'href'],
            'order_id' =>['.cart_orderlist_info > p:even','text'],
            'order_status' =>['.userdiv .cart_orderlist_info p:odd','text'],
            'order_price' =>['.userdiv > .orderlisttit .c-red','text'],
            'order_time' =>['.userdiv > .orderlisttit .fr','text'],
            'img_url' => ['.userdiv > .orderlistdiv img', 'data-original'],

            // 'detail_url' => ['.userdiv > .orderlistdiv > a.orderRlink', 'href'],
            // 'order_id' =>['.userdiv > .orderlistdiv > a > .cart_orderlist_info > p:even','text'],
            // 'order_status' =>['.userdiv >.orderlistdiv > a > .cart_orderlist_info p:odd','text'],
            // 'order_price' =>['.userdiv > .orderlisttit .c-red','text'],
            // 'order_time' =>['.userdiv > .orderlisttit > .fr','text'],
            // 'img_url' => ['.userdiv > .orderlistdiv > a > img', 'data-original'],
        ];
        return QueryList::query($str, $rules, $items_container)->getData();
    }

    public function parseOrderDetail($str)
    {

        $items_container = '';
        $rules = [
            'order_id' =>['.J-order-sn','data-order-sn'],
            'title' => ['.orderdeatilbox > .orderdeatil:eq(0) p.cart_g_name', 'text'],
            'express_detail_url' => ['.userdiv > div.orderdeatilbox:eq(0) > a.fr', 'href'],
            'receive_address' =>['#J-order-to-friend > p:eq(2)','text'],
            'receive_mobile' =>['#J-order-to-friend > p:eq(1)','text'],
            'receive_user' =>['#J-order-to-friend > p:eq(0)','text'],
        ];
        if($data = QueryList::query($str, $rules, $items_container)->getData()){
            isset($data[0]) and $data = $data[0];
        }
        return $data;
    }

    /**
     * parseExpressInfos
     *
     * @return array
     */
    public function parseExpressInfos($str)
    {
        $items_container = 'div.userdiv';
        $rules = [
            'order_id' => ['.order-logistics span.info:eq(0)', 'text', '', function($val){
                return preg_match('/([\w\d]+)/', $val, $match) ? $match[1] : $val;
            }],
            'arrive_at' => ['.order-logistics span.info:eq(1)', 'text'],
            'logistics' => ['.orderdeatilbox > p', 'text','', function($data){
                return preg_replace('/(\d{4}-\d{2}-\d{2}\s(?:\d{2}:){2}\d{2})/','|||$1   ', $data);
                // return preg_replace('/(\d{4}-\d{2}-\d{2}\s(?:\d{2}:){2}\d{2})/','###$1##', $data);
            }],
        ];
        if($data = QueryList::query($str, $rules, $items_container)->getData()){
            $data = $data[0];
            if(strpos($data['arrive_at'], '预计送达') === false){
                $data['arrive_at'] = preg_match('/商品预计.*送达/', $data['logistics'], $match) ? $match[0] : '';
            }
            $tmp = explode('物流公司名', $data['logistics']);
            $data['logistics'] = trim($tmp[0], '#');
            $data['express_info'] = '物流公司名'.str_replace('物流','###物流', $tmp[1]);
            $tmp = explode('###', $data['express_info']);
            $data['express_flow'] = implode('|||', array_reverse(explode('|||', trim($data['logistics'], '|'))));
            foreach ($tmp as $val) {
                $infos = preg_split('/：|:/i', $val);
                if(trim($infos[0]) === '物流公司名'){
                    $data['brand_name'] = $infos[1];
                }elseif(trim($infos[0]) === '物流单号'){
                    $data['express_number'] = $infos[1];
                }
            }
            // $data = $data[0];
            // $data['arrive_at'] = preg_match('/商品预计.*送达/', $data['logistics'], $match) ? $match[0] : '';
            // $tmp = explode('物流公司名', $data['logistics']);
            // $data['logistics'] = $tmp[0];
            // $data['express_info'] = '物流公司名'.str_replace('物流','##物流', $tmp[1]);
            // $tmp = explode('##', $data['express_info']);
            // foreach ($tmp as $val) {
            //     $infos = preg_split('/：|:/i', $val);
            //     if(trim($infos[0]) === '物流公司名'){
            //         $data['brand_name'] = $infos[1];
            //     }elseif(trim($infos[0]) === '物流单号'){
            //         $data['express_number'] = $infos[1];
            //     }
            // }
        }
        return $data;
    }
}
