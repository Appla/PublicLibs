<?php
namespace Amazon;

use CookieAuth\BaseTester;
use HttpRequester\CurlFacade;

class AmazonTester extends BaseTester
{
    public static $host = 'https://www.amazon.cn';
    protected $request_url = 'https://www.amazon.cn/gp/aw/ya/or';

    public function getHeaders(array $header = [])
    {
        $data = [
            'Cookie: session_id=57eb643f926b28401; cookie_uid=51d20bf7221475044417; account=f5g3hdL0eipSzxbaoMCZavnyqpzPfdYLxHQhi1Cmlxe4USubuCW66mXAKKXl9Tdp8UyGYv397Xre6lKuBfPTb6w%2B9bWrnE8jj9ZZsxp1xxtfa4Y3jjNgR0f551Hf7Z9r%2F89C2j%2BXOl1r9CnXv%2Btvh7YPwqmlmz8gmgxW6MbfF5CnjRnF0%2BPWDbBpnlLwigRXzcEtNOqN6bNUvsaZwOvysA%3D%3D; tk=5dd591041b64801811c022d48dae24debc0b7c3c; uid=113358522; v_uid=113358522; nickname=JM135AWWW0381; token=WNPIvVeuDKxo7I791bk0MJbGmg8aF33QY8HBr5h0LASps6CVyFTzn2ypZl4mBhKsDSLGkd9jwrevUfOzxTNtnXEXflWcjoi6OdM2wQRP5iEuUAYtJcgaZq14HDUFyRSL; session=jseknEpSg1h94J035UxbOuwdWzZOMDHH; cookie_ver=1; login_account_name=13564440381; last_reg=1475044622; privilege_group=0; register_time=1474859383',
            'User-Agent: Mozilla/5.0 (Linux; Android 6.0; Google Nexus 6 - 6.0.0 - API 23 - 1440x2560 Build/MRA58K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/44.0.2403.119 Mobile Safari/537.36',
        ];
        return array_merge($data, $header);
    }

}
