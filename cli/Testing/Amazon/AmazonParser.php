<?php
namespace Amazon;

use QL\QueryList;

class AmazonParser
{

    public function parseOrderList($str)
    {
        $host = 'https://www.amazon.cn';
        $items_container = '#filtered-orders';
        $rules = [
            'detail_url' => ['.order-group .a-unordered-list li:eq(4) a', 'href', '',function($url) use($host){
                return $host.$url;
            }],
            'order_id' =>['.order-group .a-unordered-list li:eq(0) a','href','' , function($url){
                parse_str($url, $urlArr);
                return $urlArr['orderId'];
            }],
            'title' =>['.order-group .a-span-last > span','text'],
            'seller' =>['.order-group .a-span-last > p > span','text', '', function($text){
                return str_replace('卖家： ', '', $text);
            }],
            'img_url' => ['.order-group .a-link-normal > img', 'src'],
            'order_status' => ['.order-group .a-spacing-top-base h3', 'text'],
        ];
        $data = QueryList::query($str, $rules, $items_container)->getData();
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $data[$key]['is_finished'] = in_array($val['order_status'], ['已退款', '已完成', '已过期', '全部退货']);
            }
        }
        return $data;
    }

    public function parseOrderDetail($str)
    {

        $host = 'https://www.amazon.cn';

        $items_container = '.a-container';

        $rules = [
            'express_detail_url' => ['.a-box-group > a', 'href', '',function($url) use($host){
                return $host.$url;
            }],
            'order_id' =>['div.a-section.a-padding-medium:eq(0) .a-span-last:eq(1) > p','text'],
            'buy_at' =>['div.a-section.a-padding-medium:eq(0) .a-span-last:eq(0) > p','text', '', function($date){
                return str_replace(['日', '年', '月'], ['','-', '-'], trim($date));
            }],
            'price' => ['#od-formatted-total', 'text', '', function($price){
                return str_replace('￥', '', $price);
            }],
            'receive_address_detail' =>['div.a-section.a-padding-medium:eq(2) .a-row:eq(0)','text'],
            'receive_address' =>['div.a-section.a-padding-medium:eq(2) .a-row:eq(1)','text'],
            'receive_mobile' =>['div.a-section.a-padding-medium:eq(2) .a-row:eq(3)','text'],
            'receive_user' =>['div.a-section.a-padding-medium:eq(2) > span:eq(0)','text'],
        ];
        if($data = QueryList::query($str, $rules, $items_container)->getData()){
            foreach ($data as &$datum) {
                $datum['receive_address'] .= $datum['receive_address_detail'];
                unset($datum['receive_address_detail']);
            }
        }
        return $data;
    }

    /**
     * parseExpressInfo
     *
     * @return array
     */
    public function parseExpressInfo($str)
    {
        $items_container = '';
        $rules = [
            'arrive_at' => ['.shipment-status-content > span:eq(0)', 'text', '', function($text){
                return str_replace(' ', '', $text);
            }],
            'logistics' => ['div.a-box-group.a-spacing-mini', 'text','', function($data){
                $str = str_replace(['  ',"\n\n","\r"], '',$data);
                return array_filter(explode("\n", $str));
            }],
            'express_number' => ['.event-list-tracking-id', 'text', '', function($str){
                return str_replace('跟踪包裹号 #: ', '', trim($str));
            }],
            'brand_name' => ['span.a-text-bold:eq(1)', 'text', '', function($str){
                return str_replace('配送员： ', '', trim($str));
            }],
        ];

        if($data = QueryList::query($str, $rules, $items_container)->getData()){
            $data = current($data);
            $data['express_flow'] = $this->formatLogisticsInfo($data['logistics']);
            $data['express_flow'] = trim(implode('|||', $data['express_flow']), '|');
        }
        return $data;
    }

    /**
     * formatLogisticsInfo
     * @param  array  $info
     * @return array
     */
    private function formatLogisticsInfo(array $info, $separator = ' ')
    {
        $datePattern = '#\d{1,2}月\d{1,2}日星期#';
        $timePattern = '#[上下]午\d{1,2}:\d{2}#';

        $lastTime = $lastDate = '';
        $logistics = [];

        foreach ($info as $val) {
            if(preg_match($timePattern, $val)){
                $lastTime = $this->timeConvert($val);
            }elseif(preg_match($datePattern, $val)){
                $lastDate = $this->dateConvert($val);
            }else{
                $logistics[$lastDate][$lastTime][] = $val;
            }
        }
        $result = [];
        foreach ($logistics as $date => $flow) {
            $result[] = $date.' '.key($flow).$separator.implode(' ', current($flow));
        }
        return array_reverse($result);
    }

    /**
     * timeConvert
     * @param  string $time
     * @return string
     */
    private function timeConvert($time)
    {
        $timeArr = explode(':', str_replace(['上午', '下午'], '', $time));
        if(strpos('上午', $time) !== false){
            if($timeArr[0] < 10){
                $timeArr[0] = '0'.$timeArr[0];
            }
        }else{
            $timeArr[0] = 12 + $timeArr[0];
        }
        $timeArr[] = '00';
        return implode(':', $timeArr);
    }

    /**
     * dateConvert
     * @param  string $date
     * @param  string $year
     * @return string
     */
    private function dateConvert($date, $year = null)
    {
        if(preg_match('#(\d{1,2})月(\d{1,2})日#i', $date, $match)){
            for ($i=1; $i < 3; $i++) {
                if($match[$i] < 10){
                    $match[$i] = '0'.$match[$i];
                }
            }
            $date = $match[1].'-'.$match[2];
        }
        return ($year ?: date('Y')).'-'.$date;
    }
}
