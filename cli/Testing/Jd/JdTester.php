<?php
namespace Jd;

use CookieAuth\BaseTester;
use HttpRequester\CurlFacade;

class JdTester extends BaseTester
{
    public static $host = 'http://home.m.jd.com';
    protected $request_url = 'http://home.m.jd.com/newAllOrders/newAllOrders.action?sid=8db9c69abb8bf1573bab7b083df108e9';

    /**
     * get http request header
     *
     * @param  array  $header
     * @return array
     */
    public function getHeaders(array $header = [])
    {
        $data = [
            // 'Host: home.m.jd.com',
            // 'Connection: keep-alive',
            // 'Pragma: no-cache',
            // 'Cache-Control: no-cache',
            // 'Upgrade-Insecure-Requests: 1',
            'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            // 'DNT: 1',
            // 'Referer: http://home.m.jd.com/myJd/newhome.action?sid=3b1421b8ccef1f05e15434fe1ddbce2d',
            // 'Accept-Encoding: gzip, deflate, sdch',
            // 'Accept-Language: zh-CN,zh;q=0.8,en;q=0.6',
            //'Cookie: abtest=20160927152056734_96; mobilev=html5; __jdv=122270672|direct|-|none|-|1474960858311; __jdu=102107582; rememberMe=""; logincheck=""; USER_FLAG_CHECK=73c35458609124a45cd0106a3a5cc8b7; TrackerID=c-xbgFYtSEipcFGm_BncOIv2sgQW7ZlERZa8eNrnHEWgC-sQwfL_oF39dMgBcU5CNQovWG87E5vNydvJJXrVD6-HdypFmt_xNmSkCWyM-KbSPZiTSArBapehQGJNwGa6cuioVzqc4lJCjMnoj_fSNA; pinId=QyuqcwnCzpk5nwcixH9uvg; pt_key=AAFX62-hADBfevuB5ckmkNPOZm8cWjO32BSPphDqidLYVSZd9CEQLzj8OVhkID5k0U6rmXC6EoI; pt_pin=15921512039_p; pt_token=nf7nybv7; pwdt_id=15921512039_p; whwswswws=HEQHkEnuK9wNGwQ5lAB7+irC3HvAD96u%2FG%2FLcTufE5OXa+B6P7qBog%3D%3D; returnurl="http://home.m.jd.com/myJd/home.action?sid=3b1421b8ccef1f05e15434fe1ddbce2d"; __jda=122270672.102107582.1474960858310.1475040135800.1475047279574.7; __jdb=122270672.3.102107582|7.1475047279574; __jdc=122270672; sid=3b1421b8ccef1f05e15434fe1ddbce2d; mba_muid=102107582; mba_sid=14750472795765170864339772518.3',
            'Cookie: JAMCookie=true; abtest=20160927152056734_96; mobilev=html5; __jdv=122270672|direct|-|none|-|1474960858311; __jdu=102107582; rememberMe=""; logincheck=""; TrackerID=c-xbgFYtSEipcFGm_BncOIv2sgQW7ZlERZa8eNrnHEWgC-sQwfL_oF39dMgBcU5CNQovWG87E5vNydvJJXrVD6-HdypFmt_xNmSkCWyM-KbSPZiTSArBapehQGJNwGa6cuioVzqc4lJCjMnoj_fSNA; pinId=QyuqcwnCzpk5nwcixH9uvg; pt_key=AAFX62-hADBfevuB5ckmkNPOZm8cWjO32BSPphDqidLYVSZd9CEQLzj8OVhkID5k0U6rmXC6EoI; pt_pin=15921512039_p; pt_token=nf7nybv7; pwdt_id=15921512039_p; whwswswws=HEQHkEnuK9wNGwQ5lAB7+irC3HvAD96u%2FG%2FLcTufE5OXa+B6P7qBog%3D%3D; __jda=122270672.102107582.1474960858310.1475040135800.1475047279574.7; __jdc=122270672; mba_muid=102107582; USER_FLAG_CHECK=25b92a114aef6463f769adcf1a470138; returnurl="http://home.m.jd.com/newAllOrders/newAllOrders.action?sid=8db9c69abb8bf1573bab7b083df108e9"; sid=8db9c69abb8bf1573bab7b083df108e9',
        ];
        return array_merge($data, $header);
    }
}
