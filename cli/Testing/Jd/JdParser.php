<?php
namespace Jd;

use QL\QueryList;

class JdParser
{

    public function parseOrderList($str)
    {
        $items_container = '#allOrders > .item';
        $rules = [
            // 'item' => ['.item > div.mc > a', 'html'],
            'detail_url' => ['div.mc > a', 'href2'],
            'title' => ['div.mc > a div.imco-r-content > div', 'text'],
            'img_url' => ['div.mc .imco-l-img > img','src'],
            'seller' => ['div.mt span.shop-name', 'text'],
            'first_img_url' => ['#carousel0 > ul > li:eq(0) img', 'src'],
            'status' => ['div.mt > span.i-info > div', 'class'],
        ];
        return QueryList::query($str, $rules, $items_container)->getData();
    }

    public function parseOrderDetail($str)
    {

        $items_container = 'div.pad55';
        $host = 'http://home.m.jd.com';
        $rules = [
            // 'express_detail_url' => ['#orderTrack', 'href'],
            // 'address' =>['div.step2.bdr-new > div > div.mc.step2-in-con','text'],
            // 'phone' =>['div.step2.bdr-new > div > div.mt > div.s2-phone','text'],
            // 'contact' =>['div.step2.bdr-new > div > div.mt > div.s2-name','text'],
            // 'express_company' =>['div.step4 div.send01 > div span','text'],
            // 'order_price' => ['div.step5.bdr-new > div.s5-item-w.bdb-1px > div.s-item.size15 > div.sitem-r', 'text'],
            'express_detail_url' => ['#orderTrack', 'href', '', function($url) use($host){
                return $host.$url;
            }],
            'receive_address' =>['div.step2 > div > div.step2-in-con','text'],
            'receive_mobile' =>['div.step2 > div > div.mt > div.s2-phone','text'],
            'receive_user' =>['div.step2 > div > div.mt > div.s2-name','text'],
            'brand_name' =>['div.step4 div.send01 > div span','text'],
            'price' => ['div.step5 > .s5-sum > div > span', 'text', '' , function($price){
                return trim($price, '¥');
            }],
            'buy_at' => ['div.step5 > .s5-sum > p', 'text', '', function($time){
                return str_replace('下单时间:', '', $time);
            }],
            'title' => ['div.step3 > div.mc > a:eq(0) p.sitem-m-txt', 'text'],
        ];
        return QueryList::query($str, $rules, $items_container)->getData();
    }

    /**
     * parseExpressInfos
     *
     * @return array
     */
    public function parseExpressInfos($str)
    {
        $items_container = '#changeContent1';
        $rules = [
            'express_number' => ['div.new-order-track > span:eq(0)', 'text', '', function($str){
                return preg_replace('/订单编号(?:：|:)\s*/i', '', $str);
            }],
            'order_status' =>['div.new-order-track > span:eq(1)','text', '', function($str){
                return preg_replace('/状态(?:：|:)\s*/i', '', $str) === '完成' ? 1 : 0;
            }],
            'logistics' => ['ul.new-of-storey > li > span', 'text','', function($data){
                return preg_replace('/(\d{4}-\d{2}-\d{2}\s(?:\d{2}:){2}\d{2})/','##$1###', $data);
            }],
        ];
        if($data = QueryList::query($str, $rules, $items_container)->getData()){
            $data = $data[0];
            $data['arrive_at'] = preg_match('/订单预计.*送达/', $data['logistics'], $match) ? $match[0] : '';
            $data['express_number'] = preg_match('/运单号为([\dA-Za-z]*)/', $data['logistics'], $match) ? $match[1] : $data['express_number'];
            $data['express_infos'] = array_map(function($str){
                return implode('  ', array_reverse(explode('##', $str)));
            }, explode('###', $data['logistics']));
            // preg_split('/(?:[\s\t]|&nbsp;)+/', htmlentities($data['express_infos'][0]));
            if($last_info = preg_split('/(?:\s|\t|\p{Z})+/u', $data['express_infos'][0])){
                $data['brand_name'] = end($last_info) ?: '';
            }
            $data['express_infos'] = trim(implode('|||', array_reverse($data['express_infos'])), '|');

        }
        return $data;
    }
}
