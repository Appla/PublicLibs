<?php
namespace Taobao;

use CookieAuth\BaseTester;
use HttpRequester\CurlFacade;

class TbTester extends BaseTester
{
    public static $host = 'https://unit.api.m.taobao.com';
    // protected $request_url = 'https://unsz.api.m.taobao.com//h5/mtop.taobao.mclaren.getmytaobaopage/1.0/?v=1.0&api=mtop.taobao.mclaren.getMyTaobaoPage&appKey=12574478&t=1475052841606&sign=2138876af6fe1105b0d1ec3ac1024f91&type=jsonp&callback=mtopjsonp2&data=%7B%22pageName%22%3A%22index%22%7D';
    protected $request_url = 'https://unit.api.m.taobao.com/h5/mtop.order.queryboughtlist/3.0/?appKey=12574478&t=1475052847795&sign=a8ee637cdd0831120ae875c91c5a0cf3&api=mtop.order.queryBoughtList&v=3.0&ttid=%23%23h5&ecode=1&AntiFlood=true&AntiCreep=true&type=jsonp&dataType=jsonp&callback=mtopjsonp3&data=%7B%22spm%22%3A%22a2141.7756461.2.6%22%2C%22page%22%3A1%2C%22tabCode%22%3A%22waitConfirm%22%2C%22appVersion%22%3A%221.0%22%2C%22appName%22%3A%22tborder%22%7D';



    public function getHeaders(array $header = [])
    {
        $data = [
            'accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'accept-language:zh-CN,zh;q=0.8,en;q=0.6',
            'cache-control:no-cache',
            //'cookie:_tb_token_=ul1vjODWivjU; thw=cn; v=0; cookie2=76bdd90180631a68dafd7f8dbf70a2cb; _m_h5_tk=6365948893a5fbb1e2a60cf3633db0ce_1475051927553; _m_h5_tk_enc=ba548a12c2c083dc177599cd51ab8d83; wud=wud; ockeqeudmj=isBeEX4%3D; _w_tb_nick=lsnha; imewweoriw=2ZWNTnzE9WWrqnCWFKebBuQ%2Flox1xpdrJ1dFVe23GV0%3D; munb=252939338; WAPFDFDTGFG=%2B4cMKKP%2B8PI%2BMKxltr9tzg%3D%3D; _w_app_lg=22; _w_al_f=1; uc3=sg2=VqoC7cwJwgkWSHHvb6WPC5lRmCbuAoc46o1tlzdo54Y%3D&nk2=D9Cfpl0%3D&id2=UU2w551eYFN6&vt3=F8dAS1HuMuQpn1R4fZs%3D&lg2=UIHiLt3xD8xYTw%3D%3D; uc1=cookie14=UoWwIfTOO%2BDJ5A%3D%3D&cookie21=VFC%2FuZ9ainBZ&cookie15=UtASsssmOIJ0bQ%3D%3D; lgc=lsnha; uss=VTsRxL3wA1PhNe4lRXKsLpKWqDw5tiTiVmVZai456gg3Ay5sy97%2BcFsG35M%3D; tracknick=lsnha; sg=a84; cookie1=VqwQnUEKvm%2BPQkV24Q%2FRWUC0xIkoF88j4QedWhyBEtM%3D; ntm=0; unb=252939338; skt=6ceb950d8309f63d; t=694e6618f9d212a051f90f912b3ee118; _cc_=U%2BGCWk%2F7og%3D%3D; _nk_=lsnha; _l_g_=Ug%3D%3D; cookie17=UU2w551eYFN6; _m_user_unitinfo_=unit|unsz; _m_unitapi_v_=1474962388253; l=AsHBPXJA-AXrEskkk8b6k-YwURbb7jXg; isg=D8653B6A02CDA202B66A1725932C9883',
            'dnt:1',
            'pragma:no-cache',
            //'referer:https://passport.taobao.com/iv/h5/h_5_verify_modes.htm?htoken=c38df9e249a9faa4697b081c1ea711d0&firstIn=true&umidToken=&wua=',
            'upgrade-insecure-requests:1',
            'user-agent:Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
            //'Referer: http://h5.m.taobao.com/mlapp/mytaobao.html',
            //'Cookie: wud=wud; v=0; cookie2=1cec1dcc178dde89ffb56b759056bafa; _tb_token_=JsEehSyvp; cna=WRpxEOa7uAICAbStXkIteJks; ockeqeudmj=itPW8es%3D; _w_tb_nick=lsnha; imewweoriw=3%2FtyBYrqQ6rYEGztQ0iB07m4%2B153hbFnkMHDukQqv5Q%3D; munb=252939338; WAPFDFDTGFG=%2B4cMKKP%2B8PI%2BMKxltr9tzg%3D%3D; _w_app_lg=22; _w_al_f=1; uc3=sg2=VqoC7cwJwgkWSHHvb6WPC5lRmCbuAoc46o1tlzdo54Y%3D&nk2=D9Cfpl0%3D&id2=UU2w551eYFN6&vt3=F8dAS1HuNUjBOrWhVFo%3D&lg2=W5iHLLyFOGW7aA%3D%3D; uc1=cookie14=UoWwIfTPLJ0e7Q%3D%3D&cookie21=UIHiLt3xTIkz&cookie15=WqG3DMC9VAQiUQ%3D%3D; lgc=lsnha; uss=U%2BM%2BExRjcwxcUT%2B1JoUx252XEpaXwrqfzqSPMshsqFg0rYu1vGFjwGNNTvs%3D; tracknick=lsnha; sg=a84; cookie1=VqwQnUEKvm%2BPQkV24Q%2FRWUC0xIkoF88j4QedWhyBEtM%3D; ntm=0; unb=252939338; skt=a1636bc87cf2219c; t=e7cd172686c7658d4f5c647d021b8a73; _cc_=W5iHLLyFfA%3D%3D; _nk_=lsnha; _l_g_=Ug%3D%3D; cookie17=UU2w551eYFN6; _m_user_unitinfo_=unit|unsz; _m_unitapi_v_=1474962388253; _m_h5_tk=82fb0316c8b7924a4aeff95dfc899eeb_1475056983973; _m_h5_tk_enc=f3d3dd34ff22f515ae9d83d5904d5a28; isg=AsnJJvQs-pMSk4bm97Z7ICqn0_yqAtRn30BGmms-RbDvsunEs2bNGLfk9q77; l=AgAA/ALHttkT-JCNjcer4NvM0JHSieRT',
            'Referer: http://h5.m.taobao.com/mlapp/olist.html?spm=a2141.7756461.2.6',
            'Cookie: wud=wud; v=0; cookie2=1cec1dcc178dde89ffb56b759056bafa; _tb_token_=JsEehSyvp; cna=WRpxEOa7uAICAbStXkIteJks; _uab_collina=147496491109234171025455; new_wud=wud41138; _umdata=2FB0BDB3C12E491DBEC7C16C23F91EF07764B1049905658E74342812D175704E4B0A68273C661A4D9E0C8BC241C674AD16CD82AF751441CE286C30889687CE4BF79CE3EE8BC38C405B54539A4CF9CA747C22E63DF5671C54332CC6046ED2679F4B0EC70FBF1E5013; l=AhoasOHw7A9lmjrz80Uhdi0iat88S54l; isg=Ant7DmBAqGn8MZTccTCJ6vQ5AVbHk4_Sca70wG04V3qRzJuu9aAfIpkO5NR9; ockeqeudmj=itPW8es%3D; _w_tb_nick=lsnha; imewweoriw=3%2FtyBYrqQ6rYEGztQ0iB07m4%2B153hbFnkMHDukQqv5Q%3D; munb=252939338; WAPFDFDTGFG=%2B4cMKKP%2B8PI%2BMKxltr9tzg%3D%3D; _w_app_lg=22; yryetgfhth=%2B4cXpeDzLTAIW6kFUQRUe0eOakB8O%2FwBKD9vL9fyZ16nUwoZwPsAipfPqo6eUjU2kqL1OL74jCMMw9hKW2dnVwqHSmjl0n%2Fgwxsvapd8cmIDurhLZFaVPMBWcZrwFHxdRhttHpG9OqtpMkWMI3v61SCKmoVEFp4fUSzT2Jz%2BgbnlY6JheX5vZG%2BYr1Ke9nPkZptk3q%2FbyrBhC2PN5M5xvo8Du7grgL7%2F98DQCllJGgH3jX5%2F4hf8BGJ0yG3y; _w_al_f=1; uc3=sg2=VqoC7cwJwgkWSHHvb6WPC5lRmCbuAoc46o1tlzdo54Y%3D&nk2=D9Cfpl0%3D&id2=UU2w551eYFN6&vt3=F8dAS1HuNUjBOrWhVFo%3D&lg2=W5iHLLyFOGW7aA%3D%3D; uc1=cookie14=UoWwIfTPLJ0e7Q%3D%3D&cookie21=UIHiLt3xTIkz&cookie15=WqG3DMC9VAQiUQ%3D%3D; lgc=lsnha; uss=U%2BM%2BExRjcwxcUT%2B1JoUx252XEpaXwrqfzqSPMshsqFg0rYu1vGFjwGNNTvs%3D; tracknick=lsnha; sg=a84; cookie1=VqwQnUEKvm%2BPQkV24Q%2FRWUC0xIkoF88j4QedWhyBEtM%3D; ntm=0; unb=252939338; skt=a1636bc87cf2219c; t=e7cd172686c7658d4f5c647d021b8a73; _cc_=W5iHLLyFfA%3D%3D; _nk_=lsnha; _l_g_=Ug%3D%3D; cookie17=UU2w551eYFN6; _m_user_unitinfo_=unit|unsz; _m_unitapi_v_=1474962388253; _m_h5_tk=ffe2a5d522b7ddab117345e23019ab29_1475057709977; _m_h5_tk_enc=a38aadcde684fb1268a626af4fb90252',
        ];
        return array_merge($data, $header);
    }
}
