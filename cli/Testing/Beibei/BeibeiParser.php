<?php
namespace Beibei;

use QL\QueryList;

class BeibeiParser
{

    public function parseOrderList($str)
    {
        $host = 'https://i.jumei.com';
        $items_container = 'div.main-wrap > div';
        $rules = [
            'detail_url' => ['div.order-info > div > div > a', 'href', '',function($url) use($host){
                return $host.$url;
            }],
            'order_id' =>['div.order-info a > span.order-num','text'],
            'title' =>['div.product-info > p.info-text','text'],
            'price' =>['div.product-price > div.price','text'],
            'img_url' => ['div.product-pic > img', 'src'],
            'order_status' => ['div.order-info span.order-status', 'text'],
        ];
        $data = QueryList::query($str, $rules, $items_container)->getData();
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $data[$key]['is_finished'] = in_array($val['order_status'], ['已退款', '已完成', '已过期', '全部退货']);
                if($data[$key]['is_finished'] && (stripos($val['detail_url'], 'shipping_no=') === false)){
                    unset($data[$key]);
                }
            }
        }
        return $data;
    }

    public function parseOrderDetail($str)
    {

        $host = 'https://i.jumei.com';

        $items_container = 'div.main-wrap';

        $rules = [
            'express_detail_url' => ['div.item-right > a', 'href', '',function($url) use($host){
                return $host.$url.'&_ajax_=1';
            }],
            'order_id' =>['div.order-content > span.order-no','text'],
            'buy_at' =>['li:eq(2) div.order-content > span','text'],
            'receive_address' =>['div.order-content > p','text'],
            'receive_mobile' =>['div.order-content > div > div.info-tel','text'],
            'receive_user' =>['div.order-content > div > div.info-name','text'],
        ];
        return QueryList::query($str, $rules, $items_container)->getData();
    }

    /**
     * parseExpressInfo
     *
     * @return array
     */
    public function parseExpressInfo($str)
    {
        $items_container = '#wrapper';
        $rules = [
            'express_info' => ['.express-info', 'text', '', function($str){
                return str_replace('快递','##快递', $str);
            }],
            'order_id' => ['div.row', 'text', '', function($str){
                return str_replace('订单号：','', $str);
            }],
            'logistics' => ['#traceHis > div.trace-item', 'text','', function($data){
                return preg_replace('/(\d{4}-\d{2}-\d{2}\s(?:\d{2}:){1,2}\d{2})/','##$1###', str_replace(['  ', "\n", "\r"], '',$data));
            }],
        ];

        if($data = QueryList::query($str, $rules, $items_container)->getData()){
            $data = $data[0];
            $data['arrive_at'] = preg_match('/订单预计*送达/', $data['logistics'], $match) ? trim($match[0]) : '';
            $data['express_number'] = preg_match('/快递单号：([^#]*)/', $data['express_info'], $match) ? trim($match[1]) : '';
            $data['brand_name'] = preg_match('/快递公司：([^#]+)/', $data['express_info'], $match) ? trim($match[1]) : '';
            $data['express_flow'] = array_map(function($str){
                return implode('  ', array_reverse(explode('##', $str)));
            }, explode('###', $data['logistics']));

            $data['express_flow'] = trim(implode('|||', array_reverse($data['express_flow'])), '|');
        }
        return $data;
    }
}
