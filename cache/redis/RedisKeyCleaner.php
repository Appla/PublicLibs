<?php
/**
 * Created by PhpStorm.
 * User: Appla
 * Date: 2017/5/5
 * Time: 13:58
 */

namespace test;

use Redis;
use BadMethodCallException;

/**
 * Class RedisCleaner
 * @package test
 *
 * @method void typeFilter()
 * @method void ttlFilter()
 * @method void valueFilter()
 */
class RedisKeyCleaner
{


    private $currentKeyName;
    private $currentKeyType;
    private $currentValue;
    private $currentKeyTtl;

    private $scanCursor;
    private $targetKeys = [];
    private $lastQueryKeys = [];

    const BATCH_SIZE = 200;
    const KEY_PATTERN = '*';
    const SHOULD_DELETE = true;
    const MATCH_ALL_KEY_PATTERN = '*';

    /**
     * filter
     * @var callable
     */
    private $typeFilter;
    private $ttlFilter;
    private $valueFilter;
    private $noFilterSet = true;

    private $limit = self::BATCH_SIZE;
    private $keyPattern = self::KEY_PATTERN;
    private $redis;


    const FILTER_TYPE_TTL = 1;
    const FILTER_TYPE_VALUE = 2;
    const FILTER_TYPE_TYPE = 4;

    private $matchedType;

    private $enableReverseFilter = false;
    private $enableReverseKey = false;

    /**
     * filter types
     * @var array
     */
    private static $filterTypes = [
        self::FILTER_TYPE_TTL => 'filterByTTL',
        self::FILTER_TYPE_TYPE => 'filterByType',
        self::FILTER_TYPE_VALUE => 'filterByValue',
    ];

    /**
     * @var array
     */
    private static $redisTypeMap = [
        Redis::REDIS_STRING => 'string',
        Redis::REDIS_SET => 'set',
        Redis::REDIS_LIST => 'list',
        Redis::REDIS_ZSET => 'zset',
        Redis::REDIS_HASH => 'hash',
        Redis::REDIS_NOT_FOUND => 'not found',
    ];

    /**
     * RedisMigration constructor.
     * @param Redis $redis
     */
    public function __construct(Redis $redis)
    {
        $this->redis = $redis;
    }

    /**
     *
     */
    public function run()
    {
        while($this->getKeys()){
            foreach ($this->lastQueryKeys as $key) {
                $this->currentKeyName = $key;
                if($this->shouldDelete($key)){
                    printf('key:%s ,is matched with filter you set[%s] and will delete soon' . PHP_EOL, $key, $this->matchedType ? self::$filterTypes[$this->matchedType] : '');
                    $this->targetKeys[] = $key;
                } else{
                    printf('key:%s, ttl:%s' . PHP_EOL, $key, $this->currentKeyTtl);
                }
            }
            $this->cleanTargetKeys();
        }
    }

    /**
     * shouldDelete
     *
     * @param  string $key
     * @return bool
     */
    private function shouldDelete(string $key = null)
    {
        $key || $key = $this->currentKeyName;
        $result = $this->ttlFilter && ($this->ttlFilter)($this->getTtl($key)) && $this->matchedType = self::FILTER_TYPE_TTL || $this->typeFilter && ($this->typeFilter)($this->getType($key)) && $this->matchedType = self::FILTER_TYPE_TYPE || $this->valueFilter && ($this->valueFilter)($this->getValue($key)) && $this->matchedType = self::FILTER_TYPE_VALUE || $this->noFilterSet && self::SHOULD_DELETE;
        return $this->enableReverseFilter ? !$result : $result;
    }

    /**
     * @return array|bool
     */
    private function getKeys()
    {
        return $this->lastQueryKeys = $this->enableReverseKey ? $this->getKeysReverse() : $this->scanKeys();
    }

    /**
     * @return array|bool
     */
    private function scanKeys()
    {
        $keys = [];
        while(!$keys && $this->scanCursor !== 0){
            $keys = $this->redis->scan($this->scanCursor, $this->keyPattern, $this->limit);
        }
        return $keys;
    }

    /**
     * get keys by reverse pattern
     *
     * @return array
     */
    private function getKeysReverse()
    {
        if($this->keyPattern === self::MATCH_ALL_KEY_PATTERN){
            return [];
        }
        $matchedKeys = [];
        $pattern = strtr($this->keyPattern, [
                '?' => '.',
                '*' => '.+',
            ]);
        while(!$matchedKeys && $this->scanCursor !== 0) {
            if($keys = $this->redis->scan($this->scanCursor, self::MATCH_ALL_KEY_PATTERN, $this->limit)){
                $matchedKeys = preg_grep(sprintf('~%s~u', $pattern), $keys, PREG_GREP_INVERT);
//                $pattern = rtrim($this->keyPattern, '*');
//                $matchedKeys = array_filter($keys, function($key) use($pattern) {
//                    return strpos($key, $pattern) === 0;
//                });
            }
        }
        return $matchedKeys;
    }

    /**
     * @param string $key
     * @return int
     */
    private function getType(string $key = null)
    {
        return $this->currentKeyType = $this->redis->type($key ?: $this->currentKeyName);
    }

    /**
     * @param string|null $key
     * @return int
     */
    private function getTtl(string $key = null)
    {
        return $this->currentKeyTtl = $this->redis->ttl($key ?: $this->currentKeyName);
    }

    /**
     * @TODO 优化数据量大的情况
     * @param string $key
     * @return int
     */
    private function getValue(?string $key = null)
    {
        $key || $key = $this->currentKeyName;
        switch ($this->getType($key)){
            case Redis::REDIS_STRING:
                $value = $this->redis->get($key);
                break;
            case Redis::REDIS_SET:
                $value = $this->redis->sGetMembers($key);
                break;
            case Redis::REDIS_LIST:
                $value = $this->redis->lRange($key, 0, -1);
                break;
            case Redis::REDIS_ZSET:
                $value = $this->redis->zRange($key, 0, -1);
                break;
            case Redis::REDIS_HASH:
                $value = $this->redis->hGetAll($key);
                break;
            case Redis::REDIS_NOT_FOUND:
            default:
                $value = null;
        }
        return isset($value) ? $this->currentValue = $value : null;
    }

    /**
     * cleanTargetKeys
     *
     * @param bool $force
     * @return int
     */
    private function cleanTargetKeys($force = false)
    {
        $ret = 0;
        if($force && $this->targetKeys || count($this->targetKeys) > $this->limit){
            $ret = $this->redis->del(...$this->targetKeys) and printf('%d keys are deleted.[%s...]'. PHP_EOL, count($this->targetKeys), implode(', ', array_slice($this->targetKeys, 0, 5)));
            $this->targetKeys = [];
        }
        return $ret;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function limit(int $limit)
    {
        if(is_int($limit) && $limit > 0){
            $this->limit = $limit;
        }
        return $this;
    }

    /**
     * @param string $pattern
     * @return $this
     */
    public function pattern($pattern)
    {
        if($pattern && is_string($pattern)){
            $this->keyPattern = $pattern;
        }
        return $this;
    }

    /**
     * setTtlFilter
     * @return $this
     */
    public function setTtlFilter(callable $func)
    {
        $this->ttlFilter = $func;
        $this->noFilterSet = false;
        return $this;
    }

    /**
     * setTypeFilter
     * @return $this
     */
    public function setTypeFilter(callable $func)
    {
        $this->typeFilter = $func;
        $this->noFilterSet = false;
        return $this;
    }

    /**
     * setValueFilter
     * @return $this
     */
    public function setValueFilter(callable $func)
    {
        $this->valueFilter = $func;
        $this->noFilterSet = false;
        return $this;
    }

    /**
     * enable reverse filter
     * @return $this
     */
    public function reverseFilter()
    {
        $this->enableReverseFilter = true;
        return $this;
    }

    /**
     * enable reverse key
     * @return $this
     */
    public function reverseKey()
    {
        $this->enableReverseKey = true;
        return $this;
    }

    /**
     * [__destruct description]
     */
    public function __destruct()
    {
        $this->targetKeys && $this->cleanTargetKeys(true);
    }

}
$redis = new Redis();

$redisHost = '127.0.0.1';
$redisPort = '6379';

$redis->connect($redisHost, $redisPort);


error_reporting(E_ALL ^ E_NOTICE);
ini_set('memory_limit', -1);

$keys = range(11, 30);
array_walk($keys, function($key) use($redis){
    echo $redis->set($key, random_int(1, 100), random_int(100, 1000)), PHP_EOL;
});

array_walk($keys, function($key) use($redis){
    echo $redis->get($key), PHP_EOL;
});

(new RedisKeyCleaner($redis))->limit(500)->setTtlFilter(function($ttl){
    return $ttl > 0;
})->run();

array_walk($keys, function($key) use($redis){
    echo $redis->get($key) ?: 'NONE', PHP_EOL;
});
