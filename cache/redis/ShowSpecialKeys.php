<?php
namespace cache\redis;

use Redis;

/**
 * Class ShowSpecialKeys
 */
class ShowSpecialKeys
{
    /**
     * Redis instance
     * @var Redis
     */
    private $redis;

    /**
     * filter
     * @var callable
     */
    private $filter;

    /**
     * key pattern
     * @var string
     */
    private $keyPattern;

    /**
     * key type
     *
     * @var string
     */
    private $keyType;

    /**
     * @var array
     */
    private $matchedKeys = [];

    /**
     * @var array
     */
    private $validKeys = [];

    const DEFAULT_SHOW_LENGTH = 1000;

    const MAX_SHOW_LEN = 3000;

    /**
     * @var array
     */
    private $methodMap = [
        'string' => 'get',
        'list' => 'lLen',
        'set' => 'sCard',
        'zset' => 'zCard',
        'hash' => 'hLen',
    ];

    /**
     * KeyCleaner constructor.
     * @param Redis $instance
     */
    public function __construct(Redis $instance)
    {
        $this->redis = $instance;
    }

    /**
     * @param string $keyPattern
     * @param callable $filter
     * @return array
     */
    private function getData(string $keyPattern, callable $filter)
    {
        $this->keyPattern = $keyPattern;
        $this->filter = $filter;
        $this->getKeys();
        $this->filterKey();
        return $this->validKeys;
    }

    /**
     * @param string $keyPattern
     * @param callable $filter
     * @return array
     */
    public function show(string $keyPattern, callable $filter, ?string $keyType) : void
    {
        $this->getData($keyPattern, $filter);
        $this->keyType = $keyType;
        foreach ($this->validKeys as $key) {
            printf('key type is:%s'.PHP_EOL, $this->keyType);
            print_r($this->fetchData($key, $this->keyType ?: $this->getType($key)));
        }
    }

    /**
     * @param string $key
     * @param null|string $type
     * @return array
     */
    private function fetchData(string $key, ?string $type) : array
    {
        $defaultReturn = [
            'count' => 1,
            'ttl' => 0,
            'data' => null,
        ];
        if(isset($this->methodMap[$type])){
            $ret = [];
            switch(strtolower($type)){
                case 'string':
                    $ret = [
                        'count' => 1,
                        'ttl' => $this->redis->ttl($key),
                        'data' => $this->redis->get($key),
                    ];
                    break;
                case 'list':
                    $ret = [
                        'count' => $this->redis->lLen($key),
                        'ttl' => $this->redis->ttl($key),
                        'data' => $this->redis->lRange($key, 1, self::DEFAULT_SHOW_LENGTH),
                    ];
                    break;
                case 'set':
                    $ret = [
                        'count' => $len = $this->redis->sCard($key),
                        'ttl' => $this->redis->ttl($key),
                        'data' => $len > self::MAX_SHOW_LEN ? $this->redis->sMembers($key) : $this->redis->sRandMember($key, self::DEFAULT_SHOW_LENGTH),
                    ];
                    break;
                case 'zset':
                    $ret = [
                        'count' => $this->redis->zCard($key),
                        'ttl' => $this->redis->ttl($key),
                        'data' => $this->redis->zRange($key, 1, self::DEFAULT_SHOW_LENGTH),
                    ];
                    break;
                case 'hash':
                    $ret = [
                        'count' => $this->redis->hLen($key),
                        'ttl' => $this->redis->ttl($key),
                        'data' => $this->redis->hGetAll($key),
                    ];
                    break;
                default:

            }
        }
        return $ret ?? $defaultReturn;
    }

    /**
     * @param string $key
     * @return string
     */
    private function getType(string $key) : string
    {
        return $key ? $this->redis->type($key) : '';
    }

    /**
     * @return array
     */
    private function getKeys() : array
    {
        return $this->matchedKeys = $this->redis->keys($this->keyPattern);
    }

    /**
     * @return array
     */
    private function filterKey() : array
    {
        return $this->validKeys = is_array($this->matchedKeys) ? array_filter($this->matchedKeys, $this->filter) : [];
    }
}

