<?php
/**
 * Created by PhpStorm.
 * User: Appla
 * Date: 2017/4/24
 * Time: 15:50
 */

namespace test;

use Redis;

class RedisMigration
{
    const MIGRATION_TYPE_COPY = 'copy';
    const MIGRATION_TYPE_MOVE = 'move';

    const MIGRATED_SET_KEY = 'redisMigratedKeys';

    private $migrationType = self::MIGRATION_TYPE_COPY;

    private $currentKeyName;
    private $currentKeyType;
    private $currentValue;
    private $currentKeyTtl;

    private $scanCursor;
    private $lastQueryKeys = [];

    const BATCH_SIZE = 200;
    const KEY_PATTERN = '*';

    /**
     * mode
     */
    const MODE_PIPELINE = Redis::PIPELINE;
    const MODE_MULTI = Redis::MULTI;

    private $limit = self::BATCH_SIZE;
    private $keyPattern = self::KEY_PATTERN;
    private $migrationMode = self::MODE_MULTI;

    /**
     * using keys to match pattern but scan
     * @var boolean
     */
    private $useKeys = false;

    private $srcRedis;
    private $dstRedis;
    private $migratedKeysSet;

    private $srcPipeline;
    private $dstPipeline;

    /**
     * @var array
     */
    const REDIS_TYPE_MAP = [
        Redis::REDIS_STRING => 'string',
        Redis::REDIS_SET => 'set',
        Redis::REDIS_LIST => 'list',
        Redis::REDIS_ZSET => 'zset',
        Redis::REDIS_HASH => 'hash',
        Redis::REDIS_NOT_FOUND => 'not found',
    ];

    /**
     * RedisMigration constructor.
     * @param Redis $srcRedis
     * @param Redis $dstRedis
     */
    public function __construct(Redis $srcRedis, Redis $dstRedis)
    {
        $this->srcRedis = $srcRedis;
        $this->dstRedis = $dstRedis;
    }

    /**
     * @param bool $replace
     */
    public function run(bool $replace = false)
    {
        $pipelineMode = $this->isPipelineMode();
        while($this->getKeys()){
            $pipelineMode && $this->dstPipeline = $this->dstRedis->pipeline();
            foreach ($this->lastQueryKeys as $key) {
                if($pipelineMode){
                    $this->getDstRedis()->sAdd($this->getMigratedKeysCacheKey(), $key);
                    $replace && $this->getDstRedis()->del($key);
                    $this->migratePipeline($key);
                }else{
                    if($this->isMigrated($key)){
                        printf('key:%s, already migrated'.PHP_EOL, $key);
                        continue;
                    }
                    $replace && $this->dstRedis->del($key);
                    $keyMigrationStatus = $this->migrate($key);
                    printf('迁移key,%s[type:%s, ttl:%s] %s'.PHP_EOL, $key, self::REDIS_TYPE_MAP[$this->currentKeyType], $this->currentKeyTtl, $keyMigrationStatus ? '成功' : '失败');
                    $keyMigrationStatus && $this->addToMigratedSet($key);
                }
            }
            if($pipelineMode){
                $result = $this->dstPipeline->exec();
                printf('command execution success %d/%d via pipeline' . PHP_EOL, count(array_filter((array)$result)),count((array)$result));
            }
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    private function isMigrated(string $key)
    {
        return $this->dstRedis->sIsMember($this->getMigratedKeysCacheKey(), $key);
    }

    /**
     * @param string $key
     * @return int
     */
    private function addToMigratedSet(string $key)
    {
        return $this->dstRedis->sAdd($this->getMigratedKeysCacheKey(), $key);

    }

    /**
     * @return string
     */
    private function getMigratedKeysCacheKey() : string
    {
        return $this->migratedKeysSet ?: $this->migratedKeysSet = self::MIGRATED_SET_KEY.$this->migrationType;
    }

    /**
     * @return Redis
     */
    private function getDstRedis()
    {
        return $this->isPipelineMode() ? $this->dstPipeline: $this->dstRedis;
    }

    /**
     * @param $key
     * @return array|bool|int|null
     */
    private function migratePipeline($key)
    {
        $ret = false;
        switch ($this->getType($key)){
            case Redis::REDIS_STRING:
                $ret = $this->getDstRedis()->set($key, $this->srcRedis->get($key));
                break;
            case Redis::REDIS_SET:
                if($members = $this->srcRedis->sGetMembers($key)){
                    array_unshift($members, $key);
                    $ret = call_user_func_array([$this->getDstRedis(), 'sAdd'], $members);
                }
                break;
            case Redis::REDIS_LIST:
                if($list = $this->srcRedis->lRange($key, 0, -1)){
                    array_unshift($list, $key);
                    $ret = call_user_func_array([$this->getDstRedis(), 'lPush'], $list);
                }
                break;
            case Redis::REDIS_ZSET:
                if($data = $this->srcRedis->zRange($key, 0, -1, true)){
                    $arguments = [$key];
                    foreach ($data as $name => $score){
                        $arguments[] = $score;
                        $arguments[] = $name;
                    }
                    $ret = call_user_func_array([$this->getDstRedis(), 'zAdd'], $arguments);
                }
                break;
            case Redis::REDIS_HASH:
                if($data = $this->srcRedis->hGetAll($key)){
                    foreach ($data as $hKey => $datum) {
                        $this->getDstRedis()->hSet($key, $hKey, $datum);
                    }
                }
                break;
            case Redis::REDIS_NOT_FOUND:
            default:
                $ret = null;
        }
        $this->getTtl($key) > 0 && $this->getDstRedis()->expire($key, $this->currentKeyTtl);
        return $ret;
    }


    /**
     * @param $key
     * @return array|bool|int|null
     */
    private function migrate(string $key)
    {
        $ret = false;
        switch ($this->getType($key)){
            case Redis::REDIS_STRING:
                $ret = $this->dstRedis->set($key, $this->srcRedis->get($key));
                break;
            case Redis::REDIS_SET:
                $ret = $this->dstRedis->sAdd($key, ...$this->srcRedis->sGetMembers($key));
                break;
            case Redis::REDIS_LIST:
                if($data = $this->srcRedis->lRange($key, 0, -1)){
                    $ret = $this->dstRedis->lPush($key, ...$data);
                }
                break;
            case Redis::REDIS_ZSET:
                if($data = $this->srcRedis->zRange($key, 0, -1, true)){
                    $arguments = [$key];
                    foreach ($data as $name => $score){
                        $arguments[] = $score;
                        $arguments[] = $name;
                    }
                    $ret = $this->dstRedis->zAdd(...$arguments);
                }
                break;
            case Redis::REDIS_HASH:
                if($data = $this->srcRedis->hGetAll($key)){
                    $pipe = $this->dstRedis->pipeline();
                    foreach ($data as $hKey => $datum) {
                        $pipe->hSet($key, $hKey, $datum);
                    }
                    $ret = $pipe->exec();
                }
                break;
            case Redis::REDIS_NOT_FOUND:
            default:
                $ret = null;
        }
        $this->getTtl($key) > 0 && $this->dstRedis->expire($key, $this->currentKeyTtl);
        $ret && $this->migrationType === self::MIGRATION_TYPE_MOVE && $this->srcRedis->del($key);
        return $ret;
    }

    /**
     * @return array|bool
     */
    private function getKeys()
    {
        return $this->lastQueryKeys = $this->useKeys ?  $this->srcRedis->keys($this->keyPattern) : (function(){
            $result = [];
            do{
                $result = $this->srcRedis->scan($this->scanCursor, $this->keyPattern, $this->limit);
            } while(!$result && $this->scanCursor !== 0);
	    return $result;
        })();
    }

    /**
     * @param string $key
     * @return int
     */
    private function getType(string $key)
    {
        return $this->currentKeyType = $this->srcRedis->type($key ?: $this->currentKeyName);
    }

    /**
     * @param string $key
     * @return int
     */
    private function getTtl(string $key)
    {
        return $this->currentKeyTtl = $this->srcRedis->ttl($key ?: $this->currentKeyName);
    }

    /**
     * @return bool
     */
    private function isPipelineMode()
    {
        return $this->migrationMode === self::MODE_PIPELINE;
    }

    /**
     * @TODO 优化数据量大的情况
     * @param string $key
     * @return int
     */
    private function getValue(string $key)
    {
        switch ($this->getType($key)){
            case Redis::REDIS_STRING:
                $value = $this->srcRedis->get($key);
                break;
            case Redis::REDIS_SET:
                $value = $this->srcRedis->sGetMembers($key);
                break;
            case Redis::REDIS_LIST:
                $value = $this->srcRedis->lRange($key, 0, -1);
                break;
            case Redis::REDIS_ZSET:
                $value = $this->srcRedis->zRange($key, 0, -1);
                break;
            case Redis::REDIS_HASH:
                $value = $this->srcRedis->hGetAll($key);
                break;
            case Redis::REDIS_NOT_FOUND:
            default:
                $value = null;
        }
        return isset($value) ? $this->currentValue = $value : null;
    }

    /**
     * @return $this
     */
    public function copy()
    {
        $this->migrationType = self::MIGRATION_TYPE_COPY;
        return $this;
    }

    /**
     * @return $this
     */
    public function move()
    {
        $this->migrationType = self::MIGRATION_TYPE_COPY;
        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function limit(int $limit)
    {
        if($limit > 0){
            $this->limit = $limit;
        }
        return $this;
    }

    /**
     * @param string $pattern
     * @return $this
     */
    public function pattern(string $pattern)
    {
        if($pattern){
            $this->keyPattern = $pattern;
        }
        return $this;
    }

    /**
     * @param bool $enable
     * @return $this
     */
    public function pipeline($enable = true)
    {
        $this->migrationMode = $enable ? self::MODE_PIPELINE : self::MODE_MULTI;
        return $this;
    }

    /**
     * @return $this
     */
    public function disableScan()
    {
        $this->useKeys = true;
        return $this;
    }
}
$srcRedis = new Redis();
$dstRedis = new Redis();

$r1Config = [
    '127.0.0.1',
    '6379',
];
$r2Config = [
    '192.168.1.196',
    '6380',
];
$srcRedis->connect(...$r1Config);
$dstRedis->connect(...$r2Config);

$prefix = 'test:';

$srcRedis->set($prefix.'100112233', 123);
$srcRedis->set($prefix.'sataetaeaa', '1a1sfasf');

$srcRedis->hSet($prefix.'hkaa', 'asfasf', 112323);
$srcRedis->hSet($prefix.'hkaa', '212asf', 'asfasf');
$srcRedis->hSet($prefix.'hkaa', 'asfas5555', 'asf00..');

$srcRedis->sAdd($prefix.'set_test', ...range(1, 100));

$srcRedis->lPush($prefix.'list_test', ...range(1000, 2000));



$srcRedis->zAdd($prefix.'zset_test', 111 ,'ttt');
$srcRedis->zAdd($prefix.'zset_test', 1, 'afasf');
$srcRedis->zAdd($prefix.'zset_test', 3, 'asfasf');
$srcRedis->zAdd($prefix.'zset_test', 4, '12asf');
$srcRedis->zAdd($prefix.'zset_test', 5, 'ff');
$srcRedis->zAdd($prefix.'zset_test', 22, '12asfasf');
error_reporting(E_ALL ^ E_NOTICE);
$tmp = $srcRedis->zRange($prefix.'zset_test', 0, -1, true);
ini_set('memory_limit', -1);
(new RedisMigration($srcRedis, $dstRedis))->copy()->run();
