<?php
namespace cache\redis;

use Redis;
use SebastianBergmann\CodeCoverage\Report\PHP;

/**
 * Class KeyCleaner
 */
class KeyCleaner
{
    /**
     * Redis instance
     * @var Redis
     */
    private $redis;

    /**
     * filter
     * @var callable
     */
    private $filter;

    /**
     * key pattern
     * @var string
     */
    private $keyPattern;

    /**
     * @var array
     */
    private $matchedKeys = [];

    /**
     * @var array
     */
    private $validKeys = [];

    /**
     * KeyCleaner constructor.
     * @param Redis $instance
     */
    public function __construct(Redis $instance)
    {
        $this->redis = $instance;
    }

    /**
     * @param string $keyPattern
     * @param callable $filter
     * @return array
     */
    private function getData(string $keyPattern, callable $filter)
    {
        $this->keyPattern = $keyPattern;
        $this->filter = $filter;
        $this->getKeys();
        $this->filterKey();
        return $this->validKeys;
    }

    /**
     * @param string $keyPattern
     * @param callable $filter
     * @return array
     */
    public function clear(string $keyPattern, callable $filter, $batch = false)
    {
        $this->getData($keyPattern, $filter);
        if($batch){
            $pipeline = $this->redis->pipeline();
            foreach ($this->validKeys as $key) {
                $pipeline->del($key);
                printf('key:%s is deleted'.PHP_EOL, $key);
            }
            $ret = $pipeline->exec();
        }else{
            $ret = [];
            foreach ($this->validKeys as $key) {
                $ret[$key] = $this->redis->del($key);
                printf('key:%s is deleted'.PHP_EOL, $key);
            }
        }
        printf('total:%d, affected:%d'.PHP_EOL, count($ret), $affected = count(array_filter($ret)));
        return [
            'data' => $ret,
            'affected' => $affected,
        ];
    }

    /**
     * @return array
     */
    private function getKeys()
    {
        return $this->matchedKeys = $this->redis->keys($this->keyPattern);
    }

    /**
     * @return array
     */
    private function filterKey()
    {
        return $this->validKeys = is_array($this->matchedKeys) ? array_filter($this->matchedKeys, $this->filter) : [];
    }
}