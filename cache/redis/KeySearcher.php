<?php
namespace cache\redis;

use Redis;

/**
 * Class KeySearcher
 */
class KeySearcher
{
    /**
     * Redis instance
     * @var Redis
     */
    private $redis;

    /**
     * filter
     * @var callable
     */
    private $filter;

    /**
     * key pattern
     * @var string
     */
    private $keyPattern;

    /**
     * @var array
     */
    private $matchedKeys = [];

    /**
     * @var array
     */
    private $validKeys = [];

    /**
     * KeyCleaner constructor.
     * @param Redis $instance
     */
    public function __construct(Redis $instance)
    {
        $this->redis = $instance;
    }

    /**
     * @param string $keyPattern
     * @param callable $filter
     * @return array
     */
    private function getData(string $keyPattern, callable $filter) : array
    {
        $this->keyPattern = $keyPattern;
        $this->filter = $filter;
        $this->getKeys();
        $this->filterKey();
        return $this->validKeys;
    }

    /**
     * @return array
     */
    private function getKeys() : array
    {
        return $this->matchedKeys = $this->redis->keys($this->keyPattern);
    }

    /**
     * @return array
     */
    private function filterKey() : array
    {
        return $this->validKeys = is_array($this->matchedKeys) ? array_filter($this->matchedKeys, $this->filter) : [];
    }

    /**
     * get key's type
     *
     * @param  string $key
     * @return array
     */
    protected function type(string $key) : string
    {
        return $this->redis->type($key);
    }
}
