<?php
namespace cache\redis;

use Redis;

const ROOT = __DIR__ . DIRECTORY_SEPARATOR;
$fileList = [
    'KeyCleaner.php',
    'KeySearcher.php',
    'ShowSpecialKeys.php',
];

//foreach ($fileList as $file) {
//    require ROOT . $file;
//}
array_walk($fileList, function(string $file){
    require ROOT . $file;
});

/**
 * GET REDIS INSTANCE
 *
 * @param  array  $configs
 * @return redis
 */
function getRedis(array $configs = []) : redis
{
    static $instance = null;
    $defaultConfigs = [
        '10.27.61.48',
        '6380',
    ];
    if(!$instance){
        $instance = new Redis();
        $instance->connect(...($configs ?: $defaultConfigs));
        print_r($instance->info());
    }
    return $instance;
}

// show

$keyPattern = 'address:deliveryInfo:scriptRunLog*';
$keyType = 'hash';

$player = new ShowSpecialKeys(getRedis());
$player->show($keyPattern, function($val){
    return true;
}, $keyType);

// cleaner

// $keyPattern = 'address:sto:Branch*';
// $excludeStr = ':2017_04_05';
// $func = function($key) use($excludeStr) {
//     return stripos($key, $excludeStr) === false;
// };

// $cleaner = new KeyCleaner($redis);
// $cleaner->clear($keyPattern, $func);
