<?php
/**
 * Created by PhpStorm.
 * User: Appla
 * Date: 2017/3/31
 * Time: 10:48
 */

namespace model\Address;

use model\Address\AddressLib;

/**
 * Class DbCache
 * @package model\Address
 * @todo add content size limit
 */
class DbCache extends AddressLib
{
    const TABLE_NAME = 'cache_data';

//CREATE TABLE `cache_data` (
//`key` varchar(190) NOT NULL COMMENT '缓存名',
//`value` longtext NOT NULL COMMENT '缓存值',
//`expiration_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '过期时间,unix时间戳,0表示没有过期时间',
//`comment` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
//`create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
//`update_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
//PRIMARY KEY (`key`)
//) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='数据库缓存(地址库用途)';

    /**
     * supported format
     */
    const FORMAT_ARRAY = 'ARRAY';
    const FORMAT_JSON = 'JSON';
    const FORMAT_SERIALIZE = 'SERIALIZE';
    const FORMAT_PLAIN = 'PLAIN';

    /**
     * @var string
     */
    private $format;

    /**
     * @var mixed
     */
    private $formattedData;

    /**
     * @var string
     */
    private $name;

    /**
     * @var mixed
     */
    private $data;

    /**
     * @var number
     */
    private $expirationTime;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var array
     */
    private $error;

    /**
     * @param string|null $name
     * @param string|null $data
     * @return bool|mixed
     */
    public function set($name = null, $data = null, $seconds = null)
    {
        $ret = false;
        $this->key($name)->data($data)->expirationTime($seconds);
        if($this->name && $this->data !== null && $this->formatData()){
            if($this->exists()){
                $ret = $this->update($this->buildCondition(), $this->buildInsertData());
            }else{
                $ret = $this->insert($this->buildInsertData());
            }
        }
        $this->reset();
        return $ret;
    }

    /**
     * @param number $seconds
     * @return $this
     */
    public function expirationTime($seconds)
    {
        if(is_numeric($seconds) && $seconds > 1){
            $this->expirationTime = time() + (int)$seconds;
        }
        return $this;
    }

    /**
     * @param string $name
     * @param number $seconds
     * @return int
     */
    public function expire($name, $seconds = null)
    {
        $ret = 0;
        $this->key($name)->expirationTime($seconds);
        if($this->name && $this->expirationTime){
            $ret = $this->update($this->buildCondition(), [
                'expiration_at' => $this->expirationTime,
            ]);
        }
        return $ret;
    }

    /**
     * @param string $name
     * @return int
     */
    public function del($name)
    {
        $ret = 0;
        $this->key($name);
        if($this->name){
            $ret = (int)$this->delete($this->buildCondition());
        }
        return $ret;
    }

    /**
     * @param string $name
     * @return int [0 : 过期, >0 : ttl, -1 : 无过期时间, -2 : 键值不存在]
     */
    public function ttl($name)
    {
        $this->key($name);
        if($this->name && ($result = self::getDb()->table(self::TABLE_NAME)->select('expiration_at')->getOne($this->buildCondition()))){
            $ttl = (int)$result['expiration_at'];
        }
        return isset($ttl) ? ($ttl ? (($surplus = $ttl - time()) > 0 ? $surplus : 0) : -1) : -2;
    }
    
    /**
     * @param string $name length required < 191
     * @return $this
     */
    public function key($name)
    {
        if((is_string($name) || is_numeric($name)) && mb_strlen($name, 'utf-8') < 191){
            $this->name = $name;
        }
        return $this;
    }

    /**
     * @param mixed $data
     * @return $this
     */
    public function data($data)
    {
        if($data !== null){
            $this->data = $data;
        }
        return $this;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function comment($note)
    {
        if(is_string($note)){
            $this->comment = $note;
        }
        return $this;
    }

    /**
     * @param string|null $name
     * @return mixed|null
     */
    public function get($name = null)
    {
        $this->key($name);
        $cachedData = null;
        if($this->name && ($row = self::getOne($this->buildCondition()))){
            $expTime = (int)$row['expiration_at'];
            if($expTime === 0 || $expTime > time()){
                $tmp = $row['value'];
                switch ($this->format){
                    case self::FORMAT_SERIALIZE:
                        $cachedData = unserialize($tmp);
                        break;
                    case self::FORMAT_PLAIN:
                        $cachedData = $tmp;
                        break;
                    case self::FORMAT_JSON:
                    default:
                        $cachedData = json_decode($tmp, true);
                }
            }
        }
        $this->reset();
        return $cachedData;
    }

    /**
     * @param string|null $name
     * @return bool
     */
    public function exists($name = null)
    {
        $this->key($name);
        return $this->name && self::count($this->buildCondition());
    }

    /**
     * @return $this
     */
    public function serializeFormat()
    {
        $this->format = self::FORMAT_SERIALIZE;
        return $this;
    }

    /**
     * @return $this
     */
    public function jsonFormat()
    {
        $this->format = self::FORMAT_JSON;
        return $this;
    }

    /**
     * @return $this
     */
    public function plaintFormat()
    {
        $this->format = self::FORMAT_PLAIN;
        return $this;
    }

    /**
     * @return array
     */
    private function buildCondition()
    {
        return [
            'name' => $this->name,
        ];
    }

    /**
     * @return array
     */
    private function buildInsertData()
    {
         $insertData =[
            'name' => $this->name,
            'value' => $this->formattedData,
        ];
        $this->expirationTime && $insertData['expiration_at'] = $this->expirationTime;
        $this->comment && $insertData['comment'] = $this->comment;
         return $insertData;
    }

    /**
     * @return null|string
     */
    private function formatData()
    {
        $formattedLocalData = null;
        if($this->data !== null){
            switch ($this->format){
                case self::FORMAT_SERIALIZE:
                    $formattedLocalData = serialize($this->data);
                    break;
                case self::FORMAT_PLAIN:
                    $formattedLocalData = $this->data;
                    break;
                case self::FORMAT_JSON:
                default:
                    $formattedLocalData = json_encode($this->data, JSON_UNESCAPED_UNICODE);
            }
        }
        return $this->formattedData = $formattedLocalData;
    }

    /**
     * unset properties
     */
    private function reset()
    {
        $this->data = $this->name = $this->formattedData = $this->format = $this->expirationTime = $this->comment = null;
    }

    /**
     * @return mixed
     */
    public function autoClean()
    {
        return self::getDb()->query(sprintf('DELETE FROM %s WHERE expiration_at != 0 AND expiration_at < %d', self::TABLE_NAME, time()));
    }

}