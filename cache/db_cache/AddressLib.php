<?php
/**
 * Created by PhpStorm.
 * User: Appla
 * Date: 2017/3/27
 * Time: 16:19
 */

namespace model\Address;

use artisan\db;
use BadMethodCallException;

/**
 * Class AddressLib
 * @package model\Address
 * @method mixed getAll(array $conditions, $limit = '', $orderBy = '');
 * @method mixed getOne(array $conditions, $orderBy = '');
 * @method mixed insert($data);
 * @method mixed update(array $conditions, $data);
 * @method mixed delete(array $conditions);
 * @method mixed count(array $conditions);
 * @method mixed clearTable($tableName);
 * @method mixed incr(array $conditions, $field);
 * @method mixed decr(array $conditions, $field);
 */
abstract class AddressLib
{
    /**
     * @var string
     */
    const DB_NAME = 'kd_address';

    const TABLE_NAME = '';

    /**
     * @var db
     */
    private static $dbInstance;

    /**
     * @var static[]
     */
    private static $instances;

    /**
     * @return static
     */
    public static function getInstance()
    {
        if(empty(self::$instances[$key = static::class])){
            self::$instances[$key] = new static();
        }
        return self::$instances[$key];
    }

    /**
     * @return db
     */
    final protected static function getDb()
    {
        return self::$dbInstance ?: self::$dbInstance = db::connect(self::DB_NAME);
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        if (method_exists(self::getDb(), $name)){
            return call_user_func_array([self::getDb()->table(static::TABLE_NAME), $name], $arguments);
        }
        throw new BadMethodCallException('不存在的方法');
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if(method_exists(self::getDb(), $name)){
            return call_user_func_array([self::getDb()->table(static::TABLE_NAME), $name], $arguments);
        }
        throw new BadMethodCallException('不存在的方法');
    }
}