<?php

namespace service\proxy;

class redis
{
    /**
     * default configs
     *
     * @var array
     */
    private static $config = [
        'host' => '127.0.0.1',
        'port' => 6379,
        'timeout' => 5,
    ];

    /**
     * singleton instance
     *
     * @var array
     */
    private static $instance = null;

    /**
     * facade door
     *
     * @param string $method
     * @param mixed $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(self::getInstance(), $method), $args);
    }

    /**
     * connect a redis instance
     *
     * @param  array  $config
     * @return null|\Redis
     */
    public static function connect(array $config = [])
    {
        self::$instance = new \Redis();
        self::$config = array_merge(self::$config, $config);
        if (self::$instance->connect(self::$config['host'], self::$config['port'], self::$config['timeout'])) {
            return self::$instance;
        }
        die('error');
    }

    /**
     *
     * @return \Redis
     */
    public static function getInstance()
    {
        return self::$instance ?: self::connect();
    }
}

$a = [123,145,66,44.11,5,555];

// redis::sAdd('sky', ...$a);
// var_dump(redis::sMembers('sky'));

// call_user_func_array([redis::getInstance(),'sAdd'],array_merge(['stt'], $a));
// forward_static_call_array(['\service\proxy\redis','sAdd'], array_merge(['stt'], $a));
call_user_func_array(['\service\proxy\redis','sAdd'], array_merge(['stt'], $a));

var_dump(redis::sMembers('stt'));
