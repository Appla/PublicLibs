<?php

namespace service\proxy;

use service\proxy\curl as http;
use service\proxy\redis as Redis;

/**
 * 代理列表维护类,基于redis
 * 每2分钟清空一次表,重新拉取,数量低于特定值,更新.
 */
class proxylist
{
    /**
     * redis instance
     *
     * @var null
     */
    // private static $redis = null;

    /**
     * api_uri
     *
     * @var string
     */
    private $api_uri = 'http://ent.kuaidaili.com/api/getproxy';

    /**
     * api_common_params
     *
     * @var array
     */
    private $api_common_params = [
        'orderid' => '907159097084750',
        'num' => 200,
    ];

    /**
     * default api params
     *
     * @var array
     */
    private $api_params = [
        'quality' => 2, //代理ip的稳定性 0: 不筛选(默认) 1: VIP稳定  2: SVIP企业版非常稳定
        // 'area' => '', //ip所在地区，支持按 国家/省/市 筛选  多个地区用英文逗号分隔，如 北京,上海
        // 'protocol' => '2', // 按代理协议筛选 1: HTTP, 2: HTTPS(同时也支持HTTP)
        //'method' => '1',  //按支持 GET/POST 筛选 1: 支持HTTP GET, 2: 支持 HTTP POST(同时也支持GET)
        // 'dedup' => 1, //  过滤今天提取过的IP  取值固定为1
        'an_ha' => 1, //返回的代理中只包含高匿名代理  取值固定为1
        'an_an' => 1, //返回的代理中只包含匿名代理   取值固定为1
        //'an_tr' => 1, //返回的代理中只包含透明代理   取值固定为1
        'sp1' => 1, //返回的代理中只包含极速代理（响应速度<1秒）  取值固定为1
        'sp2' => 1, //返回的代理中只包含快速代理（响应速度1~3秒） 取值固定为1
        //'sp3' => 1, //返回的代理中只包含慢速代理（响应速度>3秒）  取值固定为1
        // 'sort' => 1, // 返回的代理列表的排序方式    0: 默认排序  1: VIPSVIP企业版按响应速度(从快到慢)
        'format' => 'json', // 接口返回内容的格式   text: 文本格式(默认), json: VIPSVIP企业版json格式 xml : VIPSVIP企业版xml格式
        // 'sep' => '', // 提取结果列表中每个结果的分隔符 1: \r\n分隔(默认) 2: \n分隔  3: 空格分隔  4: |分隔
    ];

    /**
     * proxy update interval
     *
     * @var integer
     */
    private $update_interval = 5;

    /**
     * last get ip
     *
     * @var string|null
     */
    private $last_ip;

    /**
     * last time api returns
     *
     * @var array
     */
    private $last_time_api_return;

    /**
     * good ip store key
     *
     * @var string
     */
    const GOOD_IP = 'good_ips';

    /**
     * bad ip store key
     *
     * @var string
     */
    const BAD_IP = 'bad_ips';

    /**
     * good ip update time
     *
     * @var string
     */
    const UPDATE_TIME = 'update_time';

    /**
     * minimal good ip number
     *
     * @var integer
     */
    const MIN_GOOD_IP_NUM = 50;

    /**
     * ip expires time
     *
     * @var integer
     */
    const EXPIRES_TIME = 120;

    /**
     * init api params
     *
     * @param array $_options api request uri params
     * @param integer $interval update interval
     * @return void
     */
    public function __construct(array $_options = [], $interval = null)
    {
        $this->setApiParams($_options);
        is_int($interval) AND $this->update_interval = $interval;
    }

    /**
     * set api request params
     *
     * @param string|array $key
     * @param string $val
     * @return mixed
     */
    public function setApiParams($key, $val = null)
    {
        if (is_array($key)) {
            return $this->api_params = array_merge($this->api_params, $key);
        } elseif (is_string($key) && is_scalar($val)) {
            return $this->api_params[$key] = $val;
        }
        return false;
    }

    /**
     * getIpPoolLength
     *
     * @param string $type
     * @return integer
     */
    public function getIpPoolLength($type = self::GOOD_IP)
    {
        return Redis::sCard(self::keyBuilder($type));
        // static $_len = null;
        // return $_len ?: $_len = Redis::sCard(self::keyBuilder($type));
    }

    /**
     * get a ip
     *
     * @return mixed
     */
    public function get()
    {
        if ($this->getIpPoolLength() <= self::MIN_GOOD_IP_NUM) {
            $this->update([], true);
        } elseif (time() - (int) $this->getUpdateTime() > self::EXPIRES_TIME) {
            $this->clear();
            $this->renew();
        }
        return $this->last_ip = Redis::sRandMember(self::keyBuilder(self::GOOD_IP)) ?: false;
    }

    /**
     * add an ip to an ip pool
     *
     * @param string $ip
     * @param string $type  pool type
     * @return integer
     */
    public function add($ip = null, $type = self::GOOD_IP)
    {
        if (is_string($ip) && (bool) trim($ip)) {
            return Redis::sAdd(self::keyBuilder($type), $ip);
        }
    }

    /**
     * getLastGetIp
     *
     * @todo store in redis
     * @return string|null
     */
    public function getLastGetIp()
    {
        return $this->last_ip;
    }

    /**
     * getLastTimeApiReturn
     *
     * @todo store in redis
     * @return string|null
     */
    public function getLastTimeApiReturn()
    {
        return $this->last_time_api_return;
    }

    /**
     * get all ips
     *
     * @param string $type ip pool type
     * @return array|null
     */
    public function getAll($type = self::GOOD_IP)
    {
        return Redis::sMembers(self::keyBuilder($type));
    }

    /**
     * mannul update ip list
     *
     * @param array $params
     * @param boolean $diff_badip
     * @return boolean
     */
    public function renew(array $params = [], $diff_badip = true)
    {
        return $this->update(array_merge($this->api_params, (array) $params), true, $diff_badip);
    }

    /**
     * drop some ips from good ip list
     *
     * @param  string|array $ips
     * @return test
     */
    public function drop($ips = null)
    {
        if (is_string($ips)) {
            $ips = [$ips];
        } elseif ($ips === null) {
            $ips = [$this->getLastGetIp()];
        }
        if (is_array($ips)) {
            $_bad_ip = self::keyBuilder(self::BAD_IP);
            $_good_ip = self::keyBuilder(self::GOOD_IP);
            // $_store_key = self::keyBuilder(self::GOOD_IP);
            foreach ($ips as $k => $val) {
                $ips[$k] = Redis::sMove($_good_ip, $_bad_ip, $val);
                // Redis::sRem($_store_key, $val);
            }
            return $ips;
        }
    }

    /**
     * clear a set
     *
     * @param string $key
     * @return boolean
     */
    public function clear($key = null)
    {
        $key === null and $key = self::GOOD_IP;
        return Redis::del(self::keyBuilder($key));
    }

    /**
     * get good and bad ip Diff
     *
     * @param  string $key_a
     * @param  string $key_b
     * @return array
     */
    public function getIpPoolDiff($key_a = self::GOOD_IP, $key_b = self::BAD_IP)
    {
        return Redis::sDiff(self::keyBuilder($key_a), self::keyBuilder($key_b));
    }

    /**
     * update ip
     *
     * @param array $params
     * @param  boolean $force
     * @param  boolean $diff_badip store unintersect with bad ip
     * @return boolean|int
     */
    private function update(array $params = [], $force = false, $diff_badip = true)
    {
        if ($force || time() - (int) $this->getUpdateTime() > $this->update_interval) {
            $url = $this->buildRequestUri(array_merge($this->api_params, (array) $params));
            if ($this->last_time_api_return = $data = http::get($url)) {
                if ($data = json_decode($data, true) and isset($data['data']['count']) && $data['data']['count'] > 0) {
                    $this->setUpdateTime();
                    //$added_count = call_user_func_array(['\artisan\cache','sAdd'], array_merge([self::keyBuilder(self::GOOD_IP)], $data['data']['proxy_list']));
                    $added_count = Redis::sAdd(self::keyBuilder(self::GOOD_IP), ...$data['data']['proxy_list']);
                    if ($diff_badip === true) {
                        return Redis::sDiffStore(self::keyBuilder(self::GOOD_IP), self::keyBuilder(self::GOOD_IP), self::keyBuilder(self::BAD_IP));
                    }
                    return $added_count;
                } else {
                    var_dump($url, $data);
                    //@todo write log
                    //alerm
                }
            } else {
                var_dump(http::get_error());
                //request failed
                //alerm
            }
        }
        return false;
    }

    /**
     * buildRequestUri
     *
     * @param array $params custom parameters for api call
     * @return string
     */
    private function buildRequestUri(array $params = [])
    {
        return $this->api_uri . '?' . http_build_query(array_merge($this->api_common_params, $params));
    }

    /**
     * getUpdateTime
     *
     * @return integer
     */
    private function getUpdateTime()
    {
        return Redis::get(self::keyBuilder(self::UPDATE_TIME));
    }

    /**
     * setUpdateTime
     *
     * @return void
     */
    private function setUpdateTime()
    {
        return Redis::set(self::keyBuilder(self::UPDATE_TIME), microtime(true), 86400);
    }

    /**
     * redis store keyBuilder
     *
     * @param string $key
     * @return string
     */
    private static function keyBuilder($key = null)
    {
        if (!is_string($key)) {
            die('参数配置错误');
        }
        return '__express_proxy_list__' . $key;
    }

    /**
     * update on destruct
     */
    public function __destruct()
    {
        $this->update();
    }
}
