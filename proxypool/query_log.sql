/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50714
Source Host           : 127.0.0.1:3306
Source Database       : log

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2016-08-19 11:11:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for query_log
-- ----------------------------
DROP TABLE IF EXISTS `query_log`;
CREATE TABLE `query_log` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `express_no` varchar(50) NOT NULL COMMENT '单号',
  `query_from` varchar(20) NOT NULL DEFAULT '' COMMENT '查询来源',
  `query_source` varchar(30) NOT NULL DEFAULT '' COMMENT '数据源',
  `query_type` varchar(20) NOT NULL DEFAULT '' COMMENT '查询类型,vpn,代理,原有形式',
  `proxy_ip` varchar(25) NOT NULL DEFAULT '' COMMENT '代理ip',
  `try_times` tinyint(2) NOT NULL DEFAULT '' COMMENT 'try connect times',
  `querier_ip` varchar(15) NOT NULL DEFAULT '0' COMMENT '查询者ip',
  `express_company` varchar(20) NOT NULL DEFAULT '' COMMENT '查询品牌',
  `query_status` varchar(10) NOT NULL COMMENT '查询结果是否成功',
  `error_info` varchar(255) NOT NULL DEFAULT '' COMMENT '错误信息',
  `query_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间',
  `query_cost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '查询耗时',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
SET FOREIGN_KEY_CHECKS=1;
