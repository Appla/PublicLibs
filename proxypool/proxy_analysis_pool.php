<?php

namespace service\proxy;

use service\proxy\proxylist;

/**
 * 公共用途ip池子
 */
class proxy_analysis_pool extends proxylist
{
    /**
     * good ip store key
     *
     * @var string
     */
    const GOOD_IP = 'analysis_good_ips';

    /**
     * bad ip store key
     *
     * @var string
     */
    const BAD_IP = 'analysis_bad_ips';

    /**
     * good ip update time
     *
     * @var string
     */
    const UPDATE_TIME = 'analysis_update_time';

    /**
     * minimal good ip number
     *
     * @var integer
     */
    const MIN_GOOD_IP_NUM = 50;

    /**
     * ip expires time
     *
     * @var integer
     */
    const EXPIRES_TIME = 120;

}
