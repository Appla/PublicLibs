<?php

namespace service\proxy;

use service\proxy\proxylist;

/**
 * 快递100专用ip池子
 */
class kd100_proxy_pool extends proxylist
{
    /**
     * good ip store key
     *
     * @var string
     */
    const GOOD_IP = 'kd100_good_ips';

    /**
     * bad ip store key
     *
     * @var string
     */
    const BAD_IP = 'kd100_bad_ips';

    /**
     * good ip update time
     *
     * @var string
     */
    const UPDATE_TIME = 'kd100_update_time';

    /**
     * minimal good ip number
     *
     * @var integer
     */
    const MIN_GOOD_IP_NUM = 50;

    /**
     * ip expires time
     *
     * @var integer
     */
    const EXPIRES_TIME = 120;

}
