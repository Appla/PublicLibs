import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;


public class key {
    // out put pub key from cer file
    public static void main (String[] args)
    {
        try {
            FileInputStream is = new FileInputStream("./fiddler.cer");
            CertificateFactory f = CertificateFactory.getInstance("X.509");
            X509Certificate certificate = (X509Certificate)f.generateCertificate(is);
            PublicKey publicKey = certificate.getPublicKey();
            Base64.Encoder encoder = Base64.getEncoder();
            String publicKeyString = encoder.encodeToString(publicKey
                    .getEncoded());
            System.out.println(publicKeyString);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //out put pubkey form jks file
    // public static void main(String[] args) {
    //     try {

    //         FileInputStream is = new FileInputStream("./ks.jks");
    //         KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
    //         String password = "111111";
    //         char[] passwd = password.toCharArray();
    //         keystore.load(is, passwd);
    //         String alias = "fiddler";
    //         Key key = keystore.getKey(alias, passwd);
    //         if (key instanceof PrivateKey) {
    //           // Get certificate of public key
    //           Certificate cert = keystore.getCertificate(alias);
    //           // Get public key
    //           PublicKey publicKey = cert.getPublicKey();

    //           Base64.Encoder encoder = Base64.getEncoder();

    //           String publicKeyString = encoder.encodeToString(publicKey
    //                     .getEncoded());
    //           System.out.println(publicKeyString);
    //           String str = "abc";
    //           System.out.println(str);

    //         } else {
    //             PublicKey publicKey = keystore.getCertificate(alias).getPublicKey();
    //             Base64.Encoder encoder = Base64.getEncoder();
    //             String publicKeyString = encoder.encodeToString(publicKey
    //                     .getEncoded());
    //             System.out.println(publicKeyString);
    //            String str = "efg";
    //             System.out.println(str);
    //         }

    //     } catch (Exception e) {
    //         e.printStackTrace();
    //     }
    // }
}
