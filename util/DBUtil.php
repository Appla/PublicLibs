<?php

class DBUtil
{
    /**
     * arrayToInsertSql
     *
     * @param  array  $data
     * @param  string $table
     * @param  boolean|string $operation
     * @return string
     */
    public static function arrayToInsertSql(array $data = [], $table = '#TABLE#', $operation = false)
    {
        if (is_array($data)) {
            //统一当作多数组处理
            if (!is_array(current($data))) {
                $data = [$data];
            }
            $fields = array_keys(current($data));
            $sql = (is_string($operation) ? strtoupper($operation) : ($operation ? 'REPLACE' : 'INSERT')) . ' INTO `' . $table . '` (`' . implode('`, `', $fields) . '` ) VALUES ';
            $values = '';
            foreach ($data as $value) {
                $values .= '("' . implode('", "', $value) . '"),';
            }
            return $sql . trim($values, ',');
        }
        return '';
    }
}
