<?php

class ExcelUtil
{
	/**
	 * getColumnRange
	 *
	 * @param  int $len range length
	 * @return array
	 */
    public static function getColumnRange(int $len) : array
    {
        return $len > 0 ? array_map(function($val){
            return static::getColumnStr($val);
        }, range(1, $len)) : [];
    }

    /**
     * getColumnStr
     *
     * @param  int    $code
     * @return string
     */
    public static function getColumnStr(int $code) : string
    {
        if(is_int($code) && $code > 0){
            if($code < 27){
                return chr(64+$code);
            }
            do{
                //没有0的概念,0 means the largest number
                $numArr[] = $remainder = $code % 26 ?: 26;
                $code = ($code - $remainder) / 26;
            }while($code > 26);

            $code > 0 and $numArr[] = $code;
            return implode('', array_map(function($val){
                    return chr(64+$val);
                }, array_reverse($numArr)));
        }
    }

    /**
     * columnStrToInt
     *
     * @param  string $str
     * @return int
     */
    public static function columnStrToInt($str)
    {
        if($str){
            $numArr = array_map(function($val){
                return ord($val) - 64 ;
            }, array_reverse(str_split($str)));
            foreach ($numArr as $exp => $val) {
                //pow(26, $exp);
                $numArr[$exp] = (26 ** $exp) * $val;
            }
            return array_sum($numArr);
        }
        return 0;
    }
}
