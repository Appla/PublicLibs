<?php
/**
 * http request tool
 *
 * @author Appla [bhg@live.it]
 * @version 0.1 [20160415]
 * @todo
 */

class Curl
{
    /**
     * errors
     *
     * @var string|array
     */
    protected static $errors;

    /**
     * origin return
     *
     * @var mixed
     */
    protected static $origin_return = null;

    /**
     * last requst infos
     *
     * @var array
     */
    protected static $last_curl_info;

    /**
     * request header
     *
     * @var array
     */
    protected static $headers = [
        "cache-control: no-cache",
        "Pragma: no-cache",
    ];

    /**
     * cURL Options
     *
     * @var array
     */
    protected static $curl_options = [];

    /**
     * time_out
     *
     * @var integer
     */
    protected static $time_out = 5;

    /**
     * set_option
     *
     * @param mixed $key
     * @param mixed $val
     * @return void
     */
    public static function setOption($key, $val = null)
    {
        if (is_array($key)) {
            static::$curl_options = array_merge(static::$curl_options, $key);
        } elseif (is_int($key) && $val !== null) {
            static::$curl_options[$key] = $val;
        }
    }

    /**
     * set http request header
     *
     * @param array $header
     * @return void
     */
    public static function setRequestHeader($header)
    {
        if (is_array($header)) {
            static::$headers = array_merge(static::$headers, $header);
        } elseif (is_string($header)) {
            static::$headers[] = $header;
        }
    }

    /**
     * curl_setopt_array
     *
     * @param  resource &$ch
     * @param  array $curl_options
     * @param  boolean $skip_error 设置错误时是否跳过
     * @return boolean
     */
    private static function curl_setopt_array(&$ch, array $curl_options, $skip_error = true)
    {
        foreach ($curl_options as $option => $value) {
            if (!curl_setopt($ch, $option, $value) && !$skip_error) {
                return false;
            }
        }
        return true;
    }

    /**
     * get
     *
     * @param  string $url
     * @param  array  $options
     * @return mixed
     */
    public static function get($url, array $options = [])
    {
        return static::request($url, null, 'get', $options);
    }

    /**
     * post
     *
     * @param  string $url
     * @param  string|null|array $data
     * @param  array  $options
     * @return mixed
     */
    public static function post($url, $data = null, array $options = [])
    {
        return static::request($url, $data, 'post', $options);
    }

    /**
     * request
     *
     * @param  string $url
     * @param  string $method
     * @param  string|null|array $data
     * @param  array  $options
     * @return mixed
     */
    public static function request($url, $data = null, $method = 'get', array $options = [])
    {
        if ($url === '') {
            return false;
        }
        $ch = curl_init();
        $curl_options = [
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => static::$headers,
            CURLOPT_CUSTOMREQUEST => strtoupper($method),
            CURLOPT_TIMEOUT => static::$time_out,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML =>  like Gecko) Chrome/54.0.2403.125 Safari/537.36',
            CURLOPT_AUTOREFERER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_AUTOREFERER => true,
            CURLOPT_FOLLOWLOCATION => true,
        ];
        if ($data) {
            if (strtolower($method) === 'post') {
                $curl_options[CURLOPT_POSTFIELDS] = $data;
            } elseif (is_array($data)) {
                $url .= (stripos($url, '?') === false ? '?' : '&') . http_build_query($data);
                $curl_options[CURLOPT_URL] = $url;
            }
        }
        if (is_array(static::$curl_options) && count(static::$curl_options)) {
            $curl_options = array_replace($curl_options, static::$curl_options);
        }
        is_array($options) && count($options) && $curl_options = array_replace($curl_options, $options);
        if (!curl_setopt_array($ch, $curl_options)) {
            if (function_exists('curl_reset')) {
                curl_reset($ch);
            } else {
                curl_close($ch);
                $ch = curl_init();
            }
            static::curl_setopt_array($ch, $curl_options);
        }
        static::$origin_return = curl_exec($ch);
        static::$last_curl_info = curl_getinfo($ch);
        if (static::$origin_return === false) {
            static::setError(curl_error($ch), 'error_msg');
            static::setError(curl_errno($ch), 'error_code');
            static::setError(static::$last_curl_info, 'curl_info');
            static::setError(static::$origin_return, 'origin_return');
            curl_close($ch);
            return false;
        }
        curl_close($ch);
        return static::$origin_return;
    }

    /**
     * getError
     *
     * @return mixed
     */
    public static function getError()
    {
        return static::$errors;
    }

    /**
     * get last request original return
     *
     * @return array
     */
    public static function getLastRequestInfo()
    {
        return [
            static::$origin_return,
            static::$last_curl_info,
        ];
    }

    /**
     * getUserAgent
     *
     * @param  string $type  mobile|pc|all
     * @return string
     */
    public static function getUserAgent($type = 'all')
    {
        switch ($type) {
            case 'mobile':
                $user_agents = [
                    'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
                    // iPad
                    'Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
                    // Galaxy S5
                    'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.23 Mobile Safari/537.36',
                    // Nexus 5X
                    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.23 Mobile Safari/537.36',
                    // iPod
                    'Mozilla/5.0 (iPod; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3A101a Safari/419.3',
                ];
                break;
            case 'all':
            case 'pc':
            default:
                $user_agents = [
                    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393',
                    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
                    'Opera/9.80 (Windows NT 6.1; WOW64; U; en) Presto/2.10.229 Version/11.62',
                    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2859.0 Safari/537.36',
                ];
                break;
        }
        return $user_agents[array_rand($user_agents)];
    }

    /**
     * setError
     *
     * @param mixed $error
     * @param string|null $key
     */
    protected static function setError($error, $key = null)
    {
        if (!is_scalar($key)) {
            $key = date('Y_m_d_H_i_s') . mt_rand(1000, 9999);
        }
        static::$errors[(string) $key] = $error;
    }


    /**
     * 没有针对CURLINFO_HEADER_OUT
     * @param string $optionKey
     * @return mixed|null
     */
    private static function getCurlOptionKey($optionKey)
    {
        if(self::$curlOptionString === null){
            self::$curlOptionString = implode(',', array_filter(array_keys(get_defined_constants(true)['curl']), function ($key) {
                return strpos($key, 'CURLOPT_') === 0;
            })).',';
        }
        return strpos(self::$curlOptionString, $optionKey.',') !== false ? constant($optionKey) : null;
    }
}
