<?php

/**
 * passwd generator
 * could using hash supported algorithm or php build in passwd generator
 *
 * @author  Appla<bhg@live.it>
 * @version  0.1
 * @date 2016-12-09
 */
class passwd
{
    /**
     * password_hash supported algorithm
     * @var array
     */
	private static $supportedPasswdAlgos = [PASSWORD_DEFAULT, PASSWORD_BCRYPT];

    /**
     *  password_hash supported option
     * @var array
     */
	private static $allowedPasswdOptions = [
		'salt' => '',
		'cost' => '',
	];

    /**
     * password_hash algorithm
     * @var int
     */
	private static $passwdAlgo = PASSWORD_DEFAULT;

    /**
     * last generated password
     * @var string|null
     */
	private static $lastPasswd;

    /**
     * last generated salt
     * @var string|null
     */
	private static $lastSalt;

    /**
     * uniqid prefix
     * @var string
     */
	private static $uniqPrefix = 'kbPw';

    /**
     * hash support algo check cache
     * @var array
     */
    private static $isHashSupportedThisAlgorithm = [];

    /**
     * defaultPasswdSaltFormat
     * @var string
     */
    private static $defaultPasswdSaltFormat = '%s%s';

    /**
     * @param string $passwdStr
     * @param array $options
     * @return bool|string
     */
	public static function generate($passwdStr, array $options = [])
	{
		return $passwdStr ? static::$lastPasswd = password_hash($passwdStr, static::$passwdAlgo, array_intersect_key($options, static::$allowedPasswdOptions)): '';
	}

    /**
     * @param string $passwdHash
     * @return bool
     */
	public static function verify($passwdHash)
	{
		return $passwdHash ? password_verify($passwdHash) : false;
	}

    /**
     * @param string $passwdHash
     * @return array
     */
	public static function hashInfo($passwdHash)
	{
		return $passwdHash ? password_get_info($passwdHash) : [];
	}

    /**
     * @param string $passwdHash
     * @param int $algo
     * @param array $options
     * @return bool
     */
	public static function isNeedRehashs($passwdHash, $algo, array $options)
	{
		return $passwdHash ? password_needs_rehash($passwdHash, $algo, $options) : false;
	}

    /**
     * @param int $algo
     */
	public static function setAlgo($algo)
	{
		if(in_array($algo, static::$supportedPasswdAlgos)){
			static::$passwdAlgo = $algo;
		}
	}

    /**
     * @return null|string
     */
	public static function getLastSalt()
	{
		return static::$lastSalt;
	}

    /**
     * @return null|string
     */
	public static function getLastPasswd()
	{
		return static::$lastPasswd;
	}

    /**
     * @return string
     */
	public static function generateSalt()
	{
		return static::$lastSalt = hash('', uniqid(static::$uniqPrefix, true));
	}

    /**
     * @param string $passwdStr
     * @param string $salt
     * @return string
     */
	public static function md5Hash($passwdStr, $salt = '')
    {
        return static::hash(static::combineSalt($str, $salt), 'md5', false);
    }

    /**
     * @param string $passwdStr
     * @param string $hash
     * @return bool
     */
	public static function md5Verify($passwdStr, $hash, $salt = '')
	{
		return static::md5Hash($passwdStr, $salt) === $hash;
	}

    /**
     * @param string $str
     * @param string $algo
     * @param bool $raw
     * @return string
     */
	private static function hash($str, $algo = 'md5', $raw = false)
	{
		if(static::isHashSupported($algo)){
			return hash($algo, $str, $raw);
		}
		return '';
	}

    /**
     * hashVerify
     *
     * @param  string $algo
     * @param  string $passwdStr
     * @param  string $hash
     * @param  string $salt
     * @return boolean
     */
    private static function hashVerify($algo, $passwdStr, $hash, $salt = '')
    {
        return static::hash(static::combineSalt($passwdStr, $salt), $algo, false) === $hash;
    }

    /**
     * check if hash support this algorithm
     * @param  string  $algo
     * @return boolean
     */
    private static function isHashSupported($algo)
    {
        if(($algo = strtolower((string)$algo)) && !isset(static::$isHashSupportedThisAlgorithm[$algo])){
            static::$isHashSupportedThisAlgorithm[$algo] = in_array($algo, hash_algos());
        }
        return !empty(static::$isHashSupportedThisAlgorithm[$algo]);
    }

    /**
     * @param string $passwdStr
     * @param string $salt
     * @return string
     */
	public static function combineSalt($passwdStr, $salt = '', $tpl = '')
	{
        empty($tpl) && $tpl = static::$defaultPasswdSaltFormat;
		return sprintf($tpl, (string) $passwdStr, (string)$salt);
	}

    /**
     * __callStatic
     *
     * @param  string $method
     * @param  array $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        $method = strtolower($method);
        if(substr($method, -4) === 'hash'){
            $algo = substr($method, 0, -4);
            if(static::isHashSupported($algo)){
                return call_user_func_array([__CLASS__, 'hash'], [call_user_func_array([__CLASS__, 'combineSalt'], array_slice($args, 0, 2)), $algo]);
            }
        }elseif(substr($method, -6) === 'verify' && count($args) > 1){
            $algo = substr($method, 0, -6);
            if(static::isHashSupported($algo)){
                array_unshift($args, $algo);
                return call_user_func_array([__CLASS__, 'hashVerify'], $args);
            }
        }
        throw new BadMethodCallException('method:'.$method.' NOT found in class');
    }
}
