<?php

function getIp()
{
    $ip = empty($_SERVER['REMOTE_ADDR']) ? getenv('REMOTE_ADDR') ?: 'unknown' : $_SERVER['REMOTE_ADDR'];
    //for performace
    if(strpos($ip, '10.') === 0 ||preg_match('/^(?:10|192\.168|172\.(?:1[6-9]|2\d|3[01])\./', $ip) || $ip === 'unknown') {
        if($xForwardedForIps = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? getenv('HTTP_X_FORWARDED_FOR') ?: '' : $_SERVER['HTTP_X_FORWARDED_FOR']){
            //unreliable
            $xForwardedForIps = explode(',', $xForwardedForIps);
            //using last ip
            $ip = array_pop($xForwardedForIps);
        }
    }
    return $ip;
}

function get_ip($realIpOnly = false)
{
    $realIpOnly || $forwardIp = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? getenv('HTTP_X_FORWARDED_FOR') ?: '' : $_SERVER['HTTP_X_FORWARDED_FOR'];
    if(!empty($forwardIp)) {
        //not reliable
        $ip = explode(',', $forwardIp)[0];
    } else{
        $ip = empty($_SERVER['REMOTE_ADDR']) ? getenv('REMOTE_ADDR') ?: '' : $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}


function getRealIp()
{
    return empty($_SERVER['REMOTE_ADDR']) ? getenv('REMOTE_ADDR') ?: '' : $_SERVER['REMOTE_ADDR'];
}
