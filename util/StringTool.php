<?php

/**
 * @see  https://en.wikipedia.org/wiki/Base36
 */
class StringTool
{
    private static $base16CodeTable = '0123456789ABCDEF';

    private static $base32CodeTable = '0123456789bcdefghjkmnpqrstuvwxyz';

    private static $base64CodeTable = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

    /**
     * special code table
     * @var string
     */
    private static $base36CodeTable = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    private static $base58CodeTable = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz';

    //Bitcoin addresses  123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz
    //Ripple addresses rpshnaf39wBUDNEGHJKLM4PQRST7VWXYZ2bcdeCg65jkm8oFqi1tuvAxyz
    //short URLs for Flickr    123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ


    public static function stringCross(string $str1, string $str2)
    {
        $minLen = min(strlen($str1), strlen($str2));
        $str = '';
        for ($i = 0; $i < $minLen; $i++) {
            $str .= $str1[$i] . $str2[$i];
        }
        return $str . substr(isset($str1[$minLen]) ? $str1 : $str2, $minLen);
    }

    public static function binaryToBase32Code()
    {

    }

    public static function binaryToBase64Code()
    {

    }

    public static function binaryToString($binaryStr, $bit = 32, array $codeTable = null)
    {
        $len = log($bit, 2);
        if(!$binaryStr || !is_int($len) || !is_int(strlen($binaryStr) / $len) || count($codeTable) < $bit){
            return '';
        }
        $charArr = str_split($binaryStr, $len);
        $str = '';
        foreach ($charArr as $binary) {
            $str .= $codeTable[bindec($binary)];
        }
        return $str;
    }

    public static function stringToBinary($str, $bit = 32, array $codeTable = null)
    {

    }


    public static function numerToString($number, $bit)
    {

    }

    public static function stringToNumber($number, $bit)
    {

    }
}
