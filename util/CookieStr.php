<?php

class CookieStrUtil
{
    /**
     * parseCookieStr
     *
     * @param  string $str
     * @return array
     */
    public static function parseCookieStr(string $str): array
    {
        return $str ? (function (array $arr) {
            return $arr ? array_combine(array_column($arr, 0), array_column($arr, 1)) : [];
        })(array_map(function ($val) {
            return explode('=', $val);
        }, explode('; ', $str))) : [];
    }

}
