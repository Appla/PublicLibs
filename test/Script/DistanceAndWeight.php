<?php
// 地理位置远近和权重算法
$values = [
    0.0001,
    0.0002,
    0.1,
    0.2,
    0.3,
    0.4,
    0.5,
    0.6,
    0.7,
    0.8,
    0.9,
    1,
    1.1,
    1.2,
    1.8,
    1.9,
    2.3,
    2.9,
    4,
    6,
    7,
    9.155,
    10,
    15,
    26.1194,
];
$base = 8;
function calcPriority($val)
{
    global $base;
    return sqrt($base/$val);
}

function calcPriorityOld($val)
{
    global $base;
    return $base/$val;
}
$newArr = array_map(null, array_map('calcPriority', $values), $values, array_map('calcPriorityOld', $values));
$last = null;
$tplMinus = '%s - %s = %s';
$tplDivision = '%s / %s = %s';
foreach($newArr as [$sqrt, $origin, $division]){
    if($last === null){
        $last = compact('sqrt', 'origin', 'division');
    }
    print_r([
        'base' => $base,
        'sqrt' => $sqrt,
        'origin' => $origin,
        'division' => $division,
        'sqrt-' => sprintf($tplMinus, $last['sqrt'], $sqrt, $last['sqrt'] - $sqrt),
        'division-' => sprintf($tplMinus, $last['division'], $division, $last['division'] - $division),
        'origin-' => sprintf($tplMinus, $origin, $last['origin'], $origin - $last['origin']),
        'sqrt/origin' => sprintf($tplDivision, $sqrt , $origin , $sqrt / $origin),
        'division/origin' => sprintf($tplDivision, $division , $origin ,$division/$origin),
        'division/sqrt' => sprintf($tplDivision, $division , $sqrt ,$division/$sqrt),
    ]);
    $last = compact('sqrt', 'origin', 'division');
}
