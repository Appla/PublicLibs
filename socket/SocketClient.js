const SOCKET_SERVER_URI = 'ws://192.168.1.21:8888';
let skt = new WebSocket(SOCKET_SERVER_URI);
skt.addEventListener('open', event => console.log('connect socket sever successful', event));

skt.addEventListener('message', event => console.log('message :' + event.data));

skt.addEventListener('onclose', event => console.log('good bye', event));