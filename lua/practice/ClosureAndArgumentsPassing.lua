
local val, arr, obj = 10086, {10086}, {value = 10086};
-- val, arr, obj = 10086, {10086}, {value = 10086};


local funcVal = function()
    val = val + 1;
    return val;
end;

local funcArr = function()
    arr[1] = arr[1] + 1;
    return arr[1];
end;

local funcObj = function()
    obj.value = obj.value + 1;
    return obj.value;
end;


-- will replacing with new value,because it modified the same variable with the above declared
-- val, arr, obj = 100, {100}, {value = 100};
-- still using the old variables, building the scope in function assignments. because the following is in different scope with the above
local val, arr, obj = 100, {100}, {value = 100};

-- there is no arguments passing, bug will search from it's scope, chain scope or upvalues
print(val, funcVal(), val, funcVal());
print(arr[1], funcArr(), arr[1], funcArr());
print(obj.value, funcObj(), obj.value, funcObj());

