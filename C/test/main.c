#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// #define swap(type, x, y)	do{type tmp = x, x = y, y = tmp}while(0)


int hex_to_int(char[]);
int any(char[], char[]);
char* squeeze(char[], char[]);
int setbits(int, int, int, int);
int invert(int, int, int);
char* reverse_str(char[] , int, int);


// int strlen(char[]);

int main() {

    // ((__locale_ctype_ptr()+sizeof(""[__c]))[(int)(__c)])
    char str_a[] = "1234";
    char *str_b = "1234", *p = &str_a[1];
    // str_b[1] = 'Z';
    printf("%c,%c\n", *str_b, *p);
    printf("%c,%c\n", *str_b++, *p--);
    printf("%c,%c\n", *str_b, *p);
    // int x = 5, y = 1;

    // swap("int", x , y);
    // printf("swap test: x %d, y: %d\n", x, y);

//    char str[] = "0XAZ3f";
//    int num = hex_to_int(str);
//    printf("you input is %s, convert to int is %d\n", str, num);
//    char s1[] = "abcde", s2[] = "1234ea";
//    printf("string 1: %s, string 2:%s, squeezed string 1 is %s, any char in string 1 also in string 2 the first position is %d\n", s1, s2, squeeze(s1, s2), any(s1, s2));
//    234
    // printf("the result is %d\n", setbits(238, 3, 2, 2));
    // char str[] = "ABCDEFG";
    // printf("new str: %s\n", reverse_str(str, 0, 6));
    return 0;
}

char* reverse_str(char str[], int left, int right) {
    if(left < right) {
        char tmp = str[left];
        str[left] = str[right];
        str[right] = tmp;
        reverse_str(str, ++left, --right);
    }
    return &str[0];
}

int str_len(char str[]) {
    int i = 0, count = 0;
    while (str[i++] != '\0') {
        count++;
    }
    return count;
}

/**
 * 将num邮编数(0开始) 第 position 位右边的 count 取反
 * @param num
 * @param position
 * @param count
 * @return
 */
 int invert(int num, int position, int count) {
    int left_width = position + 1 - count;
    //1.先取出对应字段,取反,然后合并?
    //num | ~(~0 << )
    return 0;
}

/**
 * num 右边数第position位开始的 count 个值替换为 replacement
 * @param num
 * @param position
 * @param count
 * @param replacement
 * @return
 */
int setbits(int num, int position, int count, int replacement) {
//    11101110
//      11110011
//      11111111
//      11110000
//    3
//    2
//    10
//    1101010
    unsigned int left_width = position + 1 - count, i, j, k;
    //1.取num最后并与补充的值合并, num右边置零,合并
    return num & ~(~0 << left_width) | replacement << left_width | (num >> (position + 1)) << (position + 1);
//    旧的
//    return ((~0 << (position + 1)) | (~(~0 << left_width)) | replacement << left_width) & num;
//    i = (~0 << (position + 1)); //11110000
//    j = (~(~0 << left_width));  //000000011
//    k = replacement << left_width; //1000
//    i |= j;
//    i &= num;
//    i |= k;
//    return i;
}

/**
 * 删除字符串1中,出现在字符串二里的字符
 * @param s1
 * @param s2
 * @return
 */
int any(char s1[], char s2[]) {
    int i = 0, j = 0;
    char current, compared;
    while ((current = s1[i++]) != '\0') {
        while ((compared = s2[j++]) != '\0') {
            if (current == compared) {
                return i;
            }
        }
    }
    return -1;
}

/**
 *
 * @param s1
 * @param s2
 * @return
 */
char* squeeze(char s1[], char s2[]) {
    int i = 0, j = 0, n = 0;
    char current, compared;
    while ((current = s1[i++]) != '\0') {
        j = 0;
        while ((compared = s2[j++]) != '\0') {
            if (current == compared) {
                break;
            }
        }
        if(compared == '\0'){
            s1[n++] = current;
        }
    }
    s1[n] = '\0';
    char* ptr = &s1[0];
    return ptr;
}

/**
 * hex to int
 * @param char[] str
 * @return int
 */
int hex_to_int(char str[]) {
    int num = 0, i = 0, tmp = 0;
    char current;
    if (str[0] == '0' && (str[1] == 'x' || str[1] == 'X')) {
        i = 2;
    }
    for (; (current = str[i]) >= '0' && current <= '9' ||
           (current >= 'a' && current <= 'f' || current >= 'A' && current <= 'F'); i++) {
        if (current == '\0') {
            break;
        }
        tmp = 0;
        switch (current) {
            case 'a':
            case 'A':
                tmp = 10;
                break;
            case 'b':
            case 'B':
                tmp = 11;
                break;
            case 'c':
            case 'C':
                tmp = 12;
                break;
            case 'd':
            case 'D':
                tmp = 13;
                break;
            case 'e':
            case 'E':
                tmp = 14;
                break;
            case 'f':
            case 'F':
                tmp = 15;
                break;
            default:
                tmp = current - '0';
        }
        num = 16 * num + tmp;
    }
    return num;
}