(define (abs num)
  (if (> num 0) num (- num)))
(define (reciprocal num)
  (if (= num 0)
      #f
      (/ 1 num)))
(define (num2asc num)
  (if (and (> num 32) (< num 127))
      (integer->char num)
      #f))

(define (num* a b c)
  (if (and (> a  0) (> b 0) (> c 0))
      (* a b c)
      #f))
(define (num-* a b c)
  (if (or (< a 0) (< b 0) (< c 0))
      (* a b c)
      #f))


(let ((i 1) (j (+ i 2)))
  (* i j))
