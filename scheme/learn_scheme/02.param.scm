;short way for pass params function defined
(define (plus num_a num_b)
  (+ num_a num_b))
;original way for pass params function defined
(define minus (lambda (num_a num_b)
  (- num_a num_b)))

(define (p1 num)
    (- num 1))
(define (m1 num)
    (+ num 1))
