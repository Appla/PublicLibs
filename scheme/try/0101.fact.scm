(define (fact num)
  (if (= num 1)
      1
      (* num (fact (- num 1)))))
(fact 5)

(define (fact-tail num) (fact-rec num 1))

(define (fact-rec current all)
  (if (zero? current)
  all
  (fact-rec (- current 1) (* current (if (number? all) all 1)))
))

(fact-tail 5)


;
