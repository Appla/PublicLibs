;一个接受三个实数作为参数的函数，若参数皆为正数则返回它们的乘积。
(define (postitive? x)
    (if (>= x 0)
    #t
    #f
    )
)
(define (postiveNumSum x y z) 
    (if (and
    (postitive? x)
    (postitive? y)
    (postitive? z)
    )
    (* x y z)
    #f)
)
(postiveNumSum 1 2 3)
(postiveNumSum 1 2 -3)
;一个接受三个实数作为参数的函数，若参数至少一个为负数则返回它们的乘积。
(define (negativeNumSum x y z) 
    (if (or
    (negative? x)
    (negative? y)
    (negative? z)
    )
    (* x y z)
    #f)
)
(negativeNumSum 1 2 3)
(negativeNumSum 1 2 -3)