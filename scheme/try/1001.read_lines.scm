
;编写函数(read-lines)，该函数返回一个由字符串构成的表，分别代表每一行的内容。在Scheme中，换行符是由#\Linefeed表示。下面演示了将该函数用于"hello.txt"的结果。

;

;(read-lines "hello.txt") ⇒ ("Hello world!" "Scheme is an elegant programming language.")