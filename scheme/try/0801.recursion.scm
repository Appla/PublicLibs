;递归
;用于统计表中元素个数的my-length函数。（length是一个预定义函数）。
(define my-length 
    (lambda (lst) (if (null? lst) 0 (+ 1 (my-length (cdr lst)))))
)
(my-length (list 1 2 3 4 5 6 7))

;一个求和表中元素的函数。
(define (list-sum lst) 
    (if (null? lst) 0 (+ (if (number? (car lst)) (car lst) 0) (list-sum (cdr lst)))))
    (list-sum (list 1 2 3 4 5 6 7)) 
;一个分别接受一个表ls和一个对象x的函数，该函数返回从ls中删除x后得到的表。

;another bad implementation
(define (list-remove-item lst item) 
    (cons (if (null? lst) '() (if (equal? item (car lst)) '() (car lst))) (if (null? lst) (list) (list-remove-item (cdr lst) item)))
)
;使用lambda的局部变量保存每次有效的那个值,最后组合在一起.
(define (list-remove-item lst item) 
    (if (null? lst) 
        (list) 
        (let ((current (car lst)))
            ((if (equal? current item) 
                (lambda (x) x)
                (lambda (x) (cons current x))
            )
            (list-remove-item (cdr lst) item))
        )   
    )
)
(list-remove-item (list 1 2 3 4 5 "d" "x") "x")

;一个分别接受一个表ls和一个对象x的函数，该函数返回x在ls中首次出现的位置。索引从0开始。如果x不在ls中，函数返回#f。 
(define (indexOf lst item) 
    (indexOfList lst item 0)
)
(define (indexOfList lst item index)
    (if (null? lst)
        #f
        (if (equal? item (car lst))
            index
            (indexOfList (cdr lst) item (+ 1 index))
        )
    )
)
(indexOf (list 1 2 3 4 5 8 6 9 66) 1)
;比较,累加(暂时没发现全局保存状态的方法),中断(没发现)