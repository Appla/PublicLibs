<?php
namespace service\address;

require dirname(__FILE__) . '/../../web/new/cron.php';


use artisan\config;

class BranchCodeUtil
{

    const CODE_MAP_ID = 'shop_1st2nd_code';
    private static $allBranchCodes;
    private static $subBranchCodes;
    private static $subBranchCache;

    private static $branchInfo;
    private static $currentBranchCode;
    private static $firstBranchName;

    /**
     * @param $branchCode
     * @return mixed
     */
    public static function getBranchInfoByBranchCode($branchCode){
        $matched = [];
        if($branchCode && ctype_alnum($branchCode)){
            static::$currentBranchCode = $branchCode;
            if(!isset(static::$subBranchCache[$branchCode])){
                static::init(config::get(static::CODE_MAP_ID));
                $found = 0;
                if(static::$subBranchCodes && ($found = $left = count(explode($branchCode, static::$allBranchCodes)) - 1)){
                    foreach (static::$subBranchCodes as $key => $value) {
                        if(!$left){
                            break;
                        }
                        if(strpos($value, $branchCode) !== false){
                            $matched[] = $key;
                            $left--;
                        }
                    }
                }
                //using first matched for now
                static::$firstBranchName = $matched[0];
                if($found && static::$firstBranchName){
                    static::getBranchInfo();
                }
                static::$subBranchCache[$branchCode] = $found >= 1 ? static::$branchInfo : [];
            }
        }
        return static::$subBranchCache[$branchCode];
    }

    /**
     * data init
     *
     * @param  array  $codes
     */
    private static function init(array $codes)
    {
        static::$branchInfo = [];
        static::$firstBranchName = '';
        if(static::$subBranchCodes === null){
            foreach ($codes as $key => $branchs) {
                //remove first_code && center_code
                static::$subBranchCodes[$key] = implode(',', array_keys($branchs));
            }
            static::$allBranchCodes = implode('|', static::$subBranchCodes);
        }
    }

    /**
     * getBranchInfo
     *
     * @return array
     */
    private static function getBranchInfo()
    {
        if($branchInfo = config::get(static::CODE_MAP_ID)[static::$firstBranchName]){
            static::$branchInfo = [
                'first_code' => $branchInfo['first_code'],
                'center_code' => $branchInfo['center_code'],
                'first_branch_name' => static::$firstBranchName,
            ] + $branchInfo[static::$currentBranchCode];
        }
        return static::$branchInfo;
    }
}

$branchCode = '201205';
// $d[] = BranchCodeUtil::getBranchInfoByBranchCode($branchCode);
// $d[] = BranchCodeUtil::getBranchInfoByBranchCode($branchCode);
// $d[] = BranchCodeUtil::getBranchInfoByBranchCode($branchCode);
// $d[] = BranchCodeUtil::getBranchInfoByBranchCode($branchCode);
// $d[] = BranchCodeUtil::getBranchInfoByBranchCode($branchCode);
// $d[] = BranchCodeUtil::getBranchInfoByBranchCode($branchCode);
$d[] = BranchCodeUtil::getBranchInfoByBranchCode($branchCode);
// $d[] = array_keys(config::get(BranchCodeUtil::CODE_MAP_ID));
var_dump($d);
