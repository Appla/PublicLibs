<?php
require __DIR__ . '/../../web/new/cron.php';
use artisan\db;

class TimeCostAnalysis
{
    const DB_NAME = 'dts';
    /**
     * @var \artisan\db;
     */
    private static $dbInstance;

    /**
     * main
     */
    public function main()
    {
        $this->timeCost();
    }

    private function timeCost()
    {
        // $sql = 'SELECT id,request_at,response_at FROM open_api_log WHERE api = "/v1/addressLib/querySortingCode" AND id > 5131971 AND id < 5132997';
        $sql = 'SELECT id,request_at,response_at FROM open_api_log WHERE api = "/v1/addressLib/querySortingCode" AND id > 5060866 AND id < 5094855';

        // $sql = 'SELECT id,request_at,response_at FROM open_api_log WHERE api = "/v1/addressLib/querySortingCode" AND id > 5132997';
        // $sql .= ' LIMIT 100';
        $data = $this->query($sql);
        $results = [];
        if($data){
            foreach ($data as $val) {
                $results[$val['id']] = strtotime($val['response_at']) - strtotime($val['request_at']);
            }
        }
        $total = array_sum($results);
        $count = count($results);
        $status = [
            'total' => $total,
            'count' => $count,
            'avg' => $total / $count,
            'max' => max($results),
            'min' => min($results),
        ];
        var_dump($status, $results);
    }

    /**
     * @param string $sql
     * @param \artisan\db $db
     * @return array|bool|int|mixed
     */
    private function query($sql = '', db $db = null)
    {
        if(!$sql){
            return false;
        }elseif ($db === null) {
            if(!self::$dbInstance){
                self::$dbInstance = new db();
                self::$dbInstance->config = [
                    'host' => '10.168.177.85',
                    'user' => 'reader',
                    'pass' => 'Ut90CVgeOwwwfL%d',
                    'db' => 'dts',
                    'port' => '3311',
                    // �ӿ�����
                    'slaves' => []
                ];
            }
            $db = self::$dbInstance;
        }

        if (($result = $db->query($sql)) && $result instanceof mysqli_result) {
            if(function_exists('mysqli_fetch_all')){
                $result = $resource->fetch_all($fetch_type);
            }else{
                while ($row = $resource->fetch_array($fetch_type)) {
                    $result[] = $row;
                }
            }
        }
        return $result;
    }
}

(new TimeCostAnalysis())->main();
