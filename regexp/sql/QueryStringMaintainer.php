<?php

class QueryStringMaintainer
{
    public static function filterWhereCondition($sql)
    {
        return preg_replace_callback('~(where\s+)(.+)(group|having|order|limit|procedure|into|for|$)?~i', function($match) {
            return preg_replace('~([a-zA-Z_\.]+\s*(?:[=><]|>=|<=|!=)\s*)(\d+)~', ' \1\'\2\' ', $match[1].$match[2].$match[3]) ?: $match[0];
        }, $sql);
    }
}
