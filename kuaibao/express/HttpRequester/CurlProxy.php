<?php

namespace service\express\HttpRequester;

use service\express\HttpRequester\Curl;

class CurlProxy extends Curl
{
    /**
     * proxy
     *
     * @var string
     */
    private $proxy;

    /**
     * constructor
     *
     * @param string $proxy
     */
    public function __construct($proxy)
    {
        $this->proxy = $proxy;
    }

    /**
     * request via proxy
     *
     * @param  string $url
     * @param  string $method
     * @param  array|string $post_data
     * @param  array  $options
     * @return mixed
     */
    public function request($url, $method = 'get', $post_data = null, array $options = [])
    {
        if(!static::isValidProxyFormat($this->proxy)){
            $this->error = 'proxy is not set or value is invalid format';
        }
        $options = array_replace($options, [CURLOPT_PROXY => $this->proxy]);
        return parent::request($url, $method, $post_data, $options);
    }

    /**
     * getProxy
     *
     * @return string|null
     */
    public function getProxy(){
        return $this->proxy;
    }

    /**
     * setProxy
     *
     * @param string $proxy
     * @return string|null
     */
    public function setProxy($proxy)
    {
        if(static::isValidProxyFormat($proxy)){
            return $this->proxy = $proxy;
        }
    }

    /**
     * isValidProxyFormat
     *
     * @param  string  $proxy
     * @return boolean
     */
    protected static function isValidProxyFormat($proxy)
    {
        return (bool) preg_match('/^(?:\d{1,3}\.){3}(?:\d{1,3}(?:\:\d{2,5}))$/', $proxy);
    }
}
