<?php

namespace service\express;

use service\express\Contracts\HttpRequestContract;
use service\express\Exceptions\HttpRequestException;
use service\express\Contracts\DataSourceContract;
use service\express\Exceptions\DataSourceException;

use service\express\DataSourceAbstract;

use service\express\HttpRequester\Curl;
use service\express\HttpRequester\CurlProxy;

use service\express\Config;

use service\proxy\proxylist;



class ExpressQueryHandler
{
    /**
     * store all requester
     *
     * @var array
     */
    protected $requesters = [];

    /**
     * store all data source instance
     *
     * @var array
     */
    protected $data_sources = [];

    /**
     * errors
     *
     * @var array
     */
    private $errors;
    /**
     * query
     *
     * @param  string $waybill_no
     * @param  string $brand
     * @return array|string
     */
    public function query($waybill_no, $brand)
    {
        //根据品牌查询数据源
        //根据(数据源|品牌)选择是否代理
        //查询数据源参数情况,默认参数查询吧  先单号,后品牌??? 最好能自行解析,有个映射表,可以根据那个处理啊. isSupportedBrand() | isNeedBrand()
        //根据参数情况,构造,然后根据请求情况构造
        //生成数据源对象,传入参数,请求data
    }

    /**
     * getData
     * @todo 放着看看
     * @return mixed
     */
    protected function getData($express_no, $brand = null)
    {
        if(!$brand){
            return false;
        }
        try{
            $source = $this->getDataSource($brand);
            if($data = $source->getData($express_no, $brand)){
                return $data;
            }
        }catch(DataSourceException $e){
            $this->setError($e, 'getData datasource exception');
            return false;
        }catch(HttpRequestException $e){
           $this->setError($e, 'getData http exception');
            return false;
        }
    }

    /**
     * getHttpRequester
     *
     * @param string $type
     * @return HttpRequestContract
     */
    protected function getHttpRequester($type)
    {
        if(isset($this->requesters[$type])){
            return $this->requesters[$type];
        }
        $this->getProxy();
//        if($type){
//
//        }
    }

    /**
     * HttpRequestContract
     *
     * @return DataSourceContract|null
     */
    protected function getDataSource($type)
    {
        $class = $this->buildDataSourceClass($type);
        if(isset($this->data_sources[$type])){
            return $this->data_sources[$type];
        }elseif(class_exists($class)){
            $requester = new $class($this->getHttpRequester($type));
            return $this->requesters[$type] = $requester;
        }
        return null;
    }

    /**
     * getProxy
     *
     * @return string
     */
    protected function getProxy()
    {

    }

    /**
     * getDataSourceClass
     *
     * @param  string $type
     * @return string
     */
    protected function buildDataSourceClass($type)
    {
        $base = '\\service\\express\\DataSources\\';
        return $base.trim($type, '\ ');
    }

    /**
     * setError
     *
     * @param mixed $error
     * @param string|null $key
     */
    protected function setError($error, $key = null)
    {
        if(!is_scalar($key)){
            $key = date('Y_m_d_H_i_s').mt_rand(1000, 9999);
        }
        $this->errors[(string)$key] = $error;
    }
}
