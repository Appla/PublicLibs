<?php

namespace service\express\Contracts;

interface HttpRequestContract
{
    /**
     * get request
     *
     * @param  string $url
     * @param  array  $options
     * @return mixed
     */
    public function get($url, array $options = []);

    /**
     * post request
     *
     * @param  string $url
     * @param  string|array $post_data
     * @param  array  $options
     * @return mixed
     */
    public function post($url, $post_data = null, array $options = []);

    /**
     * http request
     *
     * @param  string $url
     * @param  string $method request method
     * @param  string|array $post_data
     * @param  array  $options
     * @return mixed
     */
    public function request($url, $method = 'get', $post_data = null, array $options = []);

    /**
     * getError
     *
     * @return mixed
     */
    public function getError();

    /**
     * getLastRequest info
     *
     * @return array
     */
    public function getLastRequestInfo();
}
