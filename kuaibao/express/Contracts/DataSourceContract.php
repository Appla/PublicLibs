<?php

namespace service\express\Contracts;

interface DataSourceContract
{
    /**
     * getRequestUrl
     *
     * @return string
     */
    public function getRequestUrl();

    /**
     * getData from
     *
     * @param string $express_no
     * @param string|null $brand
     * @return array|boolean
     */
    public function getData($express_no, $brand = null);

    /**
     * getError
     *
     * @return mixed
     */
    public function getError();
}
