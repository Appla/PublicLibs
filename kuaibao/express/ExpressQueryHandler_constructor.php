<?php

namespace service\express;

use service\express\Contracts\HttpRequestContract;
use service\express\Exceptions\HttpRequestException;
use service\express\Contracts\DataSourceContract;
use service\express\Exceptions\DataSourceException;

use service\express\DataSourceAbstract;

use service\express\HttpRequester\Curl;
use service\express\HttpRequester\CurlProxy;

use service\express\Config;

use service\proxy\proxylist;



class ExpressQueryHandler
{

    /**
     * query
     *
     * @param  string $waybill_no
     * @param  string $brand
     * @return array|string
     */
    public function query($waybill_no, $brand)
    {
        //根据品牌查询数据源
        //根据(数据源|品牌)选择是否代理
        //查询数据源参数情况,默认参数查询吧  先单号,后品牌??? 最好能自行解析,有个映射表,可以根据那个处理啊. isSupportedBrand() | isNeedBrand()
        //根据参数情况,构造,然后根据请求情况构造
        //生成数据源对象,传入参数,请求data
    }

    /**
     * getData
     * @todo 放着看看
     * @return mixed
     */
    protected function getData()
    {

    }

    /**
     * makeRequestParams 按顺序构建参数,屏蔽差异.
     *
     * @param  string $waybiill_no
     * @param  string $brand
     * @return array
     */
    protected function makeRequestParams($waybiill_no, $brand)
    {
        return [$waybiill_no, $brand];
    }

    /**
     * getDataSourceDefaultParamters
     *
     * @return status
     */
    protected function getDataSourceDefaultParamters()
    {

    }

    /**
     * getHttpRequester
     *
     * @return HttpRequestContract
     */
    protected function getHttpRequester()
    {

    }

    /**
     * HttpRequestContract
     *
     * @return DataSourceContract|null
     */
    protected function getDataSource()
    {

    }

    /**
     * getProxy
     *
     * @return string
     */
    protected function getProxy()
    {

    }

    /**
     * getDataSourceClass
     *
     * @param  string $type
     * @return string
     */
    protected function buildDataSourceClass($type)
    {
        $base = '\\service\\express\\DataSources\\';
    }
}
