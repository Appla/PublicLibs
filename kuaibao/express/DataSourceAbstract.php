<?php

namespace service\express;

use service\express\Contracts\DataSourceContract;
use service\express\Contracts\HttpRequestContract;
use service\express\Exceptions\DataSourceException;

abstract class DataSourceAbstract implements DataSourceContract
{
    /**
     * request uri
     *
     * @var string
     */
    protected $request_uri = '';

    /**
     * request uri tpl
     *
     * @var string
     */
    protected $request_uri_tpl = '';

    /**
     * request params
     *
     * @var array
     */
    protected $request_params = [];

    /**
     * origin return
     *
     * @var string
     */
    protected $origin_return;

    /**
     * error info
     *
     * @var array
     */
    protected $errors;

    /**
     * http requester
     *
     * @var HttpRequestContract
     */
    protected static $requester;

    /**
     * supportted_brands
     *
     * @var array
     */
    protected $supportted_brands = [];


    /**
     * constructor
     *
     * @param array               $params
     * @param HttpRequestContract $requester
     */
    public function __construct(HttpRequestContract $requester)
    {
        //在请求里调用较好,根据情况构建url,但是这样getRequestUrl就不能及时返回了
        static::$requester = $requester;
    }

    /**
     * getRequestUrl
     *
     * @return string
     */
    public function getRequestUrl()
    {
        return $this->request_uri;
    }

    /**
     * getData from
     *
     * @param string $express_no
     * @param string|null $brand
     * @return array|boolean
     */
    abstract public function getData($express_no, $brand = null);

    /**
     * getError
     *
     * @return string
     */
    public function getError()
    {
        return $this->errors;
    }

    /**
     * isSupportedBrand
     *
     * @param  string  $brand
     * @return boolean
     */
    public function isSupportedBrand($brand)
    {
        return is_scalar($brand) && isset($this->supportted_brands[(string)$brand]);
    }
    /**
     * parseResponse
     * handle the data source response
     *
     * @param  string $data
     * @return array
     */
    abstract protected function parseResponse($data = null);

    /**
     * setError
     *
     * @param mixed $error
     * @param string|null $key
     */
    protected function setError($error, $key = null)
    {
         if(!is_scalar($key)){
             $key = date('Y_m_d_H_i_s').mt_rand(1000, 9999);
         }
         $this->errors[(string)$key] = $error;
    }

    /**
     * buildRequestUrl
     *
     * @param  array  $params
     * @return string
     */
    protected function buildRequestUrl(array $params)
    {
        if(mb_substr_count($this->request_uri_tpl, '%s') !== count($params)){
            static::throwException('error parameters provided for url');
        }
        return $this->request_uri = vsprintf($this->request_uri_tpl, $params);
    }

    /**
     * throw a DataSource Exception
     *
     * @param  string  $msg
     * @param  integer $code
     * @param  throwable|null $previous
     * @return void
     */
    protected static function throwException($msg, $code = 1, $previous = null)
    {
        throw new DataSourceException($msg, $code, $previous);
    }
}

