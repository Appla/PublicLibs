<?php
/**
 * Created by PhpStorm.
 * User: Appla
 * Date: 2016/12/20
 * Time: 16:16
 */

namespace service\Address;

class AddressParserTest
{
    public function run()
    {
        db::connect();

        $parser = new AddressParser();
        $str = <<<JSON
        [
           {
                "waybillNo": "123",
                "geo": {
                    "address": "通协路269号建滔广场6号楼6楼A单元",
                    "formatted_address": "上海市长宁区建滔广场|6号楼",
                    "province": "上海市",
                    "citycode": "021",
                    "city": "上海市",
                    "district": "长宁区",
                    "adcode": "310105",
                    "location": "121.355444,31.225864",
                    "level": "门牌号"
                }
            }
        ]
JSON;

        $jsonArr = json_decode($str, true);

        $data = reset($jsonArr);
        $data = $parser->analysis($data);

    }
}