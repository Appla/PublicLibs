# encoding: UTF-8

# START@#2017-06-#

# exit(0);
# END@#2017-06-#


# START@#2017-06-27#
['kuaidihelp_address',
'kuaidihelp_order2',
'kuaidihelp_dts',
'kuaidihelp_shop2',
'kuaidihelp_wdadmin',
'kuaidihelp_express',
'kuaidihelp_admin',
'kuaidihelp_taobao',
'kuaidihelp_area',
'kuaidihelp_core',
'kuaidihelp_report',
'kuaidihelp_news',
'kuaidihelp_tel',
'kuaidihelp_fenci',
'kuaidihelp_friendlink',
'kuaidihelp_price',
'kuaidihelp',
'kuaidihelp_open',
'kd_message',
'kdhelp',
'kuaidihelp_user',
'kuaidihelp_statics',
'kuaidihelp_match',
'kuaidihelp_shixiao',
'kuaidihelp_m',
'kuaidihelp_shop',
'kuaidihelp_order',
'kuaidihelp_news2',
'kuaidihelp_zt',
'kuaidihelp_sto',
'kuaidihelp_finance',
'kuaidihelp_weixin',
'kuaidihelp_liuyan',
'kuaidihelp_shorturl',
'kuaidihelp_port',
'kuaidihelp_kdy',
'kuaidihelp_ckd',
'kuaidihelp_pda',
'kuaidihelp_app',
'kuaidihelp_login',
'kuaidihelp_mp',
'kuaidihelp_stoasw_weixin',
'kuaidihelp_stoasw',
'kuaidihelp_realname',
'kuaidihelp_zto',
'kuaidihelp_monitor'].each {|name| puts "ssh://git@gitlab.kuaidihelp.com:2020/PHP/#{name}.git"}

exit(0);
# END@#2017-06-27#

# START@#2017-06-26#

p /FROM\s(tbl_\S+).*amap_hash\s*=\s*(['"])(.[^'"]+)\2\s/ =~ "SELECT tbl_addresses_6168.* FROM tbl_addresses_6168 WHERE tbl_addresses_6168.amap_hash = 'w7mbj808b5py'  ORDER BY tbl_addresses_6168.update_date DESC  LIMIT 50"
p $1, $2, $3, $~
exit(0);
# END@#2017-06-26#


# START@#2017-06-20#
str =<<DOC
-- 海南省三亚市天涯区天涯镇 [w7m8puc8kxd1]  8930
SELECT COUNT(*) FROM tbl_addresses_6168 WHERE amap_hash = 'w7m8puc8kxd1'


-- 广东省汕头市潮阳区海门镇[ws4gvyqnzske] [5151005559]  7708
SELECT COUNT(*) FROM tbl_addresses_5967 WHERE amap_hash = 'ws4gvyqnzske'

-- 上海市青浦区腾新路65号上海捷利跨境转运仓[wtw1u4db8c8n, wtw1u46tzf60]  73, 191
SELECT COUNT(*) FROM tbl_addresses_2 WHERE amap_hash = 'wtw1u4db8c8n'

-- 湖南省株洲市天元区泰山路街道
SELECT COUNT(*) FROM tbl_addresses_7164 WHERE amap_hash = 'wsbqq2z1vhsu'


-- 广东省深圳市宝安区沙井街道    8518
SELECT COUNT(*) FROM tbl_addresses_5947 WHERE amap_hash = 'ws0cmf7hr548'

-- 广东省东莞市长安镇   6752/297511
SELECT COUNT(*) FROM tbl_addresses_6131 WHERE amap_hash = 'ws0cv9kkqpzx'

-- 广东省東莞市虎門鎮赤崗富馬工業園點買貨運中轉站     11626
SELECT COUNT(*) FROM tbl_addresses_6131 WHERE amap_hash = 'ws0f59g5d11x'

-- 上海市奉贤区南桥镇   5770
SELECT COUNT(*) FROM tbl_addresses_2 WHERE amap_hash = 'wtqrgcg9dpgc'

-- 广东省佛山市南海区大沥镇   12935
SELECT COUNT(*) FROM tbl_addresses_5982 WHERE amap_hash = 'ws07myuq6ybm'


-- 广东省东莞市虎门镇   13198
SELECT COUNT(*) FROM tbl_addresses_6131 WHERE amap_hash = 'ws0cf9mf43qp'

-- 广东省佛山市南海区大沥镇   12935/231630
SELECT COUNT(*) FROM tbl_addresses_5982 WHERE amap_hash = 'ws07myuq6ybm'

-- 浙江省温州市鹿城区滨江街道 5645
SELECT COUNT(*) FROM tbl_addresses_843 WHERE amap_hash = 'wsvz66hb9jcv'
DOC
# str.each_line {|line| line if line.start_with?('SELECT')}.split("\n").each {|line|
#     printf("DELETE FROM %s WHERE amap_hash = '%s' AND service_count < 4 AND update_date < '2017-04-01';\n", $1, $2) if /(tbl_\S+)[^']+'([^']+)'/ =~ line
# }

a = str.each_line lambda {|line| return line if line.start_with?('SELECT')}

p a.next()
# p a.each {|item| p item.class}


# p str



exit(0);
# END@#2017-06-20#



# START@#2017-06-16#
[
    '陕西省',
    '河北省',
    '广东省',
    '福建省',
    '浙江省',
    '海南省',
    '江苏省',
    '湖南省',
    '辽宁省',
    '湖北省',
    '吉林省',
    '云南省',
    '江西省',
    '河南省',
    '内蒙古自治区',
    '贵州省',
    '安徽省',
    '广西壮族自治区',
    '山西省',
    '新疆维吾尔自治区',
    '甘肃省',
    '四川省',
    '山东省',
    '天津',
    '西藏自治区',
    '黑龙江省',
    '重庆',
    '上海',
    '北京',
    '青海省',
    '宁夏回族自治区',
].select {|item| !['陕西省','河北省','辽宁省','上海','福建省','浙江省',
    '海南省','江苏省','四川省','广东省',
    '湖北省','吉林省'].include?(item)}.each {|i| print "getProvinceBranch('#{i}') AS '#{i}',\n"}
exit();
# END@#2017-06-16#
