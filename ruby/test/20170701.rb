# encoding: UTF-8

# START@#2017-07-#

# exit(0);
# END@#2017-07-#

# START@#2017-07-06#

['190302/192250=98.9867%',
'201918/203150=99.3936%',
'213644/216637=98.6184%',
'177322/181854=97.5079%',
'143177/145830=98.1808%',
'192000/194367=98.7822%',
'184183/185065=99.5234%',
'232189/235247=98.7001%',
'225145/229173=98.2424%',
'243889/246249=99.0416%',
'171233/178282=96.0462%',
'227923/230050=99.0754%',
'17292/17837=96.9446%',
'190300/193574=98.3087%',
'95501/95850=99.6359%',
'34384/35027=98.1643%',
'109679/110929=98.8732%',].zip ['湖南省',
'云南省',
'江西省',
'河南省',
'内蒙古自治区',
'贵州省',
'广西壮族自治区',
'山西省',
'新疆维吾尔自治区',
'甘肃省',
'山东省',
'天津',
'西藏自治区',
'黑龙江省',
'重庆',
'青海省',
'宁夏回族自治区',] {|array|
    # puts array
    puts "#{array[0]} => #{array[1]}"
}

exit(0);
# END@#2017-07-06#

# START@#2017-07-05#
require 'Date'
require "redis"

time_log = Hash.new
time_log[:start] = Time.now

redis_config = {
    :host => '10.20.1.161',
    :port => 6380,
    # :host => '127.0.0.1',
    # :port => 6379,
    :timeout => 1
}
begin
    r = Redis.new(redis_config)
rescue => exception
    p exception
end

cusor = nil
max = 300

matched = tmp = []
matched << 2;
# pattern = 'address:codeQuery:*'
pattern = 'address:codeQuery:tbl_addresses_*'

while ((max-=1) > 0 && cusor != '0')
    cusor, tmp = r.scan(cusor, :match => pattern, :count => 10000)
    matched += tmp;
    # p cusor, matched
end
time_log[:get_key] = Time.now


map = Hash.new
matched.each {|key|
    new_str = ''
    (map[key] = new_str.gsub(/:2017_\d{2}_\d{2}$/, '')) if (key.is_a? String) && (new_str = key.gsub(/(tbl_addresses_\d+)([^:\d]+)/, '\2:\1')) != key
}

time_log[:build_keys] = Time.now

# map.to_a.sample(300).to_h.each {|old_key, new_key| p old_key, new_key}


total, current = map.size, 1000
current = total
processed = current
map.each {|old_key, new_key|
# map.to_a.sample(1000).to_h.each {|old_key, new_key|
    # r.pipelined {
        # begin
            p "left: #{current -= 1}"
            # r.RENAMENX(old_key, new_key)
            if r.RENAMENX(old_key, new_key) === 0
                old_val = r.get(old_key).to_i
                r.incrby(new_key, old_val)
                r.del(old_key)
            end
        # rescue => exception
        #     p exception.message
        # end
    # }
}
time_log[:clean_keys] = Time.now
start = time_log[:start]
time_log.each {|key, time|
    p "#{key}, #{time - start}"
}
p "clean time cost : #{time_log[:clean_keys] - time_log[:build_keys]}"
p "left: #{total - processed}, total: #{total}, processed: #{processed}"
exit(0);


# END@#2017-07-5#

# START@#2017-07-04#

template = <<DOC
[submodule "%s"]
    path = %s
    url = %s
DOC
['ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_address.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_order2.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_dts.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_shop2.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_wdadmin.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_express.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_admin.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_taobao.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_area.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_core.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_report.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_news.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_tel.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_fenci.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_friendlink.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_price.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_open.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kd_message.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kdhelp.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_user.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_statics.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_match.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_shixiao.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_m.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_shop.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_order.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_news2.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_zt.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_sto.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_finance.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_weixin.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_liuyan.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_shorturl.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_port.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_kdy.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_ckd.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_pda.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_app.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_login.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_mp.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_stoasw_weixin.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_stoasw.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_realname.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_zto.git',
'ssh://git@gitlab.kuaidihelp.com:2020/PHP/kuaidihelp_monitor.git'].each { |url|
    if /PHP\/(?<name>[^.]+)\.git/ =~ url
        printf(template, name, name, url)
    end
}
exit(0);
# END@#2017-07-#
