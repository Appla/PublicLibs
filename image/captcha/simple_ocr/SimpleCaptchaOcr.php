<?php
/**
 * @see http://blog.csdn.net/ugg/article/details/3953137
 */
class SimpleCaptchaOcr
{
    protected $image_path;
    protected $data_array;
    protected $image_size;
    protected $data;
    protected $fragments;
    protected $num_string_arr;

    /**
     * __construct
     *
     * @param string $image
     */
    public function __construct($image = null, arrya $fragments = null)
    {
        if ($fragments) {
            $this->setFragments($fragments);
        } else {
            $this->fragments = [
                '0' => '000111000011111110011000110110000011110000011110000011110000011110000011110000011110000011011000110011111110000111000',
                '1' => '000111000011111000011111000000011000000011000000011000000011000000011000000011000000011000000011000011111111011111111',
                '2' => '011111000111111100100000110000000111000000110000001100000011000000110000001100000011000000110000000011111110111111110',
                '3' => '011111000111111110100000110000000110000001100011111000011111100000001110000000111000000110100001110111111100011111000',
                '4' => '000001100000011100000011100000111100001101100001101100011001100011001100111111111111111111000001100000001100000001100',
                '5' =>
                '111111110111111110110000000110000000110000000111110000111111100000001110000000111000000110100001110111111100011111000',
                '6' => '000111100001111110011000010011000000110000000110111100111111110111000111110000011110000011011000111011111110000111100',
                '7' => '011111111011111111000000011000000010000000110000001100000001000000011000000010000000110000000110000001100000001100000',
                '8' => '001111100011111110011000110011000110011101110001111100001111100011101110110000011110000011111000111011111110001111100',
                '9' => '001111000011111110111000111110000011110000011111000111011111111001111011000000011000000110010000110011111100001111000',
            ];
        }

        if ($image && is_file($image)) {
            $this->setImage($image);
        }
    }

    /**
     * setImage
     *
     * @param string $image
     * @return void
     */
    public function setImage($image)
    {
        $this->image_path = $image;
    }

    /**
     * setFragments
     *
     * @param string $fragments
     * @return void
     */
    public function setFragments($fragments)
    {
        $this->fragments = $fragments;
    }

    /**
     * getData
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * getNumberString
     *
     * @return array
     */
    public function getNumberString()
    {
        return $this->num_string_arr;
    }

    /**
     * getResult
     *
     * @return string
     */
    public function getResult()
    {
        return $this->data_array;
    }

    /**
     * getHec
     *
     * @return array
     */
    public function getHec($img_path = null, array $limits = null)
    {
        is_file($img_path) and $this->setImage($img_path);
        $res = imagecreatefromjpeg($this->image_path);
        $size = getimagesize($this->image_path);
        $limits === null and $limits = [
            'red' => 125,
            'green' => 125,
            'blue' => 125,
        ];
        $data = [];
        for ($i = 0; $i < $size[1]; ++$i) {
            for ($j = 0; $j < $size[0]; ++$j) {
                $rgb = imagecolorat($res, $j, $i);
                $rgbarray = imagecolorsforindex($res, $rgb);
                if ($rgbarray['red'] < $limits['red'] || $rgbarray['green'] < $limits['green']
                    || $rgbarray['blue'] < $limits['blue']) {
                    $data[$i][$j] = 1;
                } else {
                    $data[$i][$j] = 0;
                }
            }
        }
        $this->image_size = $size;
        return $this->data_array = $data;
    }

    /**
     * using predefined letter offset and space to calc data
     * 获取的是横列的值
     *
     * @param  integer $word_width
     * @param  integer $word_height
     * @param  integer $offset_x
     * @param  integer $offset_y
     * @param  integer $word_spacing
     * @return array
     */
    public function cultCaptcha($word_width = 9, $word_height = 13, $offset_x = 7, $offset_y = 3, $word_spacing = 4)
    {
        if(is_array($word_width)){
            if(count($word_width) === 5){
                list($word_width, $word_height, $offset_x, $offset_y, $word_spacing) = $word_width;
            }else{
                die('error params');
            }
        }
        // 查找4个数字
        $data = ['', '', '', ''];
        for ($i = 0; $i < 4; ++$i) {
            //文字偏移量+($i*(文字宽度+文字间隙))
            $x = ($i * ($word_width + $word_spacing)) + $offset_x;
            //高度偏移是固定的
            $y = $offset_y;
            //从高到低来循环
            for ($h = $y; $h < ($offset_y + $word_height); ++$h) {
                //从左到右来
                for ($w = $x; $w < ($x + $word_width); ++$w) {
                    $data[$i] .= $this->data_array[$h][$w];
                }
            }

        }
        return $this->num_string_arr = $data;
    }

    /**
     * getWords
     *
     * @param  string  $img_path
     * @param  array|null  $$fragments
     * @param  string   $type
     * @return string
     */
    public function getWords($img_path = null, array $fragments = null, $type = 'new')
    {
        if (is_file($img_path)) {
            $this->setImage($img_path);
        } else {
            if (!$this->image_path) {
                return false;
            }
        }
        if ($fragments) {
            $this->setFragments($fragments);
        }
        $data = $this->getHec();
        if ($type === 'new') {
            $data = $this->cutStr($data);
        } else {
            $data = $this->run();
        }
        return is_array($data) ? $this->calcStr($data) : '';
    }

    /**
     * calcStr
     *
     * @param  array  $data
     * @param  integer  $similar_rate
     * @return string
     */
    protected function calcStr(array $data, $similar_rate = 95)
    {
        // 进行关键字匹配
        $result = '';
        foreach ($data as $num_key => $num_string) {
            $max = 0.0;
            $num = 0;
            foreach ($this->fragments as $key => $value) {
                $percent = 0.0;
                similar_text($value, $num_string, $percent);
                if (intval($percent) > $max) {
                    $max = $percent;
                    $num = $key;
                    if (intval($percent) > $similar_rate) {
                        break;
                    }

                }
            }
            $result .= $num;
        }
        $this->data = $result;
        // 查找最佳匹配数字
        return $result;
    }

    /**
     * draw result
     *
     * @param array|null $data
     * @return void
     */
    public function draw(array $data = null)
    {
        $data === null and $data = $this->data_array;
        if ($data) {
            foreach ($data as $key => $val) {
                if (is_array($val)) {
                    $val = implode('', $val);
                } elseif (!is_string($val)) {
                    $val = (string) $val;
                }
                echo $val . PHP_EOL;
            }
        }
    }

    /**
     * cutStr
     * 使用的是竖列的值,所以要注意了....
     *
     * @param  array $data
     * @return array
     */
    public function cutStr(array $data, $min_num = 8)
    {
        $width = $this->image_size[0];
        $height = $this->image_size[1];
        $number_str = [];
        $str_count = 0;
        //从左到右
        for ($w = 0; $w < $width; $w++) {
            $tmpstr = '';
            $o = 0;
            //从上到下.
            for ($h = 0; $h < $height; $h++) {
                $tmpstr .= $data[$h][$w];
                //当为0, $o++,$h++
                $data[$h][$w] === 0 and $o++;
            }
            if ($o === $h) {
                //如果全是0，即空列
                $number_str[] = '';
                //有空列,则str_count++
                $str_count++;
                continue;
            } else {
                //若不是空列,连续的值将会合并到一起.
                if (!isset($number_str[$str_count])) {
                    $number_str[$str_count] = '';
                }
                $number_str[$str_count] .= $tmpstr;
            }
        }
        //@todo如果少于多少1，也剔除
        return $this->num_string_arr = array_values(array_filter($number_str, function($val) use($min_num){
        	return substr_count($val, '1') > $min_num;
        }));
    }

    /**
     * colorFilter
     *
     * @param  array  $point_rgb
     * @param  array  $limit_rgb
     * @return boolean
     */
    protected function colorFilter(array $point_rgb, array $limit_rgb)
    {
        return $point_rgb['red'] < $limit_rgb['red'] || $point_rgb['green'] < $limit_rgb['green'] || $point_rgb['blue'] < $limit_rgb['blue'];
    }

    /**
     * buildFragments
     *
     * @param  string $image_path
     * @param  string $image_path  1 === top -> bottom first ,0 left -> right first
     * @param  array $limits
     * @return array
     */
    public static function buildFragments($image_path, $type = 0, array $limits = null)
    {
        $res = imagecreatefromjpeg($image_path);
        $size = getimagesize($image_path);
        $width = $size[0];
        $height = $size[1];
        if ($type === 1) {
            //switch width and height
            $width = $size[1];
            $height = $size[0];
        }
        $limits === null and $limits = [
            'red' => 200,
            'green' => 200,
            'blue' => 200,
        ];
        $str = '';
        $str_arr = [];
        for ($i = 0; $i < $width; ++$i) {
            for ($j = 0; $j < $height; ++$j) {
                $rgb = imagecolorat($res, $j, $i);
                $rgbarray = imagecolorsforindex($res, $rgb);
                if ($rgbarray['red'] < $limits['red'] || $rgbarray['green'] < $limits['green'] || $rgbarray['blue'] < $limits['blue']) {
                    echo $str .= $str_arr[$i][$j] = '1';
                } else {
                    echo $str .= $str_arr[$i][$j] = '0';
                }
            }
        }
        return compact('str', 'str_arr', 'type', 'limits');
    }
}
