const fs = require('fs');
const fileName = process.argv[2] || '';

fs.readFile(fileName, 'utf-8', (err, data) => {
    if (err) {
        if (err.code === 'ENOENT') {
            return console.error('file is not exists');
        } else {
            throw err;
        }
    }
    console.log(data.split('\n').length - 1);
});