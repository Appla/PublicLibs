const http = require('http');

const requestUrls = process.argv.length > 2 && process.argv.slice(2, 5) || ['http://baidu.com', 'http://httpbin.org/ip', 'http://httpbin.org/headers'];

let promise = new Promise((resolve, reject) => {
    httpRequester(requestUrls.shift(), resolve, reject);
});

function httpRequester(requestUrl, resolve, reject) {
    return http.get(requestUrl, response => {
        let dataContainer = [];
        response.on('data', data => dataContainer.push(data));
        response.on('error', reject);
        response.on('end', () => {
            let allResponseStr = Buffer.concat(dataContainer).toString();
            resolve(allResponseStr);
        });
    }).on('error', reject);
}

promise.then(response => {
    console.log(response);
    if (requestUrls.length > 0) {
        return new Promise((resolve, reject) => {
            httpRequester(requestUrls.shift(), resolve, reject);
        });
    }
}, err => console.error).then(response => {
    console.log(response);
    if (requestUrls.length > 0) {
        return new Promise((resolve, reject) => {
            httpRequester(requestUrls.shift(), resolve, reject);
        });
    }
}, err => console.error).then(response => {
    console.log(response);
}, err => console.error);