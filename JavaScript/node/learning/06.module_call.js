var ls = require('./filtered_ls_module.js');
const [pathName, extName] = process.argv.slice(2, 4);

ls(pathName, extName, (err, fileList) => {
    if (err) {
        return console.error('There was an error:', err);
    }
    fileList.forEach(val => {
        console.log(val);
    });
});