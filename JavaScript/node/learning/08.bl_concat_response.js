const http = require('http');
const bl = require('bl');

const requestUrl = process.argv[2] || 'http://baidu.com';

http.get(requestUrl, response => {
    //pipe在接收完数据才会关闭,可以在管道关闭后处理.
    response.pipe(bl((err, data) => {
        let allResponseStr = data.toString();
        console.log(allResponseStr.length);
        console.log(allResponseStr);
    }));
}).on('error', console.error);