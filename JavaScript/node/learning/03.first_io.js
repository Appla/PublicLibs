const fs = require('fs');
let fileName = process.argv.length >= 2 ? process.argv[2] : '';
if (fileName) {
    let fb = fs.readFileSync(fileName);
    console.log(fb.toString().split("\n").length - 1);
} else {
    console.error('there is no file path provided');
}