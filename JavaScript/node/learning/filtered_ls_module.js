var fs = require('fs');
var path = require('path');

module.exports = (dirPath, extName, callback) => {
    fs.readdir(dirPath, (err, fileList) => {
        if (err) {
            return callback(err);
        }
        extName = (extName.startsWith('.') ? '' : '.') + extName;
        callback(null, fileList.filter(fileName => {
            return path.extname(fileName) === extName;
        }));
    });
};