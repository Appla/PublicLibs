const http = require('http');
const url = require('url');
const through2_map = require('through2-map');

const LISTEN_PORT = parseInt(process.argv[2]) || 8088;
const DEFAULT_PAGE_URL = 'http://cn.bing.com/';


http.createServer((req, res) => {
        if (req.method === 'POST') {
            //don't need collection all request data by you self
            req.pipe(through2_map(data => {
                return data.toString().toUpperCase();
            })).pipe(res);
        } else {
            http.get(DEFAULT_PAGE_URL + req.url, defaultRes => {
                defaultRes.pipe(res);
            });
        }
    }).on('error', console.error)
    .listen(LISTEN_PORT);