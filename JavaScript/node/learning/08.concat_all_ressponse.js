const http = require('http');

const requestUrl = process.argv[2] || 'http://baidu.com';

http.get(requestUrl, response => {
    let dataContainer = [];
    response.on('data', data => dataContainer.push(data));
    response.on('error', console.error);
    response.on('end', () => {
        let allResponseStr = Buffer.concat(dataContainer).toString();
        console.log(allResponseStr.length);
        console.log(allResponseStr);
    });
}).on('error', console.error);