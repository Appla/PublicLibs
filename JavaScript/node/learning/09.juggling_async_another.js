const http = require('http');

const requestUrls = process.argv.length > 2 && process.argv.slice(2, 5) || ['http://baidu.com', 'http://httpbin.org/ip', 'http://httpbin.org/headers'];

let responseContainer = [];


function httpRequester(requestUrl, index) {
    http.get(requestUrl, response => {
        let dataContainer = [];
        response.on('data', data => dataContainer.push(data));
        response.on('error', console.error);
        response.on('end', () => {
            responseContainer[index] = Buffer.concat(dataContainer).toString();
            if (responseContainer.length === 3) {
                arrayPrinter(responseContainer);
            }
        });
    }).on('error', console.error);
}

function arrayPrinter(array) {
    array.forEach(val => console.log(val));
}

requestUrls.forEach((url, index) => {
    httpRequester(url, index);
});