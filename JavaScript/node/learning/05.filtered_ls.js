const fs = require('fs');
const path = require('path');
let [dirName, extName] = process.argv.slice(2, 4);

extName = (extName.startsWith('.') ? '' : '.') + extName;
fs.readdir(dirName, 'utf8', (err, fileList) => {
    if (err) {
        if (err.code === 'ENOENT') {
            console.error('dir not exists');
            return;
        } else {
            throw err;
        }
    }
    let filterdFileList = fileList.filter(fileName => {
        return path.extname(fileName) === extName;
    });
    filterdFileList.map(val => {
        console.log(val);
    });
    //console.log(filterdFileList);
});
