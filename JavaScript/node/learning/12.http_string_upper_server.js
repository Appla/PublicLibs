const http = require('http');
const url = require('url');

const LISTEN_PORT = parseInt(process.argv[2]) || 8088;
const DEFAULT_PAGE_URL = 'http://cn.bing.com/';


http.createServer((req, res) => {
        if (req.method === 'POST') {
            let tmpData = [];
            req.on('data', data => {
                tmpData.push(data);
            });
            req.on('end', () => {
                res.end(Buffer.concat(tmpData).toString().toUpperCase());
            });
        } else {
            http.get(DEFAULT_PAGE_URL + req.url, defaultRes => {
                defaultRes.pipe(res);
            });
        }
    }).on('error', console.error)
    .listen(LISTEN_PORT);