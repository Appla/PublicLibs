function parseCookie() {
    let cookieStr = document.cookie;
    let cookiePair = cookieStr.split(';');
    let cookies = {};
    for (let item of cookiePair) {
        let tmpArr = item.split('=');
        cookies[tmpArr[0].trim()] = tmpArr[1].trim();
    }
    return cookies;
}

function cookieStringify(cookies) {
    if (typeof cookies !== 'object') {
        return;
    }
    let cookiesArr = [];
    for (let key of Object.keys(cookies)) {
        cookiesArr.push(key + '=' + cookies[key]);
    }
    return cookiesArr.join('; ');
}