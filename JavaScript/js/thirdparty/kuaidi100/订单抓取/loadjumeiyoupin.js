var NAME = 'jumeiyoupin';

var log = function(txt){
	console.log(txt);
}



var tostr = function(x){
	x = decodeURIComponent(x);
	var r = /\\u([\d\w]{4})/gi;
	x = x.replace(r, function (match, grp) {
		return String.fromCharCode(parseInt(grp, 16)); 
	});
	x = unescape(x);
	return x;
}

var getcookievalue = function(cookie,key){
	var reg = "(?:^|;\\s*)" + key + "\\=([^;]+)(?:;\\s*|$)";
	var b = new RegExp(reg).exec(cookie);
	var ret =  b ? b[1] : '';
	return ret?ret.trim():'';
}


var cookieParse =  function(str){
    var allPairsArray = str.split(/;/);
    var cookies = {};
    for(var i = 0; i < allPairsArray.length; i++){
        var index = allPairsArray[i].indexOf('=');
        if(index != -1){
            var key = allPairsArray[i].substr(0, index).trim();
            var value =  allPairsArray[i].substr(index+1, allPairsArray[i].length).trim();
            cookies[key] = value;
        }
    }
    return cookies;
}

var cookieBuild = function(cookie){
	var array = [];
	for(var x in cookie){
		array.push(x + '=' + cookie[x]);
	}
	return array.join('; ');
}

//从代理获取内容
var getdatafromproxy = function(config,task,cb){
	task.action = 'http';
	global.dotask(task,function(result){
		var errmessage = result.errmessage;
		var status = result.status;
		var headers = result.headers;
		var content = result.content;
		cb(errmessage,content,headers,status);
	});
}


var ioscookiesplit = /,(?!\s\d{2}-\w{3}-\d{4})/;//ios cookie has join by ','
var checkResponseCookie = function(config,headerjson){

	if(!headerjson){
		return;
	}
	
	try {
		var cookieMap = null;

		var doOne = function(value){
			var value = value.split(';')[0];
			var index = value.indexOf('=');
			if(index != -1){
				var key = value.substr(0, index).trim();
				var value =  value.substr(index+1, value.length).trim();
				if(!cookieMap){
					cookieMap = cookieParse(config.cookie);
				}
				log('setcookie>' + config.unb + ">" + config.name + ">" + config.tra + ">" + key + ">" + value);
				cookieMap[key] = value;
			}	
		}
		
		for(var x in headerjson){
			if(x.toLowerCase().indexOf('set-cookie')>=0){
				var value = headerjson[x];
				if(typeof value == 'string'){
					value = value.split(ioscookiesplit);
				} 
				if(value.length){
					for(var i = 0 ; i < value.length ; i ++){
						doOne(value[i]);
					}
				}
			}
		}
		
		if(cookieMap){
			config.cookie = cookieBuild(cookieMap);
			config.cookiechange = true;
		}
	
	} catch(e){
		console.log(e.stack);
		console.log(headerjson);
	}
	
}


var seq = 1;
var getOrderList = function(config,cb){
	var array = [];
	var sort = 1;
	var pageCount = {};
	var getonepage = function(page){

		var time = '' + new Date().getTime();
		
		var url = 'http://i.jumei.com/m/order/list?status=2'
		var task = {
			_id:NAME + time + '_' + (seq++) ,
			url:url,
			headers:{
				'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
				'Accept-Encoding':'gzip, deflate, sdch',
				'Accept-Language':'en-US,en;q=0.8',
				'Host':'i.jumei.com',
				'Cache-Control':'max-age=0',
				'Connection':'keep-alive',
				'Upgrade-Insecure-Requests':'1',
				'Referer':'http://i.jumei.com/m/order/list?status=2',
				Cookie:config.cookie
			},
			tra:config.tra,
			batch:config.batch,
			timeout:config.timeout
		};
		if(config.useragent){
			task.headers['User-Agent'] = config.useragent;
		}
		getdatafromproxy(config,task,function(err,data,headers){
			
			checkResponseCookie(config,headers);
			if(err && !pageCount[page]){//可以再来一次
				pageCount[page] = err;
				getonepage(page);
				return;
			}
			
			if(err){
				cb(err,null);
				return;
			}
			
			
			try {
				
				
				
				var doms = global.$(data);
				doms.find('div.order-list').each(function(){
					
					var dom = global.$(this);
					var product = dom.find(".list-product:first");
					var pic = product.find('.product-pic img').attr('src');
					var title = product.find('.product-info .info-text').html().trim();
					var orderid = dom.find('.order-num').html();
					
					var lk = dom.find('.list-ordernum a').attr('href');
					var innerorderid = lk.split('?')[1].split('&')[0].split('=')[1];
					
					
					array.push({
						comname:null,
						comnum:null,
						title:title,
						pic:pic,
						orderid:orderid,
						innerorderid:innerorderid
					});
					
				});
				cb(null,array);
			} catch(e){
				log(e.stack);
				cb(e,array);
			}
		});
	}
	getonepage(1);
}



var getOrderDetailFromNet = function(config,order,cb){
	var time = '' + new Date().getTime();
	
	var ref = 'http://i.jumei.com/m/shipping/ordertrack/?order_id='+order.innerorderid+'&shipping_no=' + order.orderid;
	var url = 'http://i.jumei.com/m/shipping/trackdetail?order_id='+order.innerorderid+'&shipping_no=' + order.orderid +  '&_ajax_=1';
	var map = {};
	var task = {
		_id:NAME + time + '_' + (seq++),
		url:url,
		headers:{
			'Accept':'*/*',
			'Accept-Encoding':'gzip, deflate, sdch',
			'Accept-Language':'en-US,en;q=0.8',
			'Connection':'keep-alive',
			'Host':'i.jumei.com',
			'Referer':ref,
			Cookie:config.cookie
		},
		tra:config.tra,
		batch:config.batch,
		timeout:config.timeout
	};
	if(config.useragent){
		task.headers['User-Agent'] = config.useragent;
	}
	
	getdatafromproxy(config,task,function(err,data,headers){	
		checkResponseCookie(config,headers);
		if(err){
			cb(err);
			return;
		}
		
		try {
			
			try {
				var doms = global.$(data);
				var com = null;
				var num = null;
				var cs = doms.find('div.express-info').html().split('<br>');
				for(var i = 0 ; i < cs.length ; i ++){
					c = cs[i].trim();
					if(c.indexOf('快递公司')>=0){
						com = c.split('：')[1];
					}
					if(c.indexOf('快递单号')>=0){
						num = c.split('：')[1];
					}
				}
				if(num){
					map[num] = {
						comname:com,
						comnum:num,
						title:order.title,
						pic:order.pic,
						//logisticStatus:logisticStatus,
						//sendtime:sendtime,
						orderid:order.orderid,
						sendtip : "商家正通知快递公司揽件"
					};
				}
			} catch(e){
				log(e.stack);
				cb(e,map);
				return;
			}
		} catch(e){
			log(e.stack);
		}
		
		cb(null,map);
	});
};

var getOrderDetail = function(config,order,cb){
	var newmap = {};
	var has = false;
	if(lastresult&&lastresult.map){
		var map = lastresult.map;
		for(var x in map){
			if(map[x].orderid == order.orderid){
				newmap[x] = map[x];
				newmap[x].cached = 1;
				has = true;
			}
		}
	}
	if(has){
		cb(null,newmap);
		return;
	}
	getOrderDetailFromNet(config,order,cb);
}

var getnumbers = function(config,cb){
	var sort = 1;
	var time = new Date().getTime();
	log('getOrderList>start>' + config.tra + ">" + config.name);
	getOrderList(config,function(err,array){
		savecookie(config);
		if(err){
			log('getOrderList>end>' + config.tra + ">" + config.name + ">" + err); 
			cb(err);
			return;
		}
		if(!array){
			log('getOrderList>end>' + config.tra + ">" + config.name + ">null");
			cb(null,{});
			return;
		}
		var list = array;
		log('getOrderList>end>' + config.tra + ">" + config.name + ">null");
		if(list.length==0){
			cb(null,{});
			return;
		}
		
		var result = {};
		var i = 0;
		var eback = function(err,mapb){
			savecookie(config);
			if(mapb){
				for(var x in mapb){
					var value = mapb[x];
					value.sort = sort++;
					result[x] = value;
				}
			}
			i++;
			if(i< list.length){
				getOrderDetail(config,list[i],eback);
			} else {
				cb(null,result);
			}
		};
		getOrderDetail(config,list[i],eback);
	});
}


var session;
var savecookie = function(config){
	if(!config.cookiechange && !config.sessionexpire){
		return;
	}	
	delete config.cookiechange;

	session.cookie = config.cookie;
	global.dotask({
		action:'setvalue',
		key:'ds_session_' + NAME,
		value:JSON.stringify(session)
	},function(result){
	});
}

var lastresult;

global.dotask({//get last result
	action:'getvalue',
	key:'ds_lastresult_' + NAME
},function(result){
	var value = result.value;
	if(value){
		lastresult = JSON.parse(value);
	}
	global.dotask({//get cookie
		action:'getvalue',
		key:'ds_session_' + NAME
	},function(result){
		var time = new Date().getTime();
		var value = result.value || '{}';
		if(value){
			session = JSON.parse(value);
		}
		if(session && session.cookie){
			getnumbers({
				cookie:session.cookie,
				batch:1,
				tra:'788c7660-3254-9658-5948-34a8d02c1257',
				useragent:global.navigator.userAgent
			},function(err,map){
				var ret = {};
				ret.status = '200';
				ret.action = 'event';
				ret.type = 'dianshangorder';
				ret.dianshang = NAME;
				if(err){
					ret.status = "302";
					global.dotask(ret,function(){
					});
					return;
				}
				
				
				
				var newMap = {};
				
				var url = "http://p.kuaidi100.com/mobile/mobileapi.do?method=comauto&json=";
				var autocount = 0;
				var json = {map:{}};
				var m = json.map;
				json.tra = session.tra;
				json.dianshang = NAME;
				for(var x in map){
					if(map[x].cached){
						newMap[x] = map[x];
					} else {
						var str = x + "," + map[x].comnum + "," + map[x].comname;
						if(str.indexOf('邮政电子面单')>=0 || str.indexOf('邮政快递包裹')>=0 || str.indexOf('中国邮政')>=0){
							newMap[x] = map[x];
							newMap[x].comnum = 'youzhengguonei';
						} else if(str.indexOf('晟邦')>=0){
							newMap[x] = map[x];
							newMap[x].comnum = 'nanjingshengbang';
						} else if(str.indexOf('中通')>=0){
							newMap[x] = map[x];
							newMap[x].comnum = 'zhongtong';
						} else {
							autocount ++;
							m[x] = str;
						}
					}
				}
				
				if(!autocount){
					ret.map = newMap;
					ret.status = '200';
					global.dotask(ret,function(){
						global.dotask({//save last result
							action:'setvalue',
							key:'ds_lastresult_' + NAME,
							value:JSON.stringify(ret)
						},function(result){});
					});
					return;
				}
				url = url + encodeURIComponent(JSON.stringify(json));
				global.dotask({
					_id:NAME + time + '_' + (seq++),
					action:'http',
					url:url,
				},function(result){
					if(result.content){
						result = JSON.parse(result.content).result;
						for(var x in result){
							var item = map[x];
							item.comnum = result[x];
							newMap[x] = item;
						}
						ret.map = newMap;
						ret.status = '200';
						global.dotask(ret,function(){
							global.dotask({//save last result
								action:'setvalue',
								key:'ds_lastresult_' + NAME,
								value:JSON.stringify(ret)
							},function(result){
							});					
						});
						
					} else {
						ret.status = '500';
						global.dotask(ret,function(){
						});
					}
				});
			});
		} else {
			var ret = {'status':'302'};
			ret.action = 'event';
			ret.type = 'dianshangorder';
			ret.dianshang = NAME;
			global.dotask(ret,function(result){
			});
		}
	});
});
