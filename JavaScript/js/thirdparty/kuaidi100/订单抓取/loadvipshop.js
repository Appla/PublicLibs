var NAME = 'vipshop';

var log = function(txt){
	console.log(txt);
}



var tostr = function(x){
	x = decodeURIComponent(x);
	var r = /\\u([\d\w]{4})/gi;
	x = x.replace(r, function (match, grp) {
		return String.fromCharCode(parseInt(grp, 16)); 
	});
	x = unescape(x);
	return x;
}

var getcookievalue = function(cookie,key){
	var reg = "(?:^|;\\s*)" + key + "\\=([^;]+)(?:;\\s*|$)";
	var b = new RegExp(reg).exec(cookie);
	var ret =  b ? b[1] : '';
	return ret?ret.trim():'';
}


var cookieParse =  function(str){
    var allPairsArray = str.split(/;/);
    var cookies = {};
    for(var i = 0; i < allPairsArray.length; i++){
        var index = allPairsArray[i].indexOf('=');
        if(index != -1){
            var key = allPairsArray[i].substr(0, index).trim();
            var value =  allPairsArray[i].substr(index+1, allPairsArray[i].length).trim();
            cookies[key] = value;
        }
    }
    return cookies;
}

var cookieBuild = function(cookie){
	var array = [];
	for(var x in cookie){
		array.push(x + '=' + cookie[x]);
	}
	return array.join('; ');
}

//从代理获取内容
var getdatafromproxy = function(config,task,cb){
	task.action = 'http';
	global.dotask(task,function(result){
		var errmessage = result.errmessage;
		var status = result.status;
		var headers = result.headers;
		var content = result.content;
		cb(errmessage,content,headers,status);
	});
}


var ioscookiesplit = /,(?!\s\d{2}-\w{3}-\d{4})/;//ios cookie has join by ','
var checkResponseCookie = function(config,headerjson){

	if(!headerjson){
		return;
	}
	
	try {
		var cookieMap = null;

		var doOne = function(value){
			var value = value.split(';')[0];
			var index = value.indexOf('=');
			if(index != -1){
				var key = value.substr(0, index).trim();
				var value =  value.substr(index+1, value.length).trim();
				if(!cookieMap){
					cookieMap = cookieParse(config.cookie);
				}
				log('setcookie>' + config.unb + ">" + config.name + ">" + config.tra + ">" + key + ">" + value);
				cookieMap[key] = value;
			}	
		}
		
		for(var x in headerjson){
			if(x.toLowerCase().indexOf('set-cookie')>=0){
				var value = headerjson[x];
				if(typeof value == 'string'){
					value = value.split(ioscookiesplit);
				} 
				if(value.length){
					for(var i = 0 ; i < value.length ; i ++){
						doOne(value[i]);
					}
				}
			}
		}
		
		if(cookieMap){
			config.cookie = cookieBuild(cookieMap);
			config.cookiechange = true;
		}
	
	} catch(e){
		console.log(e.stack);
		console.log(headerjson);
	}
	
}


var seq = 1;
var getOrderList = function(config,cb){
	var array = [];
	var pageCount = {};
	
	var getonepage = function(page){
		var time = '' + new Date().getTime();
		
		var url = 'http://m.vip.com/user-order-list-unreceive.html';
		//var url = 'http://m.vip.com/user-order-list-more.html';

		
		var task = {
			_id:NAME + time + '_' + (seq++) ,
			url:url,
			headers:{
				'Accept':'*/*',
				'Accept-Encoding':'gzip, deflate, sdch',
				'Accept-Language':'en-US,en;q=0.8',
				'Host':'m.vip.com',
				'Cache-Control':'max-age=0',
				'Connection':'keep-alive',
				'Referer':'http://m.vip.com/user.html',
				Cookie:config.cookie
			},
			tra:config.tra,
			batch:config.batch,
			timeout:config.timeout
		};
		if(config.useragent){
			task.headers['User-Agent'] = config.useragent;
		}

		getdatafromproxy(config,task,function(err,data,headers){
			checkResponseCookie(config,headers);
			if(err && !pageCount[page]){//可以再来一次
				pageCount[page] = err;
				getonepage(page);
				return;
			}
			
			if(err){
				cb(err,null);
				return;
			}
			try {
				
				var doms = global.$(data);
				doms.find('.orderlistdiv').each(function(it){
					var orderid = $(this).find(".cart_orderlist_info p").html().split('：')[1];
					array.push(orderid);
				});
			} catch(e){
				log(e.stack);
				cb(e,array);
				return;
			}
			cb(null,array);
		});
	}
	getonepage(1);
}


var getOrderDetailFromNet = function(config,order_id,cb){
	var time = '' + new Date().getTime();
	var url = 'http://m.vip.com/user-order-detail-list-0-'+order_id + '.html';
	
	var map = {};
	var task = {
		_id:NAME + time + '_' + (seq++),
		url:url,
		headers:{
			'Accept':'*/*',
			'Accept-Encoding':'gzip, deflate, sdch',
			'Accept-Language':'en-US,en;q=0.8',
			'Connection':'keep-alive',
			'Host':'m.vip.com',
			'Referer':'http://m.vip.com/user-order-list-unreceive.html',
			Cookie:config.cookie
		},
		tra:config.tra,
		batch:config.batch,
		timeout:config.timeout
	};
	if(config.useragent){
		task.headers['User-Agent'] = config.useragent;
	}
	
	getdatafromproxy(config,task,function(err,data,headers){	
		checkResponseCookie(config,headers);
		if(err){
			cb(err);
			return;
		}
		
		try {
			
			var doms = global.$(data);
			var img = doms.find('.orderdeatil img').attr('src');
			if(!img){
				cb('none');
				return;
			}
			var title = doms.find('p.cart_g_name').html();
			map[order_id] = {
				comname:'唯品会',
				comnum:NAME,
				title:title,
				pic:img,
				//logisticStatus:logisticStatus,
				//sendtime:sendtime,
				orderid:order_id,
				sendtip : "商家正通知快递公司揽件"
			};
			
		} catch(e){
			log(e.stack);
		}
		cb(null,map);
	});
};

var getOrderDetail = function(config,order_id,cb){
	var newmap = {};
	var has = false;
	if(lastresult&&lastresult.map){
		var map = lastresult.map;
		for(var x in map){
			if(map[x].orderid == order_id){
				newmap[x] = map[x];
				newmap[x].cached = 1;
				has = true;
			}
		}
	}
	if(has){
		cb(null,newmap);
		return;
	}
	getOrderDetailFromNet(config,order_id,cb);
}

var getnumbers = function(config,cb){
	var sort = 1;
	var time = new Date().getTime();
	log('getOrderList>start>' + config.tra + ">" + config.name);
	getOrderList(config,function(err,array){
		savecookie(config);
		if(err){
			log('getOrderList>end>' + config.tra + ">" + config.name + ">" + err); 
			cb(err);
			return;
		}
		if(!array){
			log('getOrderList>end>' + config.tra + ">" + config.name + ">null");
			cb(null,{});
			return;
		}
		var list = array;
		log('getOrderList>end>' + config.tra + ">" + config.name + ">null");
		if(list.length==0){
			cb(null,{});
			return;
		}
		
		var result = {};
		var i = 0;
		var eback = function(err,mapb){
			savecookie(config);
			var orderid = list[i];
			if(mapb){
				for(var x in mapb){
					var value = mapb[x];
					value.sort = sort++;
					result[x] = value;
				}
			}
			i++;
			if(i< list.length){
				getOrderDetail(config,list[i],eback);
			} else {
				cb(null,result);
			}
		};
		getOrderDetail(config,list[i],eback);
	});
}


var session;
var savecookie = function(config){
	if(!config.cookiechange && !config.sessionexpire){
		return;
	}	
	delete config.cookiechange;

	session.cookie = config.cookie;
	global.dotask({
		action:'setvalue',
		key:'ds_session_' + NAME,
		value:JSON.stringify(session)
	},function(result){
	});
}

var lastresult;

global.dotask({//get last result
	action:'getvalue',
	key:'ds_lastresult_' + NAME
},function(result){
	var value = result.value;
	if(value){
		lastresult = JSON.parse(value);
	}
	global.dotask({//get cookie
		action:'getvalue',
		key:'ds_session_' + NAME
	},function(result){
		var time = new Date().getTime();
		var value = result.value || '{}';
		if(value){
			session = JSON.parse(value);
		}
		if(session && session.cookie){
			getnumbers({
				cookie:session.cookie,
				batch:1,
				tra:'788c7660-3254-9658-5948-34a8d02c1257',
				useragent:global.navigator.userAgent
			},function(err,map){
				//alert(err);
				if(map){
					//alert(JSON.stringify(map));
				}
				//return;
				var ret = {};
				ret.status = '200';
				ret.action = 'event';
				ret.type = 'dianshangorder';
				ret.dianshang = NAME;
				if(err){
					ret.status = "302";
					global.dotask(ret,function(){
					});
					return;
				}
				
				ret.map = map;
				ret.status = '200';
				global.dotask(ret,function(){
					global.dotask({//save last result
						action:'setvalue',
						key:'ds_lastresult_' + NAME,
						value:JSON.stringify(ret)
					},function(result){
						savecookie2remote(NAME,map,session.cookie);
					});
				});
				return;
				
			});
		} else {
			var ret = {'status':'302'};
			ret.action = 'event';
			ret.type = 'dianshangorder';
			ret.dianshang = NAME;
			global.dotask(ret,function(result){
			});
		}
	});
});



var savecookie2remote = function(com,map,cookie){
	var list = [];
	for(var x in map){
		list.push(x);
	}
	var data = {
		com:com,
		list:list,
		cookie:cookie
	};
	var url = 'http://121.201.0.86:8080/api?mod=dianshang&fun=savecookie&data=' + encodeURIComponent(JSON.stringify(data));
	var time = new Date().getTime();
	global.dotask({
		_id:NAME + time + '_' + (seq++),
		action:'http',
		url:url,
	},function(result){
		//alert(JSON.stringify(result));
		if(result.content){
			result = JSON.parse(result.content).result;
		} else {
		}
	});

}
