var val = 10086,
    arr = [10086],
    obj = { val: 10086 };
funcVal = () => ++val;
funcArr = () => ++arr[0];
funcObj = () => ++obj.val;

var val = 100,
    arr = [100],
    obj = { val: 100 };

//调用时查找 scope.chain ！！！和lua还是有点差别，lua重新local的话，closure用的还是之前的值，可能和值作用域有关？？？新声明的local有另外的scope吧
console.log(val, funcVal(), val, funcVal());
console.log(arr[0], funcArr(), arr[0], funcArr());
console.log(obj.val, funcObj(), obj.val, funcObj());