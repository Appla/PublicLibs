<?php
echo PHP_VERSION, PHP_EOL;
// START@#2017-07-#

//exit();
// END@#2017-07-#


// START@#2017-07-28#

$str = '123456';
foreach (hash_algos() as $algo) {
    echo $algo.':', hash($algo, $str), PHP_EOL;
}

exit();
// $str = '12345afgasf66';

// $key = '121';

// $iv = '12125453334';

// $cipher = 'BF-CBC';


// echo $en = openssl_encrypt($str, $cipher, $key), PHP_EOL;

// var_dump($en);

// echo openssl_decrypt($en, $cipher, $key);

// exit;
// $ciphers = [
//     1 => 'AES-128-CBC',
//     'AES-128-CFB',
//     'AES-128-CFB1',
//     'AES-128-CFB8',
//     'AES-128-ECB',
//     'AES-128-OFB',
//     'AES-192-CBC',
//     'AES-192-CFB',
//     'AES-192-CFB1',
//     'AES-192-CFB8',
//     'AES-192-ECB',
//     'AES-192-OFB',
//     'AES-256-CBC',
//     'AES-256-CFB',
//     'AES-256-CFB1',
//     'AES-256-CFB8',
//     'AES-256-ECB',
//     'AES-256-OFB',
// ];

// $ciphers = openssl_get_cipher_methods(true);



// foreach ($ciphers as $key => $cipher) {
//     echo $key, ': cipher:' . $cipher , PHP_EOL;
//     echo $en = aes::encrypt($str, $key, $iv, $cipher), PHP_EOL;
//     echo aes::decrypt($en, $key, $iv, $cipher), PHP_EOL;
//     echo '_____________________SEPERATOR_____________________',PHP_EOL;
// }




// exit();
// END@#2017-07-28#

// START@#2017-07-27#

class aes
{
    /**
     * 禁用设置为null
     * @var array
     */
    private static $supportedCiphers = [
        'AES-128-CBC' => 1,
        'AES-128-CFB' => 1,
        'AES-128-CFB1' => 1,
        'AES-128-CFB8' => 1,
        'AES-128-ECB' => 1,
        'AES-128-OFB' => 1,
        'AES-192-CBC' => 1,
        'AES-192-CFB' => 1,
        'AES-192-CFB1' => 1,
        'AES-192-CFB8' => 1,
        'AES-192-ECB' => 1,
        'AES-192-OFB' => 1,
        'AES-256-CBC' => 1,
        'AES-256-CFB' => 1,
        'AES-256-CFB1' => 1,
        'AES-256-CFB8' => 1,
        'AES-256-ECB' => 1,
        'AES-256-OFB' => 1,
    ];

    const DEFAULT_CIPHER_METHOD = 'AES-128-CBC';
    const DEFAULT_PADDING_LENGTH = 16;

    /**
     * 支持的选项
     */
    const OPTION_WITH_PKCS7_PADDING = 0;
    const OPTION_NO_PADDING = OPENSSL_ZERO_PADDING;
    const OPTION_RAW_DATA = OPENSSL_RAW_DATA;

    /**
     * @var array
     * 0, 1, 2
     */
    private static $supportedOptions = [
        self::OPTION_WITH_PKCS7_PADDING,
        OPENSSL_RAW_DATA,
        OPENSSL_ZERO_PADDING,
    ];
    /**
     * @var int
     */
    private static $option = self::OPTION_WITH_PKCS7_PADDING;

    /**
     * @param int $option
     * @return int
     */
    public static function setOption($option)
    {
        if(in_array($option, self::$supportedOptions, true)){
            return self::$option = $option;
        }
        return 0;
    }

    /**
     * 加密
     * @param string $str
     * @param string $key
     * @param string $iv
     * @param string $cipherMethod
     * @return string
     */
    public static function encrypt($str, $key, $iv = '0000000000000000', $cipherMethod = self::DEFAULT_CIPHER_METHOD)
    {
        return self::isSupportedCipherMethod($cipherMethod) ? openssl_encrypt($str, $cipherMethod, self::pad16($key), self::$option, self::padStr($iv, openssl_cipher_iv_length($cipherMethod))) : '';
    }

    /**
     * 解密
     * @param string $str
     * @param string $key
     * @param string $iv
     * @param string $cipherMethod
     * @return string
     */
    public static function decrypt($str, $key, $iv = '0000000000000000', $cipherMethod = self::DEFAULT_CIPHER_METHOD)
    {
        return self::isSupportedCipherMethod($cipherMethod) ? openssl_decrypt($str, $cipherMethod, self::pad16($key), self::$option, self::padStr($iv, openssl_cipher_iv_length($cipherMethod))) : '';
    }

    /**
     * 处理长度不够或者长度超出填充问题
     * @param string $val
     * @return string
     */
    public static function pad16($val)
    {
        return self::padStr($val, 16, '0');
    }

    /**
     * @param string $str
     * @param int $length
     * @return bool|string
     */
    private static function padStr($str, $length, $padding = '0')
    {
        return strlen($str) < $length ? str_pad($str, $length, $padding) : substr($str, 0, $length);
    }

    /**
     * @param $method
     */
    private static function isSupportedCipherMethod($method)
    {
        return $method && is_string($method) && isset(self::$supportedCiphers[$method]);
    }
}


// $str = '/8QgmZCq04jSvWKiJLyP5fSIapbvKlR1hqahnO76buObyAutX8HdT/R2ms+UGq54aHlac/KEFuCXrGa4QcwmGT92cg1KeXPdEuRYpufVcLg+d6eDpeU0gF+1QQ/0u8NcYfgMFxgWYdbJrhBQyOuyHN9LHHyDpUFnehqgAoew0OamFg2cBhk3LbH0PzO4sz/0';
// $str = '/8QgmZCq04jSvWKiJLyP5fSIapbvKlR1hqahnO76buObyAutX8HdT/R2ms+UGq54aHlac/KEFuCXrGa4QcwmGT92cg1KeXPdEuRYpufVcLg+d6eDpeU0gF+1QQ/0u8NcYfgMFxgWYdbJrhBQyOuyHN9LHHyDpUFnehqgAoew0Oa67dVkc2mnDCOOk1gsDG6K';
$a = '{"no":"52240119911209763X","sex":"男","address":"贵州省毕节市小坝镇汉屯村七组64号","name":"肖雄","nation":"汉"}';



function pad16($val)
{
    $val = strlen($val) < 16 ? str_pad($val, 16, '0') : substr($val, 0, 16);
    return $val;
}


$key = (substr(md5('kuaidihelp'),0,16));
$iv = pad16(substr(md5('kuaidihelp'),16,16));

$str = '+IzAFCWRe4p3NeqkZ9YOEQ==';


// $str = aes::encrypt($a, $key, $iv);

echo aes::decrypt($str, $key, $iv), PHP_EOL;

$cipher = 'AES-128-CBC';
$a = '207270107000005';

// echo $str = aes::encrypt($a, $key, $iv, $cipher);


// $len = strlen($a);
// if($len % 2 !== 0) {
//     echo '-----', PHP_EOL;
//     $a .= '0';
// }
$key = '54b0d399201415c5';
$iv = 'f5adf175b916964a';
$str = '123456';

$str = 'L3BqsOcj5YMzkJhjBn7enQ==';

aes::setOption(aes::OPTION_NO_PADDING);
echo aes::decrypt($str, $key, $iv), PHP_EOL;
// echo openssl_decrypt($str, $cipher, $key, OPENSSL_ZERO_PADDING, $iv);
exit;

// echo base64_encode($str);
// exit;
echo $en = openssl_encrypt($str, $cipher, $key, 0, $iv), PHP_EOL;
exit;
const ENCRYPT_METHOD = 'AES-128-CBC';
echo $str = openssl_encrypt($a, ENCRYPT_METHOD, $key, 0, $iv);
// var_dump($str);
// echo OPENSSL_CIPHER_AES_256_CBC;
// echo openssl_cipher_iv_length('AES-256-CBC');
// exit;
echo openssl_decrypt($str, ENCRYPT_METHOD, $key, 0, $iv);
echo rtrim(aes::decryptStr($str), "\x00..\x1f");
exit;

// echo $b = aes::encryptStr($a);

file_put_contents('test.file', rtrim(aes::decryptStr($str), "\x00..\x1f"));
// $str = base64_decode($str);

// echo aes::decryptStr($str);


exit;
$redisConfig = [
    '192.168.1.83',
    7000
];
// $r = new RedisCluster(null, ['192.168.1.83:7000', '192.168.1.160:7003', '192.168.1.160:7004']);
$r = new RedisCluster(null, ['192.168.1.83:7000']);
// $r->connect(...$redisConfig);

$keys= range(1, 10);
$result = [];

// $r->set(10, 5);
echo $r->get(10);
exit;

array_walk($keys, function($key) use($r) {
    echo $r->get($key), PHP_EOL;
});
exit;

// array_walk($keys, function($val, $key) use($r, &$result) {
//     echo $key, ':' , $val, PHP_EOL;
//     $result[] = $r->set($key, $val);
// });


print_r($result);
exit;

var_dump('2017-05-03' > '2017-04-03');

exit();
// END@#2017-07-27#

// START@#2017-07-25#

// $dateStr = '2017-01-25';

$dateStr = null;

echo $date = (new DateTime($dateStr ?: '-6 months'))->format('Y-m-d');
echo (new DateTime($date))->getTimeStamp(), PHP_EOL;
echo ((new DateTime('-6months')))->getTimeStamp(), PHP_EOL;

assert((new DateTime($date)) > (new DateTime('-6months')), 'correct');


exit;
echo sha1(random_bytes(25));

exit();
// END@#2017-07-25#

// START@#2017-07-24#

$keys = [
    'CurlRequestLogs:url:222.66.109.133:22225/stokb/batchBillNoQuery3.action
:201707212056',
    'CurlRequestLogs:url:222.66.109.133:22225/stokb/batchBillNoQuery3.action?billCodes=发起网点：江西信丰公司
接:201707230827',
    'CurlRequestLogs:url:222.66.109.133:22225/stokb/batchBillNoQuery3.action?billCodes=发起网点：江西信丰公司
接:201707230827',
];

foreach ($keys as $key) {
    // echo trim(preg_replace('~^https?://|\?.*$~i', '', $url), '/');
}


const CACHE_KEY_PREFIX = 'CurlRequestLogs:url:';
const KEY_SEPARATOR = ':';

function parseKey($key)
{
    static $prefixLen;
    if ($prefixLen === null) {
        $prefixLen = strlen(CACHE_KEY_PREFIX);
    }
    $key = substr($key, $prefixLen);
    if (preg_match('~^(.*)'. KEY_SEPARATOR .'(\d{12})$~', $key, $match)) {
       return array_slice($match, 1, 2);
    }
    if(substr_count($key, KEY_SEPARATOR) > 1) {
        $tmp = explode(KEY_SEPARATOR, $key);
        $time = array_splice($tmp, -1, 1);
        $ret = [implode(KEY_SEPARATOR, $tmp), $time];
    } else {
        $ret = explode(KEY_SEPARATOR, $key, 2);
    }
    return $ret;
}

// $url = 'http://CurlRequestLogs:url:222.66.109.133:22225/stokb/batchBillNoQuery3.action?billCodes=发起网点：江西信丰公司
// 接:201707230827';
// echo trim(preg_replace('~^https?://|\?.*$~si', '', $url), '/');

exit();
// END@#2017-07-24#

// START@#2017-07-21#

echo preg_replace('~[[:print:]]~', '', ' 甘肃省//-定西市-定西市/\\ _.');

exit();
// END@#2017-07-21#

// START@#2017-07-19#


$str = 'O:25:\"service\\address\\AsyncTask\":9:{s:33:\"\x00service\\address\\AsyncTask\x00taskId\";s:49:\"missScan_37bda4dfdd650107828e420c100a20de7fd2322a\";s:34:\"\x00service\\address\\AsyncTask\x00paramId\";s:40:\"37bda4dfdd650107828e420c100a20de7fd2322a\";s:41:\"\x00service\\address\\AsyncTask\x00taskProperties\";a:4:{s:10:\"branchCode\";s:6:\"730000\";s:10:\"branchName\";s:18:\"\xe7\x94\x98\xe8\x82\x83\xe5\x85\xb0\xe5\xb7\x9e\xe5\x85\xac\xe5\x8f\xb8\";s:11:\"courierCode\";s:10:\"7300007878\";s:4:\"date\";s:10:\"2017-07-19\";}s:35:\"\x00service\\address\\AsyncTask\x00taskType\";s:8:\"missScan\";s:35:\"\x00service\\address\\AsyncTask\x00execTime\";i:1500429902;s:33:\"\x00service\\address\\AsyncTask\x00status\";s:8:\"finished\";s:35:\"\x00service\\address\\AsyncTask\x00runCount\";i:1;s:41:\"\x00service\\address\\AsyncTask\x00lastExecStatus\";s:7:\"success\";s:40:\"\x00service\\address\\AsyncTask\x00lastExecError\";i:11009;}';

$a = unserialize($str);

var_dump($a);

exit();
// END@#2017-07-19#
