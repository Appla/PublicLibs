<?php
echo PHP_VERSION, PHP_EOL;

class a{
    const A = 1 << 1;
}

// list($a, $b, $c) = range(1, 6);

// var_dump($a, $b, $c);

exit;
$total = 500000;
$pattern = '/市/u';

$strArr = [
    '北京市阿萨法守法沙发沙发撒发生发',
    '石家庄市阿萨沙发沙发沙发',
    '北京市',
    '上海市',
    '湖南省阿萨法守法是否尘世啊是放假啊考司法考试发城市',
    '新疆省噶厦暑假工啊暑假工啊是那个卢卡斯几个拉升股价拉升给你市',
    '新疆省噶厦暑假工啊暑假工啊是那个卢卡斯几个拉升股价拉升给你市阿萨法守法沙发沙发',
];
timer('noAnchor');
foreach($strArr as $str){
    echo preg_replace($pattern, '', $str), PHP_EOL;
}
while($total-- > 0){
    foreach($strArr as $str){
        preg_replace($pattern, '', $str);
    }
}
timer('noAnchor');

$total = 500000;
timer('withAnchor');

$pattern = '/市$/u';
foreach($strArr as $str){
    echo preg_replace($pattern, '', $str), PHP_EOL;
}
while($total-- > 0){
    foreach($strArr as $str){
        preg_replace($pattern, '', $str);
    }
}
timer('withAnchor');
timer(true);

exit;

function timer($key = 'default')
{
    static $logs = [];
    if(true === $key){
        print_r($logs);
        exit;
    }
    if(!isset($logs[$key.'_start'])){
        $logs[$key.'_start'] = microtime(true);
    }else{
        $logs[$key.'_cost'] = ($logs[$key.'_end'] = microtime(true)) - $logs[$key.'_start'];
    }
    return $logs;
}


abstract class a
{
    /**
     * @return static
     */
    public static function getInstance()
    {
        print_r([__CLASS__, static::class, get_called_class()]);
    }
}

class c extends a
{
    const TABLE_NAME = 'sto_yi_duan_ma';
}

class b extends a
{
    const TABLE_NAME = 'sto_yi_duan_ma';
}

b::getInstance();
c::getInstance();
