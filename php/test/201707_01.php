<?php
echo PHP_VERSION, PHP_EOL;
// START@#2017-07-#

// exit();
// END@#2017-07-#


// START@#2017-07-13#

$arr = [
    '"aa"',
    'aa',
    '12',
    1,
    -2,
    'null',
    'false',
    'true',
    '{"aa":"bb"}',
    '[1, 3, 5, "asf"]'
];

var_dump(array_map(function($val){
    return json_decode($val, true) ?: $val .':'. json_last_error_msg();
}, $arr));

exit;

exit;
const CACHE_KEY_PREFIX = 'CurlRequestLogs:url:';
const KEY_SEPARATOR = ':';

$keys = [
    'CurlRequestLogs:url:restapi.amap.com/v3/geocode/geo:201707141533',
    'CurlRequestLogs:url:restapi.amap.com:80/v3/geocode/geo:201707141533',
    'CurlRequestLogs:url:restapi.amap.com:80/v3/geocode/geo:201707141543',
    'CurlRequestLogs:url:restapi.amap.com:80/v3/geocode/geo:201707141513',
    'CurlRequestLogs:url:restapi:80.amap.com:8088/v3/geocode/geo:201707141553',

];

$times = 500000;
$timeStart = microtime(true);
while ($times-- > 0) {
    foreach ($keys as $key) {
        $ret = parseKey($key);
    }
}
$timeEnd = microtime(true);

printf('time cost: %s' . PHP_EOL, $timeEnd - $timeStart);

var_dump($ret);

// 2.8587889671326
// 2.8458709716797
/**
 * @param string $key
 * @return array
 * 适当时候用正则
 */
function parseKey($key)
{
    static $prefixLen;
    if ($prefixLen === null) {
        $prefixLen = strlen(CACHE_KEY_PREFIX);
    }
    if (preg_match('~^(.*)'. KEY_SEPARATOR .'(\d{12})$~', substr($key, $prefixLen), $match)) {
       return array_slice($match, 1, 2);
    }
    // $key = substr($key, $prefixLen);
    // if(substr_count($key, KEY_SEPARATOR) > 1) {
    //     $tmp = explode(KEY_SEPARATOR, $key);
    //     $time = array_splice($tmp, -1, 1);
    //     $ret = [implode(KEY_SEPARATOR, $tmp), $time];
    // } else {
    //     $ret = explode(KEY_SEPARATOR, $key, 2);
    // }
    // return $ret;
}


exit;
echo array_sum(['array' => 1, 'as' => 2, 'asf' => 55]);
exit;
$dt = new Datetime('201707131150');
echo $dt->format('Y-m-d_H:i:s');

exit;
$r = new Redis();
$redis_config = [
    '10.20.1.161',
    6380,
    3,
];

$r->connect(...$redis_config);

$r->pipeline();

$ret = $r->exec();

var_dump($ret);

exit;
$queue = new SplQueue();

function a(iterable $queue)
{
    return $queue;
}

a($queue);


exit();
// END@#2017-07-13#




// START@#2017-07-12#

     $array = array("23.32","22","12.009","23.43.43");
      print_r(preg_grep("/^(\d+)/",$array));
      exit;

$arr = [
    '"aa"',
    1,
    -2,
    'null',
    'false',
    'true',
    '{"aa":"bb"}',
    '[1, 3, 5, "asf"]'
];

var_dump(array_map(function($val){
    return json_decode($val, true) ?: json_last_error_msg();
}, $arr));

exit;
// END@#2017-07-12#


// START@#2017-07-11#
$test = 'function test(){
    return function(){
        return \'hello_world\';
    };
}';

$func = test();
echo $func();

exit();
// END@#2017-07-11#

// START@#2017-07-05#

timeLog('start');

$r = new Redis();
$redis_config = [
    '10.20.1.161',
    6380,
    3,
];

$r->connect(...$redis_config);

$cusor = null;
$max = 300;

$matched = $tmp = [];

$pattern = 'address:codeQuery:tbl_addresses_*';

while ($max-- > 0 && $cusor !== 0){
    $tmp[] = $r->scan($cusor, $pattern, 10000);
}
$matched = array_merge(...$tmp);

timeLog('getAllKeys');

$map = [];
foreach ($matched as $key) {
    if(($new_key = preg_replace('/(tbl_addresses_\d+)([^:\d]+)/', '\2:\1', $key)) !== $key) {
        $map[$key] = preg_replace('/:2017_\d{2}_\d{2}$/', '', $new_key);
    }
}

// var_dump(array_slice($map, 0, 30), $total);
// timeLog('buildKeys');
// exit;
$total = count($map);
array_splice($map, 1000);
$current = $processed = count($map);
// $r->pipeline();
foreach ($map as $old_key => $new_key) {
    if($r->renamenx($old_key, $new_key) === false){
        $old_val = $r->get($old_key);
        $r->incrby($new_key, (int)$old_val);
        $r->del($old_key);
    }
    printf('left: %d' . PHP_EOL, $current--);
}
// $ret = $r->exec();
timeLog('cleanData');
$time_log = timeLog();
var_dump($time_log);
$start = $last = $time_log['start'];
array_walk($time_log, function($val, $key) use($start, &$last) {
    printf('%s:%s   last:%s'.PHP_EOL, $key, $val - $start, $val - $last);
    $last = $val;
});
printf('left: %s, total: %s, processed : %s' . PHP_EOL, $total - $processed, $total, $processed);

function timeLog(?string $key = null)
{
    static $timeLog = null;
    if($key === null){
        return $timeLog;
    }
    $timeLog[$key] = microtime(true);
}

exit();
// END@#2017-07-05#

// START@#2017-07-04#

const NEW_KEY = 'enable_submit_btn';
const SOURCE_KEY = 'source';

$tasks = [
    'taobao' => [
        SOURCE_KEY => '{"un":["#username"],"up":["#password"]}',
        NEW_KEY => 'document.getElementById("btn-submit").setAttribute("disabled", false)',
    ],
    'jumei' => [
        SOURCE_KEY => '{"un":["#account","#dynamic_mobile"],"up":["input[name=\"password\"].register_input","#dynamic_password"]}',
        NEW_KEY => 'document.getElementById("ga_login").setAttribute("class","register_button hover")',
    ],
];


array_walk($tasks, function($task, $name) {
    echo $name, PHP_EOL;
    echo editJsonObj($task[SOURCE_KEY], NEW_KEY, $task[NEW_KEY]), PHP_EOL;
});
exit();
echo json_encode($jsonObj);






function editJsonObj(string $sourceStr, string $key, string $value) {
    $jsonObj = json_decode($sourceStr);
    $jsonObj->{$key} = $value;
    return json_encode($jsonObj);
}

exit();
// END@#2017-07-04#
