<?php
echo PHP_VERSION, PHP_EOL;


exit;

//值传递
$val = 10086;
$local = [10086];
//引用传值
$obj = (new class{
    public $val = 10086;
});

$funcVal = function() use($val){
     return $val+=1;
};

$funcArr = function() use($local){
    return $local[0]+=1;
};

$funcObj = function() use($obj){
    return $obj->val+=1;
};

//不会改变所传的值
$val = 100;
$local = [100];
//会改变闭包的结果
$obj->val = 100;

echo 'int : passing by value', PHP_EOL;
echo implode(',', [$val, $funcVal(), $val, $funcVal(), PHP_EOL]);
//10086,10087,10086,10087

echo 'Array : passing by value', PHP_EOL;
echo implode(',', [$local[0], $funcArr(), $local[0], $funcArr(), PHP_EOL]);
//10086,10087,10086,10087

echo 'Object : passing by reference', PHP_EOL;
echo implode(',', [$obj->val, $funcObj(), $obj->val, $funcObj(), PHP_EOL]);
//10086,10087,10086,10087

exit();

$json = <<<JSON
{"2017-04-01":{"totalRecordCount":"10515333","distinctCount":"3770836","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-01\" AND id BETWEEN 374700000 AND 394700000","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 374700000 AND 385215333","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 374700000 AND 385215333","idStart":374700000,"id":385215333},"2017-04-02":{"totalRecordCount":"9715318","distinctCount":"3488142","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-02\" AND id BETWEEN 385215333 AND 405215333","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 385215333 AND 394930651","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 385215333 AND 394930651","idStart":385215333,"id":394930651},"2017-04-03":{"totalRecordCount":"9170781","distinctCount":"3290435","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-03\" AND id BETWEEN 394930651 AND 414930651","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 394930651 AND 404101432","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 394930651 AND 404101432","idStart":394930651,"id":404101432},"2017-04-04":{"totalRecordCount":"10061438","distinctCount":"3310373","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-04\" AND id BETWEEN 404101432 AND 424101432","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 404101432 AND 414162870","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 404101432 AND 414162870","idStart":404101432,"id":414162870},"2017-04-05":{"totalRecordCount":"10577839","distinctCount":"3679422","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-05\" AND id BETWEEN 414162870 AND 434162870","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 414162870 AND 424740709","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 414162870 AND 424740709","idStart":414162870,"id":424740709},"2017-04-06":{"totalRecordCount":"6889184","distinctCount":"2676770","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-06\" AND id BETWEEN 424740709 AND 444740709","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 424740709 AND 431629893","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 424740709 AND 431629893","idStart":424740709,"id":431629893},"2017-04-07":{"totalRecordCount":"11293701","distinctCount":"3975605","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-07\" AND id BETWEEN 431629893 AND 451629893","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 431629893 AND 442923594","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 431629893 AND 442923594","idStart":431629893,"id":442923594},"2017-04-08":{"totalRecordCount":"12720687","distinctCount":"4142179","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-08\" AND id BETWEEN 442923594 AND 462923594","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 442923594 AND 455644281","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 442923594 AND 455644281","idStart":442923594,"id":455644281},"2017-04-09":{"totalRecordCount":"12874478","distinctCount":"4364407","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-09\" AND id BETWEEN 455644281 AND 475644281","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 455644281 AND 468518759","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 455644281 AND 468518759","idStart":455644281,"id":468518759},"2017-04-10":{"totalRecordCount":"14336959","distinctCount":"4739707","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-10\" AND id BETWEEN 468518759 AND 488518759","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 468518759 AND 482855718","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 468518759 AND 482855718","idStart":468518759,"id":482855718},"2017-04-11":{"totalRecordCount":"12677165","distinctCount":"4410146","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-11\" AND id BETWEEN 482855718 AND 502855718","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 482855718 AND 495532883","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 482855718 AND 495532883","idStart":482855718,"id":495532883},"2017-04-12":{"totalRecordCount":"12830771","distinctCount":"4448774","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-12\" AND id BETWEEN 495532883 AND 515532883","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 495532883 AND 508363654","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 495532883 AND 508363654","idStart":495532883,"id":508363654},"2017-04-13":{"totalRecordCount":"12956748","distinctCount":"4504925","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-13\" AND id BETWEEN 508363654 AND 528363654","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 508363654 AND 521320402","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 508363654 AND 521320402","idStart":508363654,"id":521320402},"2017-04-14":{"totalRecordCount":"13140817","distinctCount":"4474472","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-14\" AND id BETWEEN 521320402 AND 541320402","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 521320402 AND 534461219","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 521320402 AND 534461219","idStart":521320402,"id":534461219},"2017-04-15":{"totalRecordCount":"2918762","distinctCount":"1436087","preQuerySql":"SELECT COUNT(*) AS total FROM parsed_address WHERE LEFT(create_at, 10) = \"2017-04-15\" AND id BETWEEN 534461219 AND 554461219","distinctSql":"SELECT COUNT(DISTINCT(waybill)) AS total FROM parsed_address WHERE id BETWEEN 534461219 AND 537379981","standardCountSql":"SELECT COUNT(*) AS total FROM parsed_address WHERE id BETWEEN 534461219 AND 537379981","idStart":534461219,"id":537379981}}
JSON;

foreach (json_decode($json, true) as $date => $info) {
    printf('date:[%s], amount:[%d]' . PHP_EOL, $date, $info['totalRecordCount']);
};
