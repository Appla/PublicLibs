<?php


class A {
    public static function who() {
        echo __CLASS__,PHP_EOL;
    }
    public static function test() {
        self::who();
        static::who();
    }
}

class BC extends A {
    public static $X = 'abc';
    public static function who() {
        echo __CLASS__,PHP_EOL;
    }
    public static function t() {
        forward_static_call(['A', 'who']);
        forward_static_call(['parent', 'who']);
        forward_static_call(['self', 'who']);
        forward_static_call(['static', 'who']);
    }
}

// BC::test();
BC::t();

exit;

$str = '{"code":200,"header":"HTTP\/1.1 200 OK\r\nVia: 1.1 tinyproxy (tinyproxy\/1.9.0)\r\nX-Powered-By: PHP\/5.3.3\r\nDate: Sat, 17 Sep 2016 01:46:57 GMT\r\nSet-Cookie: lang=zh-cn; expires=Mon, 17-Oct-2016 01:46:57 GMT; path=\/\r\nSet-Cookie: theme=default; expires=Mon, 17-Oct-2016 01:46:57 GMT; path=\/\r\nSet-Cookie: sid=iku5su557c89pll2st7mlrtsu4; path=\/\r\nCache-control: private\r\nExpires: Thu, 19 Nov 1981 08:52:00 GMT\r\nContent-Type: text\/html; Language=UTF-8\r\nPragma: no-cache\r\nServer: nginx\r\n\r\n","body":"{\"success\":true,\"ico\":\"\\\/data\\\/upload\\\/201407\\\/sf_logo.gif\",\"phone\":\"95338\",\"url\":\"http:\\\/\\\/www.sf-express.com\",\"status\":8,\"companytype\":\"shunfeng\",\"nu\":\"855555542688556668875574123874\",\"company\":\"\\u987a\\u4e30\\u901f\\u8fd0\",\"reason\":\"\",\"data\":[{\"time\":\"2016-08-03 09:50:56\",\"context\":\"\\u5feb\\u4ef6\\u5230\\u8fbe\\u3010\\u5357\\u4eac\\u5e02\\u3011, \\u738b\\u5e08\\u5085\\u6b63\\u5728\\u7ed9\\u4f60\\u6d3e\\u9001\\uff0c\\u7535\\u8bdd(13813073035)\"}],\"time\":\"\",\"rank\":\"4.0\",\"exceed\":\"\",\"timeused\":\"--\"}","error":"","error_no":0,"curl_info":{"url":"http:\/\/www.kuaidi.com\/index-ajaxselectcourierinfo-855555542688556668875574123874-youzhengguonei.html","content_type":"text\/html; Language=UTF-8","http_code":200,"header_size":454,"request_size":454,"filetime":-1,"ssl_verify_result":0,"redirect_count":0,"total_time":0.192435,"namelookup_time":0.00107,"connect_time":0.001324,"pretransfer_time":0.001325,"size_upload":0,"size_download":480,"speed_download":2494,"speed_upload":0,"download_content_length":-1,"upload_content_length":0,"starttransfer_time":0.19241,"redirect_time":0,"redirect_url":"","primary_ip":"10.51.43.253","certinfo":[]},"proxy":"10.51.43.253:4433"}';

var_dump(json_decode($str, true));
exit;

$return = [
    'aa' => [
        29 => [1],
        91 => [1],
        9 => [1],
        19 => [1],
    ],
    'bb' => [
        119 => [1],
        89 => [1],
        39 => [1],
        199 => [1],
    ],
    'cc' => [
        99 => [1],
        919 => [1],
        59 => [1],
    ],

];

foreach ($return as $key => &$val) {
    ksort($val, SORT_NUMERIC);
    $return[$key] = array_slice($val, 0);
}
unset($val);

var_dump(array_merge($return));

exit;


function makeRegExpPatterns($data)
    {
        if(is_array($data)){
            $magic_key = '&1#~&#~1&';
            $str = implode($magic_key, $data);
            $str = str_replace($magic_key, '|', preg_quote($str));
        }elseif (is_string($data)) {
            $str = preg_quote($data);
        }else{
            return '';
        }
        return '/'.$str.'/';
    }

$a = array('正在派件','派件员','派件中','派送中','正在投递');

$str = makeRegExpPatterns($a);


echo $str;
$a ='正在派件中';
echo preg_match($str, $a);


// $redis = new Redis();
// $redis_config = ['127.0.0.1', '6379', 5];
// $redis->connect(...$redis_config);
// // $a = $redis->get('test');
// $a = $redis->keys('this_is_vpn*');
// var_dump($a);






exit;


// $a = [12333444];
// echo implode(',', $a);

function a(){
    echo 'a';
}

1 ? a() : 2;
exit;

$ttl = 5;
$status = '';

echo (int)$ttl ? : (($status !== 'signed')? 86400 : 3600);
exit;

// $pattern = "#__VIEWSTATE[\"\'][^>]+value=[\"\'](.*)[\"\']|__VIEWSTATEGENERATOR[\"\'][^>]+value=[\"\'](.*)[\"\']#";


// $str = <<<EOD
// <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJLTE5NjIwOTI1DxYKHgdOZXRDb2RlBQRCSkZYHgpWaWV3U2lnbkltBQFOHglDVFJOT19SRUZlHglzdHJfd2lkdGgFAzQwMB4Kc3RyX2hlaWdodAUDMzMwFgICAw9kFgQCAw8PFgIeB1Zpc2libGVoZGQCBQ8PFgIfBWhkZGQ/sprAwvhtR786uuIZj4YwK2OW8g==" />
// </div>

// <div>

//     <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0058AAAE" />

// EOD;

// $tokens = [
//     '__VIEWSTATE' => '',
//     '__VIEWSTATEGENERATOR' => '',
// ];
// $pattern_tpl = "#%s[\"\'][^>]+value=[\"\'](.*)[\"\']#i";
// foreach($tokens as $key => $val){
//     if(preg_match(sprintf($pattern_tpl, $key), $str, $match)){
//         $tokens[$key] = $match[1];
//     }else{
//         return false;
//     }
// }

// var_dump($tokens);

// exit;

// $r = ['test', 'aa'];

// $a = ['test' => 1, 'aa' => 2];

// var_dump((bool)array_diff_key(array_flip($r), $a));

// exit;

interface ai
{
    public function __construct(array $a);
}

abstract class a implements ai
{
    public function __construct(array $a){
        //throw new Exception('aaa', 2);
    }
    abstract public function ab();
}

class b extends a
{
    public function __construct(array $a)
    {
        echo 'hello';
    }

    public function ab(){

    }
}

try{
    $b = new b(111);
}catch(Exception $e){
    var_dump($e);
}

var_dump($b instanceof a);

exit;

$a = 1;
$a = $a ^ 1;

var_dump($a);

function strToArray($str = '', $delimiter = ["\n",',','\s+'])
    {
        if ($str && $delimiter && is_string($str)) {
            if (is_string($delimiter)) {
                return explode($delimiter, $str);
            }elseif(is_array($delimiter) && count($delimiter) > 0){
                $delimiter = array_filter($delimiter);
                $patterns = '/'.implode('|', $delimiter).'/i';
                return preg_split($patterns, $str);
            }

        }
        return [];
    }

$str = '1
2
3
4
5  6  7  8       9';
var_dump(strToArray($str));

exit;
 function proxy($url, $proxy = null, $time_out = 3)
    {
        static $_return = [];
        //$url = 'http://www.kuaidi100.com/query?type=yuantong&postid=700235024899';
        //$proxyauth = 'user:password';
        if(empty($url)){
            die('url is empty:');
        }elseif($url === true){
            return $_return;
        }
        $header = [
            // "DNT: 1" ,
            // "Accept-Encoding: deflate, sdch" ,
            "Accept-Language: zh-CN,zh;q=0.8",
            "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2816.0 Safari/537.36",
            "Accept: */*",
            "Cache-Control: no-cache",
            // "X-Requested-With: XMLHttpRequest",
            // "Proxy-Connection: keep-alive" ,
            // "Referer: http://www.kuaidi100.com/",
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $proxy AND curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->proxyauth);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, (int)$time_out?:3);
        curl_setopt($ch, CURLOPT_COOKIEJAR, './cookie_t.txt');
        curl_setopt($ch, CURLOPT_COOKIE, './cookie_t.txt');

        // curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2816.0 Safari/537.36');
        // curl_setopt($ch, CURLOPT_HEADER, 1);
        if(!($_returns[] = $curl_scraped_page = curl_exec($ch))){
            return [$proxy?:time() =>[ 'curl_info' =>curl_getinfo($ch), 'error_no' => curl_errno($ch), 'error' => curl_error($ch)]];
            echo '<pre>'.PHP_EOL;
            var_dump(curl_getinfo($ch), curl_errno($ch), curl_error($ch));
            echo '</pre>'.PHP_EOL;
        }
        $curl_scraped_page = ['return' => $curl_scraped_page,'curl_info' =>curl_getinfo($ch)];
        curl_close($ch);
        return $curl_scraped_page;
    }


function send($url,$proxy='', $headers=array(),$post_data='',$crt_path='',$return_with_header=1,$time=3){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, (bool)$return_with_header);
        curl_setopt($ch, CURLOPT_TIMEOUT, $time);
//      curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
//      curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt ($ch, CURLOPT_PROXY, $proxy);
        if(!empty($headers)){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if(!empty($post_data)){
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        if($crt_path){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_CAINFO, $crt_path);
        }else{
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }
        $response = curl_exec($ch);
        $errmsg = curl_error($ch);
        /*var_dump($response);
        if(''!=$errmsg){
            return 'curl error: '.$errmsg;
        }*/

        //return $response;
        $pos = strpos($response,"\r\n\r\n");
        return array(
            'code'=>curl_getinfo($ch,CURLINFO_HTTP_CODE),
            'header'=>substr($response,0,$pos),
            'body'=>substr($response,$pos+4),
            'error'=>$errmsg
        );
        //curl_close($ch);
    }


$urls = [
    'http://baidu.com',
    // 'http://www.gov.cn',
    // 'http://weibo.com',
    'http://www.kuaidi100.com/query?type=jd&postid=20386936987',
    'http://www.kuaidi100.com/query?type=jd&postid=15309279640&id=1&valicode=&temp=0.6581699516015975',
    'http://bing.com',
    // 'http://qq.com',
    'http://www.kuaidi.com/index-ajaxselectcourierinfo-200379498601-yuantong.html',
    'http://www.kiees.cn/yto/?wen=882543630433324584&action=ajax&rnd=0.4578285292756865',
    // 'http://biz.trace.ickd.cn/yuantong/882543630433324584?mailNo=882543630433324584&spellName=yuantong&exp-textName=%E5%9C%86%E9%80%9A%E5%BF%AB%E9%80%92&ts=123456&enMailNo=123456789&callback=_jqjsp&_1472094097123=',
    // 'http://www.kuaidi100.com/query?type=yuantong&postid=882543630433324584&id=1&valicode=&temp=0.06873791872429091',
];

$returns = [];

$proxy = '127.0.0.1:8087';

foreach ($urls as $key => $url) {

    // $r = proxy($url, $proxy);
    // if(is_array($r)){
    //     $r = isset($r['return'])?$r['return']:$r;
    // }
    // if(is_string($r) && mb_strlen($r) > 500){
    //     $r = htmlentities(mb_substr($r, 0, 500));
    // }
    // $returns[] = $r;
    $rtn = send($url, $proxy);
    if(is_array($rtn)){
        $r = isset($rtn['body'])?$rtn['body']:'';
    }
    if(is_string($r) && mb_strlen($r) > 500){
        $rtn['body'] = htmlentities(mb_substr($r, 0, 500));
    }
    $returns[] = $rtn;
}

var_dump($returns);
