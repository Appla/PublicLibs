<?php
ini_set('display_errors', true);
error_reporting(-1);
echo PHP_VERSION, PHP_EOL;
// START@#2017-06-#

//exit;
// END@#2017-06-#


// START@#2017-06-#

$r = new Redis();
$config = [
    '127.0.0.1',
    6379
];
$r->connect(...$config);
$arr[] = $r->sMembers('set1');
$arr[] = $r->sMembers('set2');

var_dump($arr);
exit;
$pipe = $r->pipeline();
$cacheSet = [
    'set1' => [1, 2, 3, 4, 6],
    'set2' => [5,3,4,88,6,1,3,5],
];
foreach ($cacheSet as $key => $item) {
    array_unshift($item, $key);
    call_user_func_array([$pipe, 'sadd'], $item);
}
$pipe->exec();
exit;
// END@#2017-06-#



// START@#2017-06-12#

$allowedDates = [];
$startDate = '2017-02-28';
$endDate = '2017-05-23';
$startDateObj = new DateTime($startDate);
$endDateObj = new DateTime($endDate);
$dateFormat = 'Y-m-d';
if (!$startDateObj || !$endDateObj) {
    throw new \Exception(sprintf('error params@%s', __METHOD__));
}
while ($startDateObj <= $endDateObj){
    $allowedDates[] = $startDateObj->format($dateFormat);
    $startDateObj->add(new \DateInterval('P1D'));
}

var_dump($allowedDates);

exit;
$keyPattern = 'address:taskProcessedRecordCount:mergeAddress*';

$r = new Redis();
$config = [
    '10.20.1.161',
    6380
];
$r->connect(...$config);

$allKeys = $r->keys($keyPattern);

array_walk($allKeys, function($key) use($r){
    printf('key:%s, value:%s'.PHP_EOL, $key, $r->get($key));
    // echo printf('key %s is %s'.PHP_EOL, $key, $r->del($key) ? 'deleted' : 'not modified');
});
exit;
// END@#2017-06-12#



// START@#2017-06-09#

$json_str =<<<JSON_STR
{"3331099143407":{"address":"莘庄镇莘沥路512弄莘松7村19号102室","city":"上海市","district":"闵行区","from_city":"金华市","from_district":"义乌市","from_province":"浙江省","province":"上海","waybill":"3331099143407"},"3331082928728":{"address":"沈浒路雅戈尔萧邦西区77-3003","city":"苏州市","district":"园区","from_city":"徐州市","from_district":"泉山区","from_province":"江苏省","province":"江苏省","waybill":"3331082928728"},"3331095660986":{"address":"北干街道浙江省杭州市萧山区金城路天汇园3幢901室","city":"杭州市","district":"萧山区","from_city":"揭阳市","from_district":"榕城区","from_province":"广东省","province":"浙江省","waybill":"3331095660986"},"3331097887245":{"address":"东城街道煤建巷内锦华苑小区内","city":"咸阳市","district":"兴平市","from_city":"金华市","from_district":"浦江县","from_province":"浙江省","province":"陕西省","waybill":"3331097887245"},"3331085814119":{"address":"人民路街道江西财经职业学院","city":"九江市","district":"浔阳区","from_city":"杭州市","from_district":"余杭区","from_province":"浙江省","province":"江西省","waybill":"3331085814119"},"3331090061905":{"address":"郑东新区心怡路畅和街交叉口西南角山也瑜伽","city":"郑州市","district":"其它区","from_city":"宁波市","from_district":"鄞州区","from_province":"浙江省","province":"河南省","waybill":"3331090061905"},"3331034193472":{"address":"枫林路街道零陵路250弄54号601","city":"上海市","district":"徐汇区","from_city":"天津市","from_district":"南开区","from_province":"天津","province":"上海","waybill":"3331034193472"},"402420718894":{"address":"凤凰镇嘉南印刷 西参村委会对面","city":"苏州市","district":"张家港市","from_city":"金华市","from_district":"东阳市","from_province":"浙江省","province":"江苏省","waybill":"402420718894"},"3331095543485":{"address":"西陵街道三峡大学科技学院东苑","city":"宜昌市","district":"西陵区","from_city":"杭州市","from_district":"江干区","from_province":"浙江省","province":"湖北省","waybill":"3331095543485"},"3331086886832":{"address":"北蔡镇博华路498弄3号1203室","city":"上海市","district":"浦东新区","from_city":"宁波市","from_district":"慈溪市","from_province":"浙江省","province":"上海","waybill":"3331086886832"},"3331099611850":{"address":"东海街道河西区华江里一委12号楼3门602","city":"天津市","district":"河西区","from_city":"赣州市","from_district":"龙南县","from_province":"江西省","province":"天津","waybill":"3331099611850"},"3331098210065":{"address":"碧阳镇城区东街23号。","city":"黄山市","district":"黟县","from_city":"金华市","from_district":"永康市","from_province":"浙江省","province":"安徽省","waybill":"3331098210065"},"221150169731":{"address":"华庄街道水清木华二期","city":"无锡市","district":"滨湖区","from_city":"镇江市","from_district":"丹徒区","from_province":"江苏省","province":"江苏省","waybill":"221150169731"},"3331075466603":{"address":"新华路街道延安西路1118号龙之梦大厦1507室","city":"上海市","district":"长宁区","from_city":"杭州市","from_district":"余杭区","from_province":"浙江省","province":"上海","waybill":"3331075466603"},"3331087126755":{"address":"新桥镇辽河路666号常州工学院辽河路校区","city":"常州市","district":"新北区","from_city":"杭州市","from_district":"拱墅区","from_province":"浙江省","province":"江苏省","waybill":"3331087126755"},"3331078561016":{"address":"翠竹街道田贝二路9号金翠园c座1903。","city":"深圳市","district":"罗湖区","from_city":"杭州市","from_district":"余杭区","from_province":"浙江省","province":"广东省","waybill":"3331078561016"},"402522862702":{"address":"荷花街道花鸟市场西大门29号幸运阁女装","city":"衢州市","district":"柯城区","from_city":"深圳市","from_district":"龙华新区","from_province":"广东省","province":"浙江省","waybill":"402522862702"},"221121253714":{"address":"朗霞街道余姚中国裘皮城二期商务楼B栋710室","city":"宁波市","district":"余姚市","from_city":"苏州市","from_district":"园区","from_province":"江苏省","province":"浙江省","waybill":"221121253714"},"3331096207169":{"address":"兴城镇济德医院一楼导诊 气象局南","city":"唐山市","district":"迁西县","from_city":"杭州市","from_district":"萧山区","from_province":"浙江省","province":"河北省","waybill":"3331096207169"},"3331080647065":{"address":"宽甸镇质量技术监督局","city":"丹东市","district":"宽甸满族自治县","from_city":"温州市","from_district":"瑞安市","from_province":"浙江省","province":"辽宁省","waybill":"3331080647065"},"3331099997876":{"address":"西园街道文化路保健医院向西爱心幼儿园对面（大众发廊）院内8号楼中单元16号","city":"驻马店市","district":"驿城区","from_city":"台州市","from_district":"路桥区","from_province":"浙江省","province":"河南省","waybill":"3331099997876"},"221146591036":{"address":"金钟街道欢坨工业区莱博化工有限公司院内","city":"天津市","district":"东丽区","from_city":"苏州市","from_district":"吴江区","from_province":"江苏省","province":"天津","waybill":"221146591036"},"402491092143":{"address":"潘火街道   康强路东方丽都","city":"宁波市","district":"鄞州区","from_city":"石家庄市","from_district":"长安区","from_province":"河北省","province":"浙江省","waybill":"402491092143"},"3331099997943":{"address":"金沙镇通州区翠园花苑8幢604室","city":"南通市","district":"通州区","from_city":"台州市","from_district":"路桥区","from_province":"浙江省","province":"江苏省","waybill":"3331099997943"},"3331098291205":{"address":"泉城路街道山东省历下区我爱记歌词 KTV","city":"济南市","district":"历下区","from_city":"杭州市","from_district":"萧山区","from_province":"浙江省","province":"山东省","waybill":"3331098291205"},"3331097747277":{"address":"盘河路新城街道新梅苑c区","city":"渭南市","district":"韩城市","from_city":"漳州市","from_district":"龙海市","from_province":"福建省","province":"陕西省","waybill":"3331097747277"},"3331099197705":{"address":"佛堂镇   梅林村270号","city":"金华市","district":"义乌市","from_city":"深圳市","from_district":"龙岗区","from_province":"广东省","province":"浙江省","waybill":"3331099197705"},"3331099907715":{"address":"澥浦镇宁波市镇海区澥浦卫生院","city":"宁波市","district":"镇海区","from_city":"北京市","from_district":"朝阳区","from_province":"北京","province":"浙江省","waybill":"3331099907715"},"3331099749981":{"address":"苏州高铁新城高铁北站停车场","city":"苏州市","district":"相城区","from_city":"临沂市","from_district":"兰山区","from_province":"山东省","province":"江苏省","waybill":"3331099749981"},"3331100130045":{"address":"东山街道天马山大道芙蓉新城正大门左侧孟婆寨（下午四点半派送）","city":"赣州市","district":"南康区","from_city":"台州市","from_district":"温岭市","from_province":"浙江省","province":"江西省","waybill":"3331100130045"},"3331097859956":{"address":"彭李街道黄河十二路733号望海花园","city":"滨州市","district":"滨城区","from_city":"温州市","from_district":"苍南县","from_province":"浙江省","province":"山东省","waybill":"3331097859956"},"3331099667616":{"address":"永福东大街大华富贵世家","city":"钦州市","district":"钦北区","from_city":"金华市","from_district":"义乌市","from_province":"浙江省","province":"广西壮族自治区","waybill":"3331099667616"},"3331099555016":{"address":"三江街道新农贸市场二楼三街B124-125","city":"金华市","district":"婺城区","from_city":"深圳市","from_district":"龙岗区","from_province":"广东省","province":"浙江省","waybill":"3331099555016"},"3331100454769":{"address":"凤州镇和谐花园","city":"宝鸡市","district":"凤县","from_city":"泉州市","from_district":"安溪县","from_province":"福建省","province":"陕西省","waybill":"3331100454769"},"3331099983251":{"address":"江浦街道浦口区浦口大道1号新城总部大厦B座21楼","city":"南京市","district":"浦口区","from_city":"深圳市","from_district":"福田区","from_province":"广东省","province":"江苏省","waybill":"3331099983251"},"3331099231253":{"address":"三乡镇乌石工业区宇宙工业大夏6楼","city":"中山市","district":"","from_city":"宁波市","from_district":"宁海县","from_province":"浙江省","province":"广东省","waybill":"3331099231253"},"3331099741729":{"address":"三十里堡街道三十里堡西山后农场卫国社区 英翔生物科技公司","city":"大连市","district":"普兰店市","from_city":"宁波市","from_district":"余姚市","from_province":"浙江省","province":"辽宁省","waybill":"3331099741729"},"3331099799181":{"address":"江都路街道河北区江都路第三医院","city":"天津市","district":"河北区","from_city":"深圳市","from_district":"龙华新区","from_province":"广东省","province":"天津","waybill":"3331099799181"},"3331094903448":{"address":"梅园新村街道太平门街81号政治部办公室","city":"南京市","district":"玄武区","from_city":"深圳市","from_district":"罗湖区","from_province":"广东省","province":"江苏省","waybill":"3331094903448"},"3331100309724":{"address":"戴圩街道江苏省邳州市戴圩镇前进庄","city":"徐州市","district":"邳州市","from_city":"上海市","from_district":"普陀区","from_province":"上海","province":"江苏省","waybill":"3331100309724"},"402579970917":{"address":"开发区江苏省昆山市蓬朗蓬溪南路1号 昆山弘琨电子有限公司","city":"苏州市","district":"昆山市","from_city":"泉州市","from_district":"丰泽区","from_province":"福建省","province":"江苏省","waybill":"402579970917"},"3331100449376":{"address":"三江街道三江城兴旺街288号茶叶城南-11号","city":"绍兴市","district":"嵊州市","from_city":"保定市","from_district":"高碑店市","from_province":"河北省","province":"浙江省","waybill":"3331100449376"},"3331100251060":{"address":"起毛北，液化气站对面，旭日综合楼","city":"连云港市","district":"赣榆区","from_city":"潍坊市","from_district":"潍城区","from_province":"山东省","province":"江苏省","waybill":"3331100251060"},"3331081261272":{"address":"六里山街道山东省济南市市中区山景园小区4号楼3单元301","city":"济南市","district":"市中区","from_city":"杭州市","from_district":"余杭区","from_province":"浙江省","province":"山东省","waybill":"3331081261272"},"3331099900660":{"address":"荣巷街道江苏省无锡市滨湖区钱荣路68号无锡太湖学院","city":"无锡市","district":"滨湖区","from_city":"台州市","from_district":"温岭市","from_province":"浙江省","province":"江苏省","waybill":"3331099900660"},"3331096245621":{"address":"旺村镇流标（中通，韵达，天天，圆通）这四个任选其一","city":"廊坊市","district":"大城县","from_city":"湖州市","from_district":"吴兴区","from_province":"浙江省","province":"河北省","waybill":"3331096245621"},"3331076403464":{"address":"城厢镇解放街52号金太子发屋","city":"重庆市","district":"巫溪县","from_city":"杭州市","from_district":"余杭区","from_province":"浙江省","province":"重庆","waybill":"3331076403464"},"221150157277":{"address":"板桥新城宋都南郡9栋1407","city":"南京市","district":"雨花台区","from_city":"苏州市","from_district":"昆山市","from_province":"江苏省","province":"江苏省","waybill":"221150157277"},"3331097846737":{"address":"null常青街道长治市西二环路（北寨十字路往南300米）北京现代4S店","city":"长治市","district":"城区","from_city":"金华市","from_district":"浦江县","from_province":"浙江省","province":"山西省","waybill":"3331097846737"},"3331076846790":{"address":"~saf","city":"自贡市","district":"自流井区","from_city":"杭州市","from_district":"余杭区","from_province":"浙江省","province":"四川省","waybill":"3331076846790"}}
JSON_STR;


$batchAddress = json_decode($json_str, true);

var_dump($arr = filter($batchAddress), count($batchAddress), count($arr));

function filter($batchAddress)
{
    return $batchAddress ? array_filter(array_map(function($address){
    $address['address'] = preg_replace('~[[:space:]]|null~', '', $address['address']);
    return $address;
}, $batchAddress), function($info){
//            return stripos($info['city'], $this->currentCity) !== false;
    return mb_strlen(preg_replace('~[[:print:]]~', '', $info['address'])) > 3;
}) : [];
}

exit;

$addresses = array_column(json_decode($json_str, true), 'address');
$count = 0;
array_walk($addresses, function($address) use(&$count){
    $cleanAddress = cleanAddressDetail($address);
    $address === $cleanAddress || printf('%d, compare: %s : %s'. PHP_EOL,  ++$count , $address, $cleanAddress);
});

/**
 * @param string $address
 * @return mixed
 */
function cleanAddressDetail($address)
{
    /**
     * 楼栋匹配规则
     * @var array
     */
    $building = [
        '((?:[A-Z]区)?\d{1,4}(?:号楼|[D#栋H幢]|单元)(?:\w+(?:单元|[区座栋幢梯号]))?)',
        '(\d+号(?:[A-Z]区))',
        '(?<![楼层F])([A-Z](?:[座栋梯幢D#]|单元))',
        '(^\d{1,4}号)(?:[1-9]\d?(?:[楼层F]|单元)|[1-9]\d?[012]\d室?|[A-H]单元)',
    ];
    /**
     * 层匹配规则
     * @var array
     */
    $floor = [
        '((?:地下|负|B)?\d{1,2}(?:[楼层FL]|\\\')(?:[A-H](?:[座室]|单元)?|\d{1,4}室)?)',
        '([1-9]\d?[012]\d(?:-\d{3,4}室?|房|室|宿舍|[A-H]))',
        '(?<!\d|[路道街弄巷区])([1-9]\d?[012]?\d(?:-\d{1,2}[号室]?|[号室])?$)',
        '(?<!\d)([1-9][012]?\d?-\d{1,2}$)',
        '(?<!\d)([1-9]\d?[A-G]$)',
    ];
    //上海的小区
    $tmp = $address;
    $village = '(?=(?:乡|镇|街道|弄|^))(.+[村庄])(?:$|\d+)';
    if(preg_match('~' . $village . '~u', $address, $matched)){
        $address = explode($matched[1], $address)[0] . $matched[1];
    }else{
        $address = preg_split('~' . implode('|', $floor) . '~iu', $address)[0];
        $address =  preg_split('~' . implode('|', $building) . '~iu', $address)[0];
        $address = preg_replace('/(?<![[:print:]])[[:print:]]$/', '', trim($address, " \t\r\n-/#.|\',"));
    }
    return $address;
}

exit;
$keyPattern = 'address:taskProcessedRecordCount*';
//address:lastExecutionTime:StoLocationInfoCleaner:tbl_addresses_1149cleanInvalidAddress
// $keyPattern = 'address:lastExecutionTime:StoLocationInfoCleaner:tbl_addresses_*';
$r = new Redis();
$config = [
    '10.20.1.161',
    6380
];
$r->connect(...$config);

$allKeys = $r->keys($keyPattern);

array_walk($allKeys, function($key) use($r){
    printf('key:%s, value:%s'.PHP_EOL, $key, $r->get($key));
    // echo printf('key %s is %s'.PHP_EOL, $key, $r->del($key) ? 'deleted' : 'not modified');
});


exit;
// END@#2017-06-09#

// START@#2017-06-08#
// echo date('Y-m-d\TH:i:s\Z');
echo $str = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d\TH:i:s\Z');
echo PHP_EOL;

echo (new DateTime($str))->format('Y-m-d\TH:i:s\Z');
echo PHP_EOL;
echo date(DateTime::ATOM);
exit;
$str = "\n\r\t";
var_dump(trim($str) === '');

exit(0);
// END@#2017-06-8#

// START@#2017-06-07#


echo json_encode(array_column(json_decode($jsonStr, true), null, 'waybill'));

exit(0);
// END@#2017-06-7#

// START@#2017-06-06#

$a = ['1' => [1, 2]];
$b = ['1    ' => [3, 2, 4]];

var_dump(array_merge_recursive($a, $b));

exit;
$r = new Redis();
$config = [
    '10.20.1.161',
    6380
];
$config = [
    '127.0.0.1',
    6379
];
$r->connect(...$config);


$r->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_PHP);
// $r->set('foo', ['one','two','three']);
// var_dump($r->get('foo'));
$r->hset('abc', '111', [1, 2, 3]);
var_dump($r->hget('abc', '111'));
// $r->mset(['aaa' => [1, 2, 3, 4], 'bbbbb' => ['one','two','three'], '333333' => ['one','two','three']]);
// var_dump($r->mget(['aaa', 'bbbbb', '333333', 'foo', 'cccc']));
exit;

$key = 'addressInfo:level2_嘉峪关市';
$ret = $r->get($key);

var_dump($ret);
exit;
function dupCheck()
{
    $prefix = 'addressInfo:level3_';
$area = [
    '城区',
    '市中区',
    '郊区',
    '鼓楼区',
    '桥西区',
    '新华区',
    '铁西区',
    '江北区',
    '南山区',
    '普陀区',
    '长安区',
    '西湖区',
    '河东区',
    '双桥区',
    '朝阳区',
    '青山区',
    '宝山区',
    '通州区',
    '海州区',
    '向阳区',
    '桥东区',
    '高新区',
    '白云区',
    '和平区',
    '铁东区',
    '城中区',
    '矿区',
    '永定区',
    '龙华区',
    '新城区',
    '城关区',
    '西安区',
];

array_walk($area, function($name) use($prefix, $r) {
    $key = $prefix . $name;
    if($ret = $r->get($key)){
        $districts = unserialize($ret);
        $checkKey = 'city_name';
        $provinces = array_column($districts, $checkKey);
        if(count(array_unique($provinces)) !== count($provinces)){
            sort($provinces);
            printf('the province[%s] has same name district[%s]'.PHP_EOL, implode(',', $provinces), $name);
        } else {
            printf('district[%s] is%s[%s] not share the same name'.PHP_EOL, $name, $checkKey, implode(',', $provinces));
        }
    }else{
        printf('ERROR: area %s not CACHED'.PHP_EOL, $name);
    }
});
}

exit;
$key = 'addressInfo:level3_东山区';
$ret = $r->get($key);

var_dump($ret);

var_dump(unserialize('a:1:{i:0;a:12:{s:13:"province_name";s:12:"黑龙江省";s:18:"province_shortname";s:9:"黑龙江";s:11:"province_id";s:4:"7661";s:14:"province_alias";a:0:{}s:9:"city_name";s:9:"鹤岗市";s:14:"city_shortname";s:6:"鹤岗";s:7:"city_id";s:4:"7700";s:10:"city_alias";a:0:{}s:11:"county_name";s:9:"东山区";s:16:"county_shortname";s:6:"东山";s:9:"county_id";s:4:"7709";s:12:"county_alias";a:0:{}}}'));

exit;
// END@#2017-06-06#




// START@#2017-06-#

$ips = [
    '10.1.1.1',
    '192.168.1.1',
    '100.65.1.1',
    '0.0.0.0',
    '172.16.1.1',
    '192.0.2.1',
];

$result = array_map(function($ip){
    return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE);
}, $ips);
//filter_var()


var_dump($result);


exit;
// END@#2017-06-#



// START@#2017-06-02#

$r = new Redis();
$config = [
    '127.0.0.1',
    6379
];
$r->connect(...$config);
$r->close();
$a = null;
var_dump(!$a instanceof Redis);
// $r->close();
exit;


// $sql = 'SELECT * FROM cache_data WHERE `key` = "a"';
// $config = [
//     '127.0.0.1',
//     'root',
//     'root',
//     'address',
// ];
// $mysql = new mysqli(...$config);
// if ($mysql->connect_errno) {
//     die($mysql->connect_error);
// }
// $mysql->close();
// if ( && $mysql->ping() && $result = $mysql->query($sql)){
//     $arr = $result->fetch_array();
//     var_dump($arr);
// } else {
//     echo 'hello', PHP_EOL;
//     // var_dump($mysql);
// }


var_dump($result);



exit;
$str = 'sekect SELECT ASFASF';
$pattern = '/^(select|SELECT)/';
preg_match($pattern, $str, $match);
var_dump($match);

exit();
// END@#2017-06-02#




