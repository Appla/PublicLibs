<?php
echo PHP_VERSION, PHP_EOL;
// START@#2017-05-#  // END@#2017-05-#


// START@#2017-05-31#

$configs = [
    '127.0.0.1',
    6379
];
$r = new Redis();
$r->connect(...$configs);

$key = 'test';
var_dump($r, $r->isConnected(), $r->get($key));

$r->close();

var_dump($r, $r->isConnected(), empty($r));

exit;
// END@#2017-05-31#

// START@#2017-05-27#

function quickSort(array $arr, $left, $right)
{
    //left最左边,right在最右边, 方法是将所有大于某值的数都放在左边,反之放到右边
    //随机选取一个值,比较,大于选定值就和 right 交换位置right--,小于就和left交换位置,left++
}
exit;
// END@#2017-05-27#


// START@#2017-05-26#

$arr = range(0, 10);

$newArr = array_filter($arr, function($val) {
    return $val % 2;
});

var_dump($newArr);

exit;
// END@#2017-05-26#




// START@#2017-05-17#

// echo 1 ? 1 ? 2 : 1 : -1;
// echo 1 ? 1 : 0 ? 1 : -1;

// exit;
$str = 'a:1:{i:0;a:12:{s:13:"province_name";s:9:"辽宁省";s:18:"province_shortname";s:6:"辽宁";s:11:"province_id";s:4:"7869";s:14:"province_alias";a:0:{}s:9:"city_name";s:9:"抚顺市";s:14:"city_shortname";s:6:"抚顺";s:7:"city_id";s:4:"7923";s:10:"city_alias";a:0:{}s:11:"county_name";s:9:"东洲区";s:16:"county_shortname";s:6:"东洲";s:9:"county_id";s:4:"7926";s:12:"county_alias";a:0:{}}}';

var_dump(unserialize($str));

exit();
// END@#2017-05-17#



// START@#2017-05-16#

$url = 'http://www.baidu.com/query';
// $url = 'http://api.open.kuaidihelp.com/v1/addressLib/querySortingCode?asfas1=411';

echo preg_replace('~^https?://|\?.*$~i', '', $url);
exit;
// echo strstr($url, '?', true), PHP_EOL;

// exit;

// $urlArr = parse_url($url);
// var_dump($urlArr);

$postData = [
    'test' => 'safasf',
    'afasfa' => '111111',
];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$postData && curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 1);
$ret = curl_exec($ch);
$info = curl_getinfo($ch);
$info['error'] = curl_error($ch);
$info['error_no'] = curl_errno($ch);
var_dump($ret, $info);

exit;
// END@#2017-05-16#
