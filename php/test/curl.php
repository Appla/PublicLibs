<?php
class curl_test
{
    protected $url = 'http://www.kuaidi100.com/query?type=yuantong&postid=700235024899';
    protected $proxy = '';
    public $returns = [];
    protected $store_return = false;

    public function __construct($url = 'http://dynupdate.no-ip.com/ip.php', $proxy = '')
    {
        $proxy and $this->proxy = $proxy;
        $url and $this->url = $url;
    }


    public function setUrl($url)
    {
        $url and $this->url = $url;
    }


    public function setProxy($proxy)
    {
        $proxy and $this->proxy = $proxy;
    }


    public function request($post_data = [], array $options = [])
    {
        if(empty($this->url)){
            die('url is empty:');
        }
        $header = [
            "DNT: 1" ,
            "Accept-Language: zh-CN,zh;q=0.8",
            "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2816.0 Safari/537.36",
            "Accept: */*",
            "Cache-Control: no-cache",
            "X-Requested-With: XMLHttpRequest",
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        $this->proxy AND curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        if($post_data){
            // curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
        }
        curl_setopt($ch, CURLOPT_COOKIESESSION, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_COOKIEJAR, './cookie_t.txt');
        curl_setopt($ch, CURLOPT_COOKIE, './cookie_t.txt');
        foreach ($options as $k => $val) {
            curl_setopt($ch, $k, $val);
        }

        // curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2816.0 Safari/537.36');
        // curl_setopt($ch, CURLOPT_HEADER, 1);
        if(!($curl_scraped_page = curl_exec($ch))){
            var_dump(curl_getinfo($ch), curl_errno($ch), curl_error($ch));
        }
        curl_close($ch);
        $this->store_return && $this->returns[] = $curl_scraped_page;
        echo $curl_scraped_page;
    }

    public function repeater($func = null,array $params = [], $times = 10)
    {
        $timer['start'] = microtime(true);
        if (is_callable($func)) {
            while ($times > 0) {
                echo $times--,PHP_EOL;
                call_user_func($func, $params);
            }
        }
        $timer['end'] = microtime(true);
        $timer['cost']  = $timer['end'] - $timer['start'];
        echo implode(' ',$timer),PHP_EOL;
    }

    public function get_returns()
    {
        return $this->returns;
    }
}


// exit;
// var_dump(is_callable('curl_proxy_test'));

// $url = 'https://httpbin.org/headers';
// $url = 'https://httpbin.org/cookies/set?k2=v2&k1=v1';
// $url = 'https://httpbin.org/cookies';
//$url = 'http://www.kuaidi100.com/query?type=yuantong&postid=700235024899';
//$url = 'http://58.83.226.244/Express_NET_BJFX/UI/Track/TrackPOD_Comm100.aspx';

// $proxy = '153.120.43.42:80';
// $post_data = [
//                 'txtJobNoList' => 'YG00003563523',
//                 'btnQuery' => '确认查询',
//                 '__VIEWSTATE' => '/wEPDwUJLTE5NjIwOTI1DxYKHgdOZXRDb2RlBQRCSkZYHgpWaWV3U2lnbkltBQFOHglDVFJOT19SRUZlHglzdHJfd2lkdGgFAzQwMB4Kc3RyX2hlaWdodAUDMzMwFgICAw9kFgQCAw8PFgIeB1Zpc2libGVoZGQCBQ8PFgIfBWhkZGQ/sprAwvhtR786uuIZj4YwK2OW8g==',
//                 'RdList' => 0
//             ];

$post_data = [
    'test' => '111',
    'ZZ' => [],
    1 => [],
];
// $url = 'http://bhgit.com/appla/index.php?debug=1';
$url = 'http://t.com/handler.php?TZ=1';
$url = 'baidu.com';
// $proxy = '10.117.192.194:4433';
$proxy = '';
$test = new curl_test($url, $proxy);
$test->request();
var_dump(CURLOPT_HTTPPROXYTUNNEL);
// $test->request($post_data, [CURLOPT_HTTPPROXYTUNNEL => false]);
// $test->request($post_data, [CURLOPT_HTTPPROXYTUNNEL => true]);
// $test->repeater('request',3);
// $return = $test->get_returns();
// var_dump($return);
// echo "\n", count($return), ':', count(array_unique($return));

//http://www.kuaidi100.com/query?type=yuantong&postid=700235024899&id=1&valicode=&temp=0.616005345769606


