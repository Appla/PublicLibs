<?php
echo PHP_VERSION, PHP_EOL;

class a{
    private function pf(){}
    protected function pt(){}
    public function pb(){}
}
$a = new a;

var_dump(method_exists($a, 'pf'), method_exists($a, 'pb'), method_exists($a, 'pt'));

exit;
class myData implements IteratorAggregate {
    public $property1 = "Public property one";
    public $property2 = "Public property two";
    public $property3 = "Public property three";

    public function __construct() {
        $this->property4 = "last property";
    }

    public function getIterator() {
        echo '....';
        return new ArrayIterator($this);
    }
}

$obj = new myData;

var_dump(new ArrayIterator($obj));

foreach($obj as $key => $value) {
    var_dump($key, $value);
    echo "\n";
}

exit;

$rand = implode('',array_pad([],30,mt_rand(1,9)));
$start = $rand.'00';
$end = $rand.'50';

echo $start,PHP_EOL,$end,PHP_EOL,$rand;
$start = 1000000;
$end = 1000050;

$range = createWaybillRange($start, $end, 2, 10);

var_dump($range);

function createWaybillRange($start = null, $end = null, $step = 1, $limit = PHP_INT_MAX)
{
    if ($start === null) {
        return [];
    } elseif ($end === null) {
        $end = $start + 100;
    }
    $int_safe_flag = true;
    if($start >= (string) PHP_INT_MAX || $end >= (string) PHP_INT_MAX){
        if(!function_exists('gmp_sub')){
            return [];
        }
        $sub_val = gmp_strval(gmp_sub($end, $start));
        if($sub_val >= (string)$limit){
            $sub_val = $limit;
        }
        $int_safe_flag = false;
    }else{
        ($end - $start) > $limit and $end = $start + $limit;
    }
    return  $int_safe_flag ? range($start, $end, $step) : bigIntRange($start, $end, $sub_val, $step);
}

function bigIntRange($start = null, $end = null, $sub_val, $step = 1)
{
    $result = [$start];
    for ($i=0; $i <= (int)$sub_val; $i+=$step) {
        $result[] = gmp_strval(gmp_add($start, $i));
    }
    return $result;
}


exit;
$a = (float)'111111111111111';
for($i =0; $i < $a; $i++){
    echo $a++,PHP_EOL, $a - 0;
    if($i > 20){
        break;
    }
}

echo $a;
exit;

var_dump('9976681038412' > PHP_INT_MAX);
var_dump(PHP_INT_MAX > '9976681038412');
var_dump((int)'9976681038412' > PHP_INT_MAX);
var_dump('9976681038412' > (string)PHP_INT_MAX);

exit;
$a = range('9976681038412', '9976681038462');
var_dump($a);



exit;
// ini_set('memory_limit', '1000M');
$array = range( 0, 1000000 );

function doNothing( $value, $key ) {;}

$t1_start = microtime(true);
foreach( $array as $key => $value ) {
    doNothing( $value, $key );
}
$t1_end = microtime(true);

$t2_start = microtime(true);
$array_size = count( $array );
for( $key = 0; $key < $array_size; $key++ ) {
    doNothing( $array[$key], $key );
}
$t2_end = microtime(true);

    //suggestion from PHPBench as the "fastest" way to iterate an array
$t3_start = microtime(true);
$key = array_keys($array);
$size = sizeOf($key);
for( $i=0; $i < $size; $i++ ) {
    doNothing( $key[$i], $array[$key[$i]] );
}
$t3_end = microtime(true);

$t4_start = microtime(true);
array_walk( $array, "doNothing" );
$t4_end = microtime(true);

print
    "Test 1 ".($t1_end - $t1_start)."\n". //Test 1 0.342370986938
    "Test 2 ".($t2_end - $t2_start)."\n". //Test 2 0.369848966599
    "Test 3 ".($t3_end - $t3_start)."\n". //Test 3 0.78616809845
    "Test 4 ".($t4_end - $t4_start)."\n"; //Test 4 0.542922019958

exit;


echo (int)PHP_INT_MAX;
exit;
class tester
{
    public $valid_express_nos;
    protected $vpns;

    protected static $db;

    protected static $redis;

    protected $url_tpls = [
        'http://express.interface.kuaidihelp.com/infor.php?company=%s&express_no=%s&type=express.get&no_cache=1',
    ];

    protected $url_tpl =  'http://express.interface.kuaidihelp.com/infor.php?company=%s&express_no=%s&type=express.get&no_cache=1';

    public function __construct()
    {

    }

    public function run()
    {

    }

    private function getDb()
    {

    }

    private function redis()
    {

    }




    public function getUrl(array $data, array $rand_data = null )
    {
        $str = '';
        if(is_array($rand_data)){
            $str = $this->buildQueryString($rand_data);
        }
        return vsprintf($this->url_tpl, $data).$str;
    }

    private function buildQueryString(array $data)
    {
        return http_build_query($data);
    }

    private function getWaybills()
    {

    }

    public function repeater($func = null,array $params = [], $times = 10)
    {
        $timer['start'] = microtime(true);
        if (is_callable($func)) {
            while ($times > 0) {
                echo $times--,PHP_EOL;
                call_user_func($func, $params);
            }
        }
        $timer['end'] = microtime(true);
        $timer['cost']  = $timer['end'] - $timer['start'];
        echo implode(' ',$timer),PHP_EOL;
    }

}


