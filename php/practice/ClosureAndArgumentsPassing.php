<?php
echo PHP_VERSION, PHP_EOL;

/**
 * inspired by lua closure
 * @see http://stackoverflow.com/questions/7781432/implementation-of-closures-in-lua/7781507#7781507
 * @date 2017-04-17
 */

//值传递
$val = 10086;
$local = [10086];
//引用传值
$obj = (new class{
    public $val = 10086;
});

$funcVal = function() use($val){
     return $val+=1;
};

$funcArr = function() use($local){
    return $local[0]+=1;
};

$funcObj = function() use($obj){
    return $obj->val+=1;
};

//不会改变所传的值
$val = 100;
$local = [100];
//会改变闭包的结果
$obj->val = 100;

echo 'int : passing by value', PHP_EOL;
echo implode(',', [$val, $funcVal(), $val, $funcVal(), PHP_EOL]);
//10086,10087,10086,10087

echo 'Array : passing by value', PHP_EOL;
echo implode(',', [$local[0], $funcArr(), $local[0], $funcArr(), PHP_EOL]);
//10086,10087,10086,10087

echo 'Object : passing by reference', PHP_EOL;
echo implode(',', [$obj->val, $funcObj(), $obj->val, $funcObj(), PHP_EOL]);
//10086,10087,10086,10087

